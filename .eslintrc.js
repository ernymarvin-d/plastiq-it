module.exports = {
    root: true,
    extends: [
      "react-native-lit"
    ],
    plugins: [
      "detox"
    ],
    env: {
      "detox/detox": true
    },
    rules: {
      "no-underscore-dangle": "off",
      "global-require": "off",
      "no-var": "warn",
      "no-redeclare": "warn",
      "no-unused-vars": "warn",
      "no-undef": "warn",
      "no-use-before-define": "warn",
      "no-useless-constructor": "warn",
      "no-shadow": "warn",
      "class-methods-use-this": "warn",
      "default-case": "warn",
      "camelcase": "warn",
      "eqeqeq": "warn",
      "no-nested-ternary": "warn",
      "vars-on-top": "warn",
      "block-scoped-var": "warn",
      "consistent-return": "warn",
      "no-case-declarations": "warn",
      "max-classes-per-file": "warn",
      "no-dupe-keys": "warn",
      "no-lonely-if": "warn",
      "no-return-await": "warn",
      "no-empty": "warn",
      "import/no-extraneous-dependencies": "warn",
      "import/order": "warn",
      "react/prefer-stateless-function": "off",
      "react/no-deprecated": "warn",
      "react/jsx-no-duplicate-props": "warn",
      "react/no-unused-state": "warn",
      "react/no-access-state-in-setstate": "warn",
      "react/no-string-refs": "warn",
      "react/destructuring-assignment": "warn",
      "react/no-unescaped-entities": "warn",
      "react/prop-types": "warn",
      "react/require-optimization": "warn",
      "react/state-in-constructor": "warn",
      "react-native/no-inline-styles": "warn",
      "react-native/no-single-element-style-arrays": "warn",
      "react-native/no-unused-styles": "warn",
      "eslint-comments/disable-enable-pair": "off",
      "eslint-comments/no-unlimited-disable": "warn",
      "react-redux/connect-prefer-named-arguments": "warn",
      "react-redux/mapDispatchToProps-prefer-shorthand": "warn",
      "react-redux/no-unused-prop-types": "warn"
    },
    "settings": {
      "import/resolver": {
        "babel-module": {}
      }
    }
  }