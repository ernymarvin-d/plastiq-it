import React from "react";
import { AppState, StatusBar, StyleSheet } from "react-native";
import { ThemeProvider } from "react-native-elements";

import { Provider } from "react-redux";
import NavigationService from "service/navigation";

import store from "@store/index";
import { setCurrentRoute } from "@store/navigation/actions";
import { refreshCards } from "@store/cashback/actions";
import AppContainer from "@navigation/AppContainer";
import analytics from "@segment/analytics-react-native";
import branch, { BranchEvent } from "react-native-branch";
import Radar from "react-native-radar";
import * as Sentry from "@sentry/react-native";
import AppConfig from "./src/config/AppConfig";
import "service/LogService";

const styles = StyleSheet.create({
  font: {
    fontFamily: "Montserrat",
    fontSize: 15,
    color: "white"
  }
});

global.ReactAppboy = require("react-native-appboy-sdk");

global.analytics = analytics;
global.branch = branch;
global.BranchEvent = BranchEvent;
global.Smartlook = require("smartlook-react-native-wrapper");

global.Radar = Radar;
global.Sentry = Sentry;

export default class extends React.PureComponent {
  constructor(props) {
    super(props);
    global.apiUrl = process.env.API_URL;

    console.disableYellowBox = true;

    this.state = {
      appState: AppState.currentState
    };
  }

  async componentDidMount() {
    const { smartlook_api_key } = AppConfig;
    const { Smartlook } = global;

    await Smartlook.init(smartlook_api_key);

    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    this.setState({ appState: nextAppState });
    const { Smartlook } = global;

    if (nextAppState === "background" || nextAppState === "inactive") {
      Smartlook.stopRecording();
    }

    if (nextAppState === "active") {
      Smartlook.startRecording();
      store.dispatch(
        refreshCards(() => {
          console.log("Refreshing cards...");
        })
      );
    }
  };

  render() {
    return (
      <Provider store={store}>
        <ThemeProvider
          theme={{
            colors: {
              primary: "#a64adb",
              grey3: "#d7beea"
            },
            Icon: {
              size: 20,
              color: "white",
              underlayColor: "transparent"
            },
            Input: {
              placeholderTextColor: "#ba90d8",
              selectionColor: "#ba90d8",
              inputStyle: styles.font,
              errorStyle: {
                marginLeft: 16,
                fontFamily: "Montserrat",
                fontSize: 14
              }
            },
            Button: {
              titleStyle: styles.font,
              loadingProps: { color: "#a64adb" }
            },
            Text: {
              h2Style: {
                fontSize: 18
              },
              h4Style: {
                fontSize: 14
              },
              style: styles.font
            }
          }}
        >
          <StatusBar barStyle="light-content" />
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
            onNavigationStateChange={(
              prevRoute,
              currentRoute,
              currentRouteName
            ) => {
              console.log(currentRouteName);
              store.dispatch(setCurrentRoute(currentRouteName.routeName));
            }}
          />
        </ThemeProvider>
      </Provider>
    );
  }
}
