# Plastiq - Mobile
[![CircleCI](https://circleci.com/gh/plastiq-it/mobile-app.svg?style=svg&circle-token=95c0b58857abb6c991cd619da9b157bbb3c67160)](https://circleci.com/gh/plastiq-it/mobile-app)

Plastiq Mobile is built using React Native. It uses Redux, AsyncStorage and Flux architecture to manage and simplify our application and data flows

## Installation

Setting up Android, iOS and React Native can be done in parallel.

### Android

- Install Java.
- Install Android SDK.
- Open Android SDK, install Gradle, Developer Tools, Google Play Services, Android Images etc.

### iOS

- Install XCode.
- Install CocoaPods.

### React Native

- Install NPM
- Run `npm install -g yarn react-native eslint`
- Download a Javascript Editor or IDE such as Visual Studio Code.

### Setting up an Android Simulator

- In Android Studio, when the Project has finished loading, go to Tools -> AVD Manager.
- Create a Android Image with Google Play Services.
- Optional - Enable HAXM Accelerator using an Android Image with Intel and Installing HAXM Accelerator.

### Installing on Android Device

TODO:

### Installing on iOS Device

- Open iTunes to detect the iOS Device and trust it.
- Plug in your iOS Device.
- Accept the Trust Prompts on both your Mac and iPhone.
- In Xcode, in Target Device, select `? iPhone` or `No devices connected to ...` which will help detect your iPhone.
- Press `Run` in Xcode.
- On initial runs, you will need to trust your Developer Account. On your iPhone, go to Settings, General, Profiles, YourDeveloperAccount, Trust YourDeveloperAccountEmail. Press `Run` again in Xcode if necessary.

## Development

- Run `yarn cache-clean` to clean your Cache, re-download dependencies and start a Node server to serve Javascript files and assets on the fly.
- Run `yarn start` to start a Node server.
- Run `yarn ios` to deploy to connected iOS Device or Simulator.
- Run `yarn android` to deploy to connected Android Device or Simulator (open AVD beforehand as the command can't open it automatically). 
- Run `yarn test` to run tests.

Logs from the app should appear in your Node Server.

## Debugging

### iOS

- Press `Cmd + D` to open the Developer Tools.

### Android

- Press `Cmd + M` to open the Developer Tools.

## Deployment

### iOS

- Open Xcode.
- Edit Scheme to Release.
- Product -> Archive.
- Window -> Organizer. (This should be automatically opened by the previous step)
- Distribute App.

To test Release Builds.

- Press `Run` in Xcode.

### Android

- `cd android`
- `./gradlew bundleRelease` -> `android/app/build/outputs/bundle/release/app.aab`

To test Release Builds.

- `react-native run-android --variant=release`
