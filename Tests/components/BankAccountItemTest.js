import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import BankAccountItem from "../../src/components/BankAccountItem";

const middlewares = [thunk]; // you can mock any middlewares here if necessary

const initialState = {};
const mockStore = configureStore(middlewares)(initialState);

describe("Testing BankAccountItem", () => {
  it("renders as expected", () => {
    const wrapper = shallow(<BankAccountItem store={mockStore} />);

    expect(wrapper.dive()).toMatchSnapshot();
  });
});
