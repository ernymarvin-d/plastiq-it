import React from "react";
import { shallow } from "enzyme";
import BannerCard from "../../src/components/BannerCard";

describe("Testing BannerCard", () => {
  it("renders as expected", () => {
    const component = shallow(<BannerCard data={{}} />);

    expect(component).toMatchSnapshot();
  });
});
