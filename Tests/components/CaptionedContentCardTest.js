import React from "react";
import { shallow } from "enzyme";
import CaptionedContentCard from "../../src/components/CaptionedContentCard";

describe("Testing CaptionedContentCard", () => {
  it("renders as expected", () => {
    const data = {};
    const component = shallow(<CaptionedContentCard data={data} />);

    expect(component).toMatchSnapshot();
  });
});
