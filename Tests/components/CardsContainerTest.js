import React from "react";
import { shallow } from "enzyme";
import CardsContainer from "../../src/components/CardsContainer";

describe("Testing CardsContainer", () => {
  it("renders as expected", () => {
    const component = shallow(
      <CardsContainer bannerCards={[]} carouselCards={[]} staticCards={[]} />
    );

    expect(component).toMatchSnapshot();
  });
});
