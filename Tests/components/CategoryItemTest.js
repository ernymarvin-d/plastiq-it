import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import CategoryItem from "../../src/components/CategoryItem";

const middlewares = [thunk]; // you can mock any middlewares here if necessary

const initialState = {};
const mockStore = configureStore(middlewares)(initialState);

describe("Testing CategoryItem", () => {
  it("renders as expected", () => {
    const wrapper = shallow(<CategoryItem store={mockStore} />);

    expect(wrapper.dive()).toMatchSnapshot();
  });
});
