import "react-native";
import React from "react";
import renderer from "react-test-renderer";
import CouponItem from "../../src/components/CouponItem";

test("CouponItem component renders correctly if item is empty object", () => {
  const tree = renderer.create(<CouponItem item={{}} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test("CouponItem component renders correctly if item.end_date is not null", () => {
  const item = {
    code: "demo code",
    description: "demo description",
    end_date: "2020-02-07"
  };
  const tree = renderer.create(<CouponItem item={item} />).toJSON();
  expect(tree).toMatchSnapshot();
});
