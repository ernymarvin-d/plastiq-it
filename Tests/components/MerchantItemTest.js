import "react-native";
import React from "react";
import renderer from "react-test-renderer";
import MerchantItem from "../../src/components/MerchantItem";

test("MerchantItem component renders correctly", () => {
  const item = {
    status: "demo status",
    avatar_url: "demo avatar_url",
    merchant_name: "demo merchant_name",
    refined_transaction_date: "demo refined_transaction_date",
    amount: "demo amount"
  };
  const tree = renderer.create(<MerchantItem item={item} />).toJSON();
  expect(tree).toMatchSnapshot();
});
