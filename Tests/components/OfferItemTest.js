import React from "react";
import { shallow } from "enzyme";
import OfferItem from "../../src/components/OfferItem";

describe("Testing OfferItem", () => {
  it("renders as expected", () => {
    const component = shallow(<OfferItem item={{}} />);

    expect(component).toMatchSnapshot();
  });
});
