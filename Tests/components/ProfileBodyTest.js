import React from "react";
import { shallow } from "enzyme";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import ProfileBody from "../../src/components/ProfileBody";

const middlewares = [thunk]; // you can mock any middlewares here if necessary
const initialState = {
  cashback: {} // very important option: it should be set
};
const mockStore = configureStore(middlewares)(initialState);

describe("Testing ProfileBody", () => {
  it("renders as expected", () => {
    const wrapper = shallow(<ProfileBody store={mockStore} />);

    expect(wrapper.dive()).toMatchSnapshot();
  });
});
