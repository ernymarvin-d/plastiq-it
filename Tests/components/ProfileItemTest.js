import "react-native";
import React from "react";
import ProfileItem from "../../src/components/ProfileItem";
import { shallow } from "enzyme";

test("ProfileItem component renders correctly", () => {
  const data = {
    btn_name: "demo button"
  };
  const component = shallow(<ProfileItem data={data} />);
  expect(component).toMatchSnapshot();
});
