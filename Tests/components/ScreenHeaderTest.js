import React from "react";
import { shallow } from "enzyme";
import ScreenHeader from "../../src/components/ScreenHeader";

describe("Testing ScreenHeader", () => {
  it("renders as expected", () => {
    const component = shallow(<ScreenHeader title="Demo" />);

    expect(component).toMatchSnapshot();
  });
});
