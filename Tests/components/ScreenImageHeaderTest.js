import React from "react";
import { shallow } from "enzyme";
import ScreenImageHeader from "../../src/components/ScreenImageHeader";

describe("Testing ScreenImageHeader", () => {
  it("renders as expected", () => {
    const component = shallow(<ScreenImageHeader />);

    expect(component).toMatchSnapshot();
  });
});
