import "react-native";
import React from "react";
import renderer from "react-test-renderer";
import SectionHeader from "../../src/components/SectionHeader";

test("SectionHeader component renders correctly", () => {
  const info = {
    title: "demo title"
  };
  const tree = renderer.create(<SectionHeader info={info} />).toJSON();
  expect(tree).toMatchSnapshot();
});
