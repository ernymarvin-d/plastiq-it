import React from "react";
import { shallow } from "enzyme";
import SignUpScreenHeader from "../../src/components/SignUpScreenHeader";

describe("Testing SignUpScreenHeader", () => {
  it("renders as expected", () => {
    const component = shallow(<SignUpScreenHeader title="Demo" index={1} />);

    expect(component).toMatchSnapshot();
  });
});
