import React from "react";
import { shallow } from "enzyme";
import StaticImageCard from "../../src/components/StaticImageCard";

describe("Testing StaticImageCard", () => {
  it("renders as expected", () => {
    const component = shallow(<StaticImageCard data={{}} />);

    expect(component).toMatchSnapshot();
  });
});
