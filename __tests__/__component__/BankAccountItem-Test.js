import React from "react";
import renderer from "react-test-renderer";
import BankAccountItem from "../../src/components/BankAccountItem";

test("BankAccountItem snapshot renders correctly", () => {
  const item = {
    account_name: "test name",
    provider_name: "test provider",
    account_type: "test account type",
    provider_logo:
      "https://plastiq.it/wp-content/uploads/elementor/thumbs/cropped-PLASTIQ-LOGO-02-oh4n44xis1aolxr4fpcryxxgznfgatvlrad8xe38jk.png"
  };
  const tree = renderer.create(<BankAccountItem item={item} />).toJSON();
  expect(tree).toMatchSnapshot();
});
