package com.plastiq_mobile_rn;

import android.app.Application;
import android.util.Log;

import com.facebook.react.PackageList;
import com.facebook.hermes.reactexecutor.HermesExecutorFactory;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.ReactApplication;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;

import com.appboy.Appboy;
import com.appboy.AppboyLifecycleCallbackListener;
import com.appboy.configuration.AppboyConfig;
import com.appboy.support.AppboyLogger;

import io.branch.rnbranch.RNBranchModule;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import io.invertase.firebase.perf.RNFirebasePerformancePackage;

import java.util.List;
import io.radar.sdk.Radar;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for example:
      // packages.add(new MyReactNativePackage());
      packages.add(new RNFirebaseCrashlyticsPackage());
      packages.add(new RNFirebasePerformancePackage());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);

    // Initialize braze sdk
    AppboyLogger.setLogLevel(Log.VERBOSE);
    configureAppboyAtRuntime();
    registerActivityLifecycleCallbacks(new AppboyLifecycleCallbackListener());

    // Initialize the Branch object
    RNBranchModule.getAutoInstance(this);

    // Initialize Radar SDK
    configureRadar();
  }

  private void configureRadar() {
    String publishableKey = "prj_live_pk_1b827d3a5bc8bc0e38648c73c3f3ccc23c0a29ca";
    if (BuildConfig.DEBUG) {
      // do something for a debug build
      publishableKey = "prj_test_pk_25e5068af604ed754cf9ada0db8c59f896ed5ca4";
    }
    Radar.initialize(publishableKey);
  }

  private void configureAppboyAtRuntime() {
    // List<String> localeToApiKeyMapping = new ArrayList<>();
    // localeToApiKeyMapping.add("customLocale, customApiKeyForThatLocale");
    // localeToApiKeyMapping.add("fr_NC, anotherAPIKey");
    
    String braze_api_key = "905edc19-eeb5-4ae1-9baa-142754eae402";
    if (BuildConfig.DEBUG) {
      // do something for a debug build
      braze_api_key = "5abfc092-a2de-4785-a89d-5f70f6809a2d";
    }

    // Resources resources = getResources();
    AppboyConfig appboyConfig = new AppboyConfig.Builder()
        .setApiKey(braze_api_key)
        // .setIsFirebaseCloudMessagingRegistrationEnabled(false)
        // .setAdmMessagingRegistrationEnabled(false)
        // .setSessionTimeout(11)
        // .setHandlePushDeepLinksAutomatically(true)
        // .setSmallNotificationIcon(resources.getResourceEntryName(R.drawable.ic_launcher_hello_appboy))
        // .setLargeNotificationIcon(resources.getResourceEntryName(R.drawable.ic_launcher_hello_appboy))
        // .setTriggerActionMinimumTimeIntervalSeconds(5)
        // .setIsLocationCollectionEnabled(false)
        // .setNewsfeedVisualIndicatorOn(true)
        // .setDefaultNotificationAccentColor(0xFFf33e3e)
        // .setLocaleToApiMapping(localeToApiKeyMapping)
        // .setBadNetworkDataFlushInterval(120)
        // .setGoodNetworkDataFlushInterval(60)
        // .setGreatNetworkDataFlushInterval(10)
        .build();
    Appboy.configure(this, appboyConfig);
  }
}
