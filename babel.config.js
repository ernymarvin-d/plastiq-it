module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    [
      require.resolve("babel-plugin-module-resolver"),
      {
        root: ["./src/"],
        cwd: "babelrc",
        alias: {
          "@screens": "./src/screens",
          "@stylesheet": "./src/stylesheet",
          "@navigation": "./src/navigation",
          "@store": "./src/store",
          service: "./src/service",
          "@service": "./src/service",
          "@assets": "./assets",
          "@components": "./src/components"
        }
      }
    ]
  ]
};
