const upgradeApp = async () => {
  const upgradeBtn = await element(by.traits.UIAccessibilityTraitButton);
  await upgradeBtn.tap();
};

const getStarted = async () => {
  const getStartedBtn = await element(by.id("get-started-btn"));
  await getStartedBtn.tap();
};

const loginWithEmail = async email => {
  try {
    const emailInput = await element(by.id("login-email-input"));

    await emailInput.tap();
    await emailInput.typeText(email);

    const loginBtn = await element(by.id("login-email-btn"));

    // await loginBtn.tap(); // to close the keyboard
    await loginBtn.tap(); // to start the authentication process
  } catch (e) {
    console.log("A sign in has not been done.", e);
  }
};

const loginWithWrongEmail = async () => await loginWithEmail("t1@test.com");
const loginWithRightEmail = async () =>
  await loginWithEmail("philip@plastiq.it");

const loginWithPassword = async password => {
  try {
    const passwordInput = await element(by.id("login-password-input"));

    await passwordInput.tap();
    await passwordInput.typeText(password);

    const loginPasswordBtn = await element(by.id("login-password-btn"));

    await loginPasswordBtn.tap(); // to start the authentication process
  } catch (e) {
    console.log("Error happened trying password.", e);
  }
};

const loginWithWrongPassword = async () => await loginWithPassword("T");
const loginWithRightPassword = async () => await loginWithPassword("Abcd1992");

const enterPasscodeNumber = async number => {
  const passcode_id = `passcode_${number}`;
  const passcodeNumberBtn = await element(by.id(passcode_id));
  await passcodeNumberBtn.tap();
};

const enterRightPasscode = async () => {
  await enterPasscodeNumber("1");
  await enterPasscodeNumber("1");
  await enterPasscodeNumber("1");
  await enterPasscodeNumber("1");
};

const logOut = async () => {
  const logOutBtn = await element(by.id("logout-btn"));
  await logOutBtn.tap();
};

describe("Authentication flow", () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  // afterEach(async () => {
  //   try {
  //     await logOut();
  //   } catch (e) {}
  // });
  it("should show Landing screen", async () => {
    // const appboy_In_app_message_Not_now_Btn = await element(by.type('ABKInAppMessageUIButton').and(by.label('Not now')));
    // await appboy_In_app_message_Not_now_Btn.tap();
    // const appboy_In_app_message_Upgrade_now_Btn = await element(by.type('ABKInAppMessageUIButton').and(by.label('UPGRADE NOW')));
    // await appboy_In_app_message_Upgrade_now_Btn.tap();

    let appboy_In_app_message_Close_Btn;
    if (device.getPlatform() === "ios") {
      appboy_In_app_message_Close_Btn = await element(
        by.type("UIButton").and(by.label("Close In App Message"))
      );
      await appboy_In_app_message_Close_Btn.tap();
    }

    await expect(element(by.id("get-started-btn"))).toBeVisible();
  });

  it('should show SignInEmailScreen after tapping "Get Started"', async () => {
    await getStarted();
    await expect(element(by.id("login-email-btn"))).toBeVisible();
    // TODO: Assert showing SignIn Email Screen.
  });

  it('should show SignInPasswordScreen after tapping "Next" with correct email', async () => {
    await getStarted();
    await loginWithRightEmail();
    // TODO: Assert showing SignIn Password Screen.
  });

  it('should show PasscodeScreen after tapping "Sign In" with correct password', async () => {
    await getStarted();
    await loginWithRightEmail();
    await loginWithRightPassword();
    // TODO: Assert showing Passcode Screen.
  });

  it("should show Home screen after entering correct passcode", async () => {
    // await getStarted();
    // await loginWithRightEmail();
    // await loginWithRightPassword();
    await enterRightPasscode();
    // TODO: Assert showing Home Screen.
  });
});
