/* eslint-disable jest/require-top-level-describe */
/* eslint-disable jest/no-jasmine-globals */
/* eslint-disable import/no-extraneous-dependencies */
import detox from "detox";
import adapter from "detox/runners/jest/adapter";
import specReporter from "detox/runners/jest/specReporter";
import json from "../package.json";

const { detox: config } = json;
// Set the default timeout
jest.setTimeout(120000);

jasmine.getEnv().addReporter(adapter);

// This takes care of generating status logs on a per-spec basis. By default, jest only reports at file-level.
// This is strictly optional.
jasmine.getEnv().addReporter(specReporter);

beforeAll(async () => {
  await detox.init(config);
}, 300000);

beforeEach(async () => {
  await adapter.beforeEach();
});

afterAll(async () => {
  await adapter.afterAll();
  await detox.cleanup();
});
