/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
#import <Firebase.h>
#import <RNBranch/RNBranch.h>
#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "Appboy-iOS-SDK/AppboyKit.h"
#import "IDFADelegate.h"
#import <RadarSDK/RadarSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [FIRApp configure];
  // Uncomment this line to use the test key instead of the live one.
  #ifdef DEBUG
  [RNBranch useTestInstance];
  [RNBranch setDebug];
//  [[Branch getInstance] validateSDKIntegration];
  #endif // DEBUG
  
  NSString *braze_api_key = @"2e804add-a203-4418-83e6-e07208204fc3";
  NSString *radar_publishable_key = @"prj_live_pk_1b827d3a5bc8bc0e38648c73c3f3ccc23c0a29ca";
  [RNBranch initSessionWithLaunchOptions:launchOptions isReferrable:YES]; // <-- add this
  NSURL *jsCodeLocation;
  
  #ifdef DEBUG
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  braze_api_key = @"0c07fd1c-2abc-413d-9fcc-f00eb885d3b4";
  radar_publishable_key = @"prj_test_pk_25e5068af604ed754cf9ada0db8c59f896ed5ca4";
  #else
  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
  #endif
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"plastiq_mobile_rn"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  NSMutableDictionary *appboyOptions = [NSMutableDictionary dictionary];
  // Enabling Automatic Location Tracking
//  appboyOptions[ABKEnableAutomaticLocationCollectionKey] = @(YES);
//  appboyOptions[ABKEnableGeofencesKey] = @(YES);
  
  // IDFA tracking
  IDFADelegate *idfaDelegate = [[IDFADelegate alloc] init];
  appboyOptions[ABKIDFADelegateKey] = idfaDelegate;
  
  appboyOptions[ABKInAppMessageControllerDelegateKey] = self;
  
  [Appboy startWithApiKey:braze_api_key
         inApplication:application
     withLaunchOptions:launchOptions
    withAppboyOptions:appboyOptions];
  [Appboy sharedInstance].inAppMessageController.delegate = self;
  
  [Radar initializeWithPublishableKey:radar_publishable_key];
  
  NSLog(@"Appboy device identifier is %@", [[Appboy sharedInstance] getDeviceId]);
  
  [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
  return YES;
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
  NSLog(@"applicationWillEnterForeground");
  if (@available(iOS 10.0, *)) {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler: ^(UNNotificationSettings * _Nonnull settings) {      
      if (settings.authorizationStatus == UNAuthorizationStatusNotDetermined || settings.authorizationStatus == UNAuthorizationStatusDenied) {
        // If we have not shown the push prompt on this device before, then show the in-app message
        self.notificationStatus = NO;
      } else { // If we have already shown the push prompt, discard the in-app message
        self.notificationStatus = YES;
      }
    }];
  }
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  [[Appboy sharedInstance] registerApplication:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  [[Appboy sharedInstance] registerApplication:application didReceiveRemoteNotification:userInfo];
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  NSLog(@"deviceToken = %@", deviceToken);
  [[Appboy sharedInstance] registerDeviceToken:deviceToken];
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

// Check if the key-value pair is “Push Primer” and not null
- (ABKInAppMessageDisplayChoice)beforeInAppMessageDisplayed:(ABKInAppMessage *)inAppMessage {
  printf("inAppMessage = %s \n", inAppMessage);
//  if (inAppMessage != nil && [inAppMessage objectForKey:@"Push Primer"] !=[NSNull null] && [[inAppMessage objectForKey:@"Push Primer"] length] > 0 ) {
//    NSString *value = [NSString stringWithFormat:@"%@", [inAppMessage objectForKey:@"Push Primer"]];
    if (@available(iOS 10.0, *)) {
      if (self.notificationStatus) {
        printf("braze step registered \n");
        return ABKDiscardInAppMessage;
      } else {
        printf("braze step not registered \n");
        return ABKDisplayInAppMessageNow;
      }
    } else {
      // If not using the UserNotifications framework
      UIUserNotificationSettings *notificationSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
      if (!notificationSettings.types) {
        return ABKDisplayInAppMessageNow;
      } else {
        return ABKDiscardInAppMessage;
      }
    }
}

// Deep link handler:
// deprecated method
/*- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
  NSString *urlString = url.absoluteString.stringByRemovingPercentEncoding;
  printf("braze urlString = %s \n", urlString.UTF8String);
  if ([urlString isEqualToString:@"plastiq://push-primer"]) {
    if (@available(iOS 10.0, *)) {
      UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
      [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        [[Appboy sharedInstance] pushAuthorizationFromUserNotificationCenter:granted];
      }];
      center.delegate = self;
      [center setNotificationCategories:[ABKPushUtils getAppboyUNNotificationCategorySet]];
      [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
      // If not using the UserNotifications framework
      UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:[ABKPushUtils getAppboyUIUserNotificationCategorySet]];
      UIApplication *sharedApplication = [UIApplication sharedApplication];
      [sharedApplication registerUserNotificationSettings:settings];
      [sharedApplication registerForRemoteNotifications];
    }
    return YES;
    
    NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:settingsURL];
  }
  
  return YES;
}*/

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
  NSString *urlString = url.absoluteString.stringByRemovingPercentEncoding;
  printf("branch.io urlString = %s \n", urlString.UTF8String);
  
  if ([urlString isEqualToString:@"plastiq.it://push-notification"]) { // authorization for push notification
    if (@available(iOS 10.0, *)) {
      UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
      [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        // if we don't already know the authorisation status, we can defer calling requestAuthorizationWithOptions until after the soft push prompt is shown
        // if (settings.authorizationStatus != UNAuthorizationStatusNotDetermined) {
        
        // If authorization has already been requested, follow the default steps
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
          [[Appboy sharedInstance] pushAuthorizationFromUserNotificationCenter:granted];
        }];
        center.delegate = self;
        [center setNotificationCategories:[ABKPushUtils getAppboyUNNotificationCategorySet]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
          [[UIApplication sharedApplication] registerForRemoteNotifications];
        });
        
        // }
      }];
    } else {
      // If not using the UserNotifications framework
      UIApplication *sharedApplication = [UIApplication sharedApplication];
      UIUserNotificationSettings *notificationSettings = [sharedApplication currentUserNotificationSettings];
      if (notificationSettings.types) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:[ABKPushUtils getAppboyUIUserNotificationCategorySet]];
        [sharedApplication registerUserNotificationSettings:settings];
        [sharedApplication registerForRemoteNotifications];
      }
    }
  } else if ([urlString isEqualToString:@"plastiq.it://location-authorization"]) { // location authorization
    self.locationManager = [[CLLocationManager alloc] init];
    //  self.locationManager.delegate = self;
    [self.locationManager requestAlwaysAuthorization];
  }
  
  return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
  return [RNBranch continueUserActivity:userActivity];
}

# pragma mark - UNUserNotificationCenterDelegate
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
  [[Appboy sharedInstance] userNotificationCenter:center didReceiveNotificationResponse:response withCompletionHandler:completionHandler];
  NSLog(@"Application delegate method userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: is called with user info: %@", response.notification.request.content.userInfo);
  
  if ([ABKPushUtils isAppboyUserNotification:response]) {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"User notification was sent from Braze, clearing badge number.");
  }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
  NSLog(@"Application delegate method userNotificationCenter:willPresentNotification:withCompletionHandler: is called.");
  completionHandler(UNNotificationPresentationOptionAlert);
}

@end
