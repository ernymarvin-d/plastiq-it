import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from "react-native";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import Modal from "react-native-modal";
import { connect } from "react-redux";
import { getBankAccounts, deleteBankAccount } from "@store/account/actions";
import styles from "./styles/BankAccountItemStyle";

class BankAccountItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor(props) {
    super(props);

    this.state = {
      is_confirm_modal_shown: false
    };
  }

  handleDeleteBankAccount = item => {
    this.setState({ is_confirm_modal_shown: false });
    setTimeout(() => {
      this.props.deleteBankAccount(item.account_id, this.reloadBankAccounts);
    }, 500);
  };

  reloadBankAccounts = () => {
    this.props.getBankAccounts();
  };

  displayConfirmModal = () => {
    this.setState({ is_confirm_modal_shown: true });
  };

  render() {
    const { item } = this.props;

    return (
      <View style={styles.container}>
        <Image style={styles.circle} source={{ uri: item.provider_logo }} />
        <View style={styles.textContainer}>
          <Text style={styles.providerName}>{item.provider_name}</Text>
          <Text style={styles.accountName}>{item.account_name}</Text>
          <Text style={styles.accountType}>{item.account_type}</Text>
        </View>
        <TouchableOpacity
          style={styles.deleteBtn}
          onPress={this.displayConfirmModal}
        >
          <FontAwesome5Icon name="cog" size={20} color="gray" />
        </TouchableOpacity>

        <Modal isVisible={this.state.is_confirm_modal_shown} transparent>
          <View style={styles.modalContainer}>
            <Text style={styles.modalHeader}>Delete Bank Account</Text>

            <Text style={styles.modalBody}>
              Are you sure you want to delete the account "{item.account_name}"?
            </Text>

            <View style={styles.btnContainer}>
              <TouchableOpacity
                style={styles.modalBtn}
                onPress={() => this.handleDeleteBankAccount(item)}
              >
                <Text style={styles.btnText}>OK</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.modalBtn}
                onPress={() => this.setState({ is_confirm_modal_shown: false })}
              >
                <Text style={styles.btnText}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapState = state => {
  const { bank_accounts } = state;

  return {
    // bank_accounts_deleting: bank_accounts.bank_accounts_deleting
  };
};

const mapDispatch = dispatch => ({
  getBankAccounts: () => dispatch(getBankAccounts()),
  deleteBankAccount: (account_id, callback) =>
    dispatch(deleteBankAccount(account_id, callback))
});

export default connect(mapState, mapDispatch)(BankAccountItem);
