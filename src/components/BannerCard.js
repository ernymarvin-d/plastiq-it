import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { TouchableOpacity, Image, Alert } from "react-native";
import { withNavigation } from "react-navigation";
import styles from "./styles/BannerCardStyle";

class BannerCard extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  handleOnPressCard = data => {
    if (this.props.onPress) {
      const { id } = data;

      const { ReactAppboy } = global;
      ReactAppboy.logContentCardImpression(id);
      ReactAppboy.logContentCardClicked(id);
      this.props.onPress();
    } else {
      const { url, id } = data;

      const { ReactAppboy } = global;
      ReactAppboy.logContentCardImpression(id);

      if (url != null) {
        ReactAppboy.logContentCardClicked(id);
        this.navigateToWebViewScreen(url);
      } else {
        Alert.alert("No URL to refer the card");
      }
    }
  };

  navigateToWebViewScreen = tracking_url => {
    this.props.navigation.navigate("ShopWebView", {
      link: tracking_url,
      prevScreen: "Home"
    });
  };

  render() {
    const { image } = this.props.data;

    if (image != null) {
      img_source = { uri: image };
    }

    let image_style = [styles.image];
    if (this.props.bank_detail_screen) {
      image_style = [styles.image, styles.image2];
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.handleOnPressCard(this.props.data)}
      >
        <Image style={image_style} source={img_source} />
      </TouchableOpacity>
    );
  }
}

export default withNavigation(BannerCard);
