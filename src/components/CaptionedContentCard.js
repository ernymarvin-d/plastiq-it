import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity, Alert } from "react-native";
import { withNavigation } from "react-navigation";
import styles from "./styles/CaptionedContentCardStyle";

class CaptionedContentCard extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  handleOpenWebURL = async url => {
    if (url != null) {
      this.navigateToWebViewScreen(url);
    } else {
      Alert.alert("No URL to refer the card");
    }
  };

  handleOnPressCard = data => {
    const { url, id } = data;

    const { ReactAppboy } = global;
    ReactAppboy.logContentCardImpression(id);

    if (url != null) {
      ReactAppboy.logContentCardClicked(id);
      this.navigateToWebViewScreen(url);
    } else {
      Alert.alert("No URL to refer the card");
    }
  };

  navigateToWebViewScreen = tracking_url => {
    this.props.navigation.navigate("ShopWebView", {
      link: tracking_url,
      prevScreen: "Home"
    });
  };

  render() {
    const { title, cardDescription, domain, url, image } = this.props.data;

    if (image != null) {
      img_source = { uri: image };
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.handleOnPressCard(this.props.data)}
      >
        <Image style={styles.image} source={img_source} />
        <View style={styles.textContainer}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.content}>{cardDescription}</Text>
          <Text style={styles.link} onPress={() => this.handleOpenWebURL(url)}>
            {domain}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
export default withNavigation(CaptionedContentCard);
