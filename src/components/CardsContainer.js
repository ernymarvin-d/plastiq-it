import React, { Component } from "react";
// import PropTypes from "prop-types";
import { View } from "react-native";
import { withNavigation } from "react-navigation";
import Carousel from "react-native-snap-carousel";

import CaptionedContentCard from "./CaptionedContentCard";
import BannerCard from "./BannerCard";
import StaticImageCard from "./StaticImageCard";

import styles from "./styles/CardsContainerStyle";
import { Metrics } from "../themes";

class CardsContainer extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor(props) {
    super(props);
  }

  _renderCaptionedContentCard = ({ item }) => (
    <CaptionedContentCard data={item} />
  );

  _navigateOffersScreen = () => {
    this.props.navigation.navigate("Offers");
  };

  render() {
    const { bannerCards, carouselCards, staticCards } = this.props;
    const banner_cards_count = bannerCards.length;
    const static_cards_count = staticCards.length;
    return (
      <View style={styles.container}>
        {banner_cards_count > 0 && <BannerCard data={bannerCards[0]} />}
        <View style={{ height: 20 }} />
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          data={carouselCards}
          renderItem={this._renderCaptionedContentCard}
          sliderWidth={Metrics.sliderWidth}
          itemWidth={Metrics.itemWidth}
        />
        {static_cards_count > 0 && (
          <View>
            <View style={{ height: 20 }} />
            {/* There is one card that takes you to the offers screen. */}
            {staticCards[0] != undefined && (
              <StaticImageCard
                data={staticCards[0]}
                onPress={
                  staticCards[0].extras.sub_kind != undefined &&
                  staticCards[0].extras.sub_kind == "offer"
                    ? this._navigateOffersScreen
                    : null
                }
              />
            )}
            <View style={{ height: 20 }} />
            {staticCards[1] != undefined && (
              <StaticImageCard
                data={staticCards[1]}
                onPress={
                  staticCards[1].extras.sub_kind != undefined &&
                  staticCards[1].extras.sub_kind == "offer"
                    ? this._navigateOffersScreen
                    : null
                }
              />
            )}
            <View style={{ height: 20 }} />
            {staticCards[2] != undefined && (
              <StaticImageCard
                data={staticCards[2]}
                onPress={
                  staticCards[2].extras.sub_kind != undefined &&
                  staticCards[2].extras.sub_kind == "offer"
                    ? this._navigateOffersScreen
                    : null
                }
              />
            )}
          </View>
        )}
      </View>
    );
  }
}

export default withNavigation(CardsContainer);
