import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { Image, TouchableOpacity, TouchableHighlight } from "react-native";
import { connect } from "react-redux";
import { getOffers, setSearchKey } from "@store/offer/actions";
import styles from "./styles/CategoryItemStyle";

class CategoryItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  handleOnPress = category => {
    this.props.setSearchKey(category);

    const params = {
      category,
      search: category
    };

    this.props.getOffers(params);
  };

  render() {
    const { category, source } = this.props;

    return (
      <TouchableHighlight
        style={styles.btn}
        underlayColor="#924bba"
        onPress={() => this.handleOnPress(category)}
      >
        <Image style={styles.category} source={source} />
      </TouchableHighlight>
    );
  }
}

const mapState = state => {
  return {};
};

const mapDispatch = dispatch => ({
  getOffers: params => dispatch(getOffers(params)),
  setSearchKey: search_key => dispatch(setSearchKey(search_key))
});

export default connect(mapState, mapDispatch)(CategoryItem);
