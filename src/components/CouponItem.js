import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text } from "react-native";
import styles from "./styles/CouponItemStyle";

export default class CouponItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render() {
    const { item } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.textTitle}>Coupon Code</Text>
        <Text style={styles.textContent}>{item.code}</Text>
        <Text style={styles.textTitle}>Description</Text>
        <Text style={styles.textContent}>{item.description}</Text>
        {item.end_date != null && (
          <View>
            <Text style={styles.textTitle}>End Date</Text>
            <Text style={styles.textContent}>{item.end_date}</Text>
          </View>
        )}
      </View>
    );
  }
}
