import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, Image } from "react-native";
import styles from "./styles/MerchantItemStyle";

export default class MerchantItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor(props) {
    super(props);
  }

  capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  render() {
    const { item } = this.props;

    const status = this.capitalizeFirstLetter(item.status);
    return (
      <View style={styles.container}>
        <Image style={styles.merchant_logo} source={{ uri: item.avatar_url }} />
        <View style={styles.centerContainer}>
          <Text style={styles.user}>{item.merchant_name}</Text>
          <Text style={styles.status}>{status}</Text>
          <Text style={styles.date}>{item.refined_transaction_date}</Text>
        </View>
        <Text style={styles.amount}>${item.amount}</Text>
      </View>
    );
  }
}
