import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from "react-native";
import { withNavigation } from "react-navigation";

import styles from "./styles/OfferItemStyle";

class OfferItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  handleOnPressShopNow = tracking_url => {
    // this.props.handleShopNow(tracking_url);
    this.props.navigation.navigate("ShopWebView", {
      link: tracking_url,
      prevScreen: "Offers",
      title: "Shop Now"
    });
  };

  handleOnPressOfferItem = ({ source, merchant_id, name }) => {
    // console.log("/handleOnPressOfferItem merchant_id", merchant_id);
    this.props.navigation.navigate("Merchant", {
      source,
      merchant_id,
      name
    });
  };

  render() {
    const {
      merchant_id,
      source,
      avatar_url,
      name,
      commission_rate,
      tracking_url,
      commission_type_symbol
    } = this.props.item;

    let percent_or_dollar_comment = `Earn up to ${commission_rate}% Cashback`;
    if (commission_type_symbol == "$") {
      percent_or_dollar_comment = `Earn up to $${commission_rate} Cashback`;
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() =>
          this.handleOnPressOfferItem({ source, merchant_id, name })
        }
      >
        <Image style={styles.avatar} source={{ uri: avatar_url }} />
        <View style={styles.centerContainer}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.percent}>{percent_or_dollar_comment}</Text>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity
            style={styles.shopBtn}
            onPress={() => this.handleOnPressShopNow(tracking_url)}
          >
            <Text style={styles.shopBtnText}>Shop</Text>
            <Text style={styles.shopBtnText}>Now</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
}

export default withNavigation(OfferItem);
