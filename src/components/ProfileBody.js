import React, { Component } from "react";
// import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Alert,
  ActivityIndicator,
  Keyboard,
  FlatList
} from "react-native";
import Modal from "react-native-modal";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";

import { withdrawCashback } from "@store/cashback/actions";

import ProfileItem from "./ProfileItem";
import StaticImageCard from "./StaticImageCard";
import { Metrics } from "../themes";
import styles from "./styles/ProfileBodyStyle";

const profile_buttons = [
  {
    kind: "btn_account",
    btn_name: "Account"
  },
  {
    kind: "btn_bank_details",
    btn_name: "Bank Details"
  },
  {
    kind: "btn_cashbacks",
    btn_name: "Cashbacks"
  },
  {
    kind: "btn_withdraw",
    btn_name: "Withdraw"
  },
  {
    kind: "btn_support",
    btn_name: "Support"
  }
];

class ProfileBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal_available_withdrawal: false,
      modal_no_available_withdrawal: false,
      is_empty_fields_existed: false,
      withdrawal_amount: "",
      bank_account_name: "",
      bsb_number: "",
      bank_account_number: "",
      active_banner_index: 0
    };
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({ keyboardHeight: e.endCoordinates.height });
  };

  _keyboardDidHide = e => {
    this.setState({ keyboardHeight: 0 });
  };

  handleNavigateToAccountDetails = () => {
    this.props.navigation.navigate("AccountDetails");
  };

  handleNavigateToBankDetails = () => {
    this.props.navigation.navigate("BankDetails");
  };

  handleNavigateToCashbacks = () => {
    this.props.navigation.navigate("Cashbacks");
  };

  displayWithdrawalRequestModal = () => {
    const cashback_amount = this.props.cashback_credit.amount;

    if (cashback_amount < 10) {
      // if cashback amount is less than $10, display none-available modal.
      this.setState({ modal_no_available_withdrawal: true });
    } else {
      // if greater than $10, display available modal for entering detailed info.
      this.setState({ modal_available_withdrawal: true });
    }
  };

  handleWithdrawalRequest = () => {
    const {
      withdrawal_amount,
      bank_account_name,
      bsb_number,
      bank_account_number
    } = this.state;

    if (
      withdrawal_amount == "" ||
      bank_account_name == "" ||
      bsb_number == "" ||
      bank_account_number == ""
    ) {
      this.setState({ is_empty_fields_existed: true });
    } else {
      const property = {
        amount: withdrawal_amount,
        bank_account_name,
        bsb_number,
        bank_account_number
      };

      console.log("/handleWithdrawalRequest property", property);

      this.props.withdrawCashback(property, this.completeWithdrawal);
    }
  };

  completeWithdrawal = () => {
    this.setState({ modal_available_withdrawal: false });
  };

  handleNavigateToSupport = () => {
    this.props.navigation.navigate("Support");
  };

  async logout() {
    await AsyncStorage.clear();
    this.props.navigation.navigate("SignInEmailScreen");
  }

  _logout = () => {
    Alert.alert(
      "",
      "Are you sure you want to logout?",
      [
        {
          text: "Cancel",
          onPress: () => false,
          style: "cancel"
        },
        { text: "OK", onPress: () => this.logout() }
      ],
      { cancelable: true }
    );
  };

  _renderProfileButton = ({ item }) => {
    let operation = null;
    switch (item.kind) {
      case "btn_account":
        operation = this.handleNavigateToAccountDetails;
        break;
      case "btn_bank_details":
        operation = this.handleNavigateToBankDetails;
        break;
      case "btn_cashbacks":
        operation = this.handleNavigateToCashbacks;
        break;
      case "btn_withdraw":
        operation = this.displayWithdrawalRequestModal;
        break;
      case "btn_support":
        operation = this.handleNavigateToSupport;
        break;
    }
    return <ProfileItem data={item} onPress={operation} />;
  };

  render() {
    const {
      account_static_cards,
      cashbacks_loading,
      request_cashback_errors
    } = this.props;

    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              data={profile_buttons}
              renderItem={this._renderProfileButton}
              sliderWidth={Metrics.sliderWidth}
              itemWidth={Metrics.itemWidth}
              onSnapToItem={index =>
                this.setState({ active_banner_index: index })
              }
              containerCustomStyle={styles.carouselContainer}
            />

            <Pagination
              dotsLength={profile_buttons.length}
              activeDotIndex={this.state.active_banner_index}
              containerStyle={styles.paginationContainer}
              dotStyle={styles.activeDot}
              inactiveDotStyle={
                {
                  // Define styles for inactive dots here
                }
              }
              inactiveDotOpacity={0.2}
              inactiveDotScale={0.8}
            />

            {account_static_cards[0] != undefined && (
              <StaticImageCard data={account_static_cards[0]} />
            )}

            {account_static_cards[1] != undefined && (
              <View>
                <View style={{ height: 20 }} />
                <StaticImageCard data={account_static_cards[1]} />
              </View>
            )}

            <View style={styles.logoutBtnContainer}>
              <TouchableOpacity
                style={styles.logOutBtn}
                onPress={() => {
                  this._logout();
                }}
                testID="logout-btn"
              >
                <Text style={styles.btnText}>Log Out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <Modal
          isVisible={this.state.modal_available_withdrawal}
          transparent
          animationType="none"
        >
          <View style={styles.modalContainer}>
            <Text style={styles.modalHeader}>Withdrawal Request</Text>

            <View style={styles.inputContainer}>
              <Text style={styles.errorLabel}>
                {this.state.is_empty_fields_existed ? "Fill empty fields!" : ""}
              </Text>

              <View style={styles.formInput}>
                <Text style={styles.textLabel}>Withdrawal Amount:</Text>
                <TextInput
                  style={{
                    ...styles.textValueEdit,
                    borderColor:
                      this.state.is_empty_fields_existed &&
                      this.state.withdrawal_amount == ""
                        ? "red"
                        : "transparent"
                  }}
                  onChangeText={val =>
                    this.setState({
                      withdrawal_amount: val,
                      is_empty_fields_existed: false
                    })
                  }
                  value={this.state.withdrawal_amount}
                  keyboardType="decimal-pad"
                />
                <FlatList
                  data={request_cashback_errors.amount}
                  renderItem={({ item }) => (
                    <Text style={styles.errorText}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View style={styles.formInput}>
                <Text style={styles.textLabel}>Bank Account Name:</Text>
                <TextInput
                  style={{
                    ...styles.textValueEdit,
                    borderColor:
                      this.state.is_empty_fields_existed &&
                      this.state.bank_account_name == ""
                        ? "red"
                        : "transparent"
                  }}
                  onChangeText={val =>
                    this.setState({
                      bank_account_name: val,
                      is_empty_fields_existed: false
                    })
                  }
                  value={this.state.bank_account_name}
                />
                <FlatList
                  data={request_cashback_errors.bank_account_name}
                  renderItem={({ item }) => (
                    <Text style={styles.errorText}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View style={styles.formInput}>
                <Text style={styles.textLabel}>BSB Number:</Text>
                <TextInput
                  style={{
                    ...styles.textValueEdit,
                    borderColor:
                      this.state.is_empty_fields_existed &&
                      this.state.bsb_number == ""
                        ? "red"
                        : "transparent"
                  }}
                  onChangeText={val =>
                    this.setState({
                      bsb_number: val,
                      is_empty_fields_existed: false
                    })
                  }
                  value={this.state.bsb_number}
                  keyboardType="number-pad"
                />
                <FlatList
                  data={request_cashback_errors.bsb_number}
                  renderItem={({ item }) => (
                    <Text style={styles.errorText}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View style={styles.formInput}>
                <Text style={styles.textLabel}>Bank Account Number:</Text>
                <TextInput
                  style={{
                    ...styles.textValueEdit,
                    borderColor:
                      this.state.is_empty_fields_existed &&
                      this.state.bank_account_number == ""
                        ? "red"
                        : "transparent"
                  }}
                  onChangeText={val =>
                    this.setState({
                      bank_account_number: val,
                      is_empty_fields_existed: false
                    })
                  }
                  value={this.state.bank_account_number}
                  keyboardType="number-pad"
                />
                <FlatList
                  data={request_cashback_errors.bank_account_number}
                  renderItem={({ item }) => (
                    <Text style={styles.errorText}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>

            <View style={styles.modalBtnContainer}>
              <TouchableOpacity
                style={styles.modalBtn}
                onPress={this.handleWithdrawalRequest}
              >
                {cashbacks_loading ? (
                  <ActivityIndicator color="white" />
                ) : (
                  <Text style={styles.modalBtnText}>OK</Text>
                )}
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.modalBtn}
                onPress={() =>
                  this.setState({ modal_available_withdrawal: false })
                }
              >
                <Text style={styles.modalBtnText}>Cancel</Text>
              </TouchableOpacity>
            </View>

            <View style={{ height: this.state.keyboardHeight }} />
          </View>
        </Modal>

        <Modal
          isVisible={this.state.modal_no_available_withdrawal}
          transparent
          animationType="none"
        >
          <View style={styles.modalContainer}>
            <Text style={styles.modalHeader}>Withdrawal Request</Text>

            <Text style={styles.modalBody}>
              A balance must have $10 in approved cashback to be eligible to
              withdraw.
            </Text>

            <TouchableOpacity
              style={styles.modalBtn}
              onPress={() =>
                this.setState({ modal_no_available_withdrawal: false })
              }
            >
              <Text style={styles.modalBtnText}>OK</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapState = state => {
  const { cashback } = state;

  return {
    cashback_credit: cashback.cashback_credit,
    cashbacks_loading: cashback.cashbacks_loading,
    account_static_cards: cashback.account_static_cards,
    request_cashback_errors: cashback.request_cashback_errors
  };
};

const mapDispatch = dispatch => ({
  withdrawCashback: (data, callback) =>
    dispatch(withdrawCashback(data, callback))
});

export default connect(mapState, mapDispatch)(withNavigation(ProfileBody));
