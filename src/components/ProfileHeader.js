import React, { Component } from "react";
// import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";

import MaterialIcons from "react-native-vector-icons/dist/MaterialIcons";
import ImagePicker from "react-native-image-picker";
import { RNS3 } from "react-native-aws3";
import FastImage from "react-native-fast-image";

import styles from "./styles/ProfileHeaderStyle";
import AppConfig from "../config/AppConfig";

class ProfileHeader extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor(props) {
    super(props);

    this.state = {
      options: {
        title: "Select Avatar",
        // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: "images"
        }
      },
      // avatarSource: null,
      s3_uploading: false
    };
  }

  handleEditAvatar = () => {
    ImagePicker.showImagePicker(this.state.options, response => {
      console.log("Image Picker Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        Alert.alert(
          `There are some issues opening the picker. ${response.error}`
        );
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        if (response.type === undefined || response.type === null) {
          Alert.alert("The file is unsupported. Please try again.");
          return;
        }

        this.setState({
          // avatarSource: source,
          s3_uploading: true
        });

        const { id } = this.props.user_detail;

        let user_id = Date.now().toString();
        if (id) {
          user_id = `${id.toString()}_${user_id}`;
        }
        const file_name = `avatar_${user_id}`;

        /**
         * upload the avatar to s3.
         */
        const file = {
          // `uri` can also be a file system path (i.e. file://)
          uri: response.uri,
          name: file_name,
          type: response.type
        };

        const s3_options = AppConfig.aws_s3_options;

        RNS3.put(file, s3_options).then(response => {
          this.setState({
            s3_uploading: false
          });
          console.log("AWS S3 response", response);
          if (response.status !== 201) {
            Alert.alert("There are some issues uploading avatar to s3.");
            return;
          }
          /**
           * {
           *   postResponse: {
           *     bucket: "your-bucket",
           *     etag : "9f620878e06d28774406017480a59fd4",
           *     key: "uploads/image.png",
           *     location: "https://your-bucket.s3.amazonaws.com/uploads%2Fimage.png"
           *   }
           * }
           */
          const original_location = response.body.postResponse.location;
          const processed_location = original_location.replace("%2F", "/");
          const data = {
            avatar_uri: processed_location
          };

          this.props.updateAvatar(data);
        });
      }
    });
  };

  render() {
    const { user_detail = {} } = this.props;
    let avatar_img = require("@assets/images/auth/avatar/avatar_male.png");

    if (user_detail.avatar_uri) {
      // image from backend server
      avatar_img = { uri: user_detail.avatar_uri };
    } else {
      // image from backend
      if (user_detail.gender) {
        const gender = user_detail.gender.toLowerCase();
        if (gender == "female") {
          avatar_img = require("@assets/images/auth/avatar/avatar_female.png");
        }
      }
    }

    return (
      <View style={styles.container}>
        <View style={styles.avatarContainer}>
          {/* <Image style={styles.avatar}
            source={avatar_img}/> */}
          <FastImage style={styles.avatar} source={avatar_img} />
          <TouchableOpacity
            onPress={this.handleEditAvatar}
            style={styles.editBtn}
          >
            <MaterialIcons name="edit" size={20} color="gray" />
          </TouchableOpacity>
          {(this.props.user_detail_loading || this.state.s3_uploading) && (
            <View style={styles.avatarOverlay}>
              <ActivityIndicator color="#a84bdd" />
            </View>
          )}
        </View>
        <Text style={styles.name}>
          {user_detail.name} {user_detail.last_name}
        </Text>
      </View>
    );
  }
}

const mapState = state => {
  const { auth } = state;
  return {
    user_detail: auth.user_detail,
    user_detail_loading: state.auth.user_detail_loading
  };
};

const mapDispatch = dispatch => ({
  updateAvatar: data => dispatch(updateAvatar(data))
});

export default connect(mapState, mapDispatch)(ProfileHeader);
