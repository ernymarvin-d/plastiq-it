import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from "react-native";
import styles from "./styles/ProfileItemStyle";

export default class ProfileItem extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor(props) {
    super(props);
  }

  handleOnPress = () => {
    this.props.onPress();
  };

  render() {
    const { btn_name } = this.props.data;

    return (
      <TouchableOpacity onPress={this.handleOnPress} style={styles.container}>
        <Text style={styles.info}>{btn_name}</Text>
      </TouchableOpacity>
    );
  }
}
