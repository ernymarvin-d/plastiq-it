import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from "react-native";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import { withNavigation } from "react-navigation";

import styles from "./styles/ScreenHeaderStyle";

class ScreenHeader extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  handleGoBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <TouchableOpacity style={styles.backBtn} onPress={this.handleGoBack}>
            <FontAwesome5Icon name="angle-left" size={30} color="white" />
          </TouchableOpacity>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}

export default withNavigation(ScreenHeader);
