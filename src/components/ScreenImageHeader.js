import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Image, TouchableOpacity } from "react-native";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import { withNavigation } from "react-navigation";

import { PLASTIC_BRAND_LOGO } from "@components/Images";
import styles from "./styles/ScreenImageHeaderStyle";

class ScreenImageHeader extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  handleGoBack = () => {
    // let prevScreen = this.props.navigation.getParam("prevScreen");
    // this.props.navigation.navigate(prevScreen);
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <TouchableOpacity style={styles.backBtn} onPress={this.handleGoBack}>
            <FontAwesome5Icon name="angle-left" size={30} color="white" />
          </TouchableOpacity>
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={PLASTIC_BRAND_LOGO} />
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(ScreenImageHeader);
