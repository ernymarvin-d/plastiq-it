import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text } from "react-native";
import styles from "./styles/SectionHeaderStyle";

class SectionHeader extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render() {
    const { title } = this.props.info;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
}

export default SectionHeader;
