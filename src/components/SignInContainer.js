import React from "react";
import { KeyboardAvoidingView, View, Image, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  scrollView: {
    ...StyleSheet.absoluteFill,
    backgroundColor: "#a64adb",
    alignItems: "center",
    justifyContent: "center"
  },
  imageContainer: {
    width: "100%",
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center",
    backgroundColor: "#a64adb"
  },
  logo: {
    width: "100%",
    resizeMode: "contain"
  },
  bottomView: {
    width: "100%",
    flex: 1,
    flexShrink: 0,
    alignItems: "center",
    backgroundColor: "#8e47c1",
    paddingTop: 25
  }
});

export default ({
  children,
  scrollViewStyle,
  imageStyle,
  style,
  source = require("@assets/images/auth/email/illustration.png"),
  ...props
}) => (
  <KeyboardAvoidingView
    behavior="height"
    style={[styles.scrollView, scrollViewStyle]}
    {...props}
  >
    <View style={[styles.imageContainer, imageStyle]}>
      <Image style={styles.logo} source={source} />
    </View>

    <View style={[styles.bottomView, style]}>{children}</View>
  </KeyboardAvoidingView>
);
