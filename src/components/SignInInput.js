import React from "react";
import { StyleSheet } from "react-native";
import { Input } from "react-native-elements";

export const styles = StyleSheet.create({
  inputViewContainer: {
    marginTop: 24,
    paddingHorizontal: 32
  },
  errorInputViewContainer: {
    borderBottomColor: "red"
  },
  input: {
    fontSize: 15,
    marginLeft: 16
  }
});

export default ({ errors, inputStyle = {}, ...props }) => (
  <Input
    containerStyle={[
      styles.inputViewContainer,
      errors && errors.length > 0 ? styles.errorInputViewContainer : {}
    ]}
    inputStyle={{ ...styles.input, ...inputStyle }}
    errorMessage={errors && errors.length > 0 ? errors[0] : null}
    {...props}
  />
);
