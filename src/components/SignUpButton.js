import React from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-elements";

const styles = StyleSheet.create({
  buttonContainer: {
    width: "100%",
    marginVertical: 32
  },
  button: {
    width: "100%",
    height: 48,
    backgroundColor: "#ecd7f8",
    borderRadius: 0,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 1,
    shadowRadius: 10.0
  },
  title: {
    color: "#a64adb"
  }
});

export default props => (
  <Button
    containerStyle={styles.buttonContainer}
    buttonStyle={styles.button}
    title="Next"
    titleStyle={styles.title}
    {...props}
  />
);
