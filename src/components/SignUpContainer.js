import React from "react";
// import PropTypes from 'prop-types';
import { ScrollView, View, Text, StyleSheet } from "react-native";
import SignUpScreenHeader from "@components/SignUpScreenHeader";

const styles = StyleSheet.create({
  scrollView: {
    ...StyleSheet.absoluteFill,
    backgroundColor: "#8e47c1",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 32
  },
  bottomContainer: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    backgroundColor: "#8e47c1"
  },
  alreadyRegisteredText: {
    fontFamily: "Montserrat",
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 0,
    color: "white",
    marginVertical: 16
  },
  signInText: {
    textDecorationLine: "underline"
  }
});

export default ({ title, index, children, navigation, style }) => (
  <ScrollView
    style={StyleSheet.absoluteFillObject}
    contentContainerStyle={styles.scrollView}
    showsVerticalScrollIndicator={false}
    bounces={false}
    keyboardShouldPersistTaps="handled"
  >
    <SignUpScreenHeader title={title} index={index} />
    <View style={[styles.bottomContainer, style]}>{children}</View>
    <Text style={styles.alreadyRegisteredText}>
      {"Already registered? "}
      <Text
        style={styles.signInText}
        onPress={() => navigation.navigate("SignInEmailScreen")}
      >
        Sign In
      </Text>
    </Text>
  </ScrollView>
);
