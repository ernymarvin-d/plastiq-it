import React from "react";
import { shallow } from "enzyme";
import SignUpContainer from "./SignUpContainer";

describe("SignUpContainer", () => {
  it("renders correctly", () => {
    const component = shallow(<SignUpContainer title="Title" index={1} />);
    expect(component.dive()).toMatchInlineSnapshot(`ShallowWrapper {}`);
  });
});
