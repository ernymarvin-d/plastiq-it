import React from "react";
import { Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  text: {
    color: "red",
    marginTop: 8
  }
});

export default ({ errors = [], style }) => (
  <>
    {errors.map(error => (
      <Text style={[style, styles.text]}>{error}</Text>
    ))}
  </>
);
