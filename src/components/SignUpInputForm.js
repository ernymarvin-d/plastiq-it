import React from "react";
import { View, StyleSheet } from "react-native";
import { Input } from "react-native-elements";

const styles = StyleSheet.create({
  inputForm: {
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 5,
    marginHorizontal: 28,
    paddingHorizontal: 4,
    borderBottomWidth: 1,
    borderColor: "#ba90d8"
  },
  inputFormIcon: {},
  inputContainer: {
    borderColor: "transparent"
  },
  inputText: {
    width: "100%",
    height: 40,
    fontFamily: "Montserrat",
    paddingLeft: 12
  }
});

export default ({ ImageComponent, size = 18, name, ...props }) => (
  <View style={styles.inputForm}>
    <ImageComponent
      style={styles.inputFormIcon}
      size={size}
      name={name}
      color="white"
    />
    <Input
      inputContainerStyle={styles.inputContainer}
      style={styles.inputText}
      returnKeyType="next"
      {...props}
    />
  </View>
);
