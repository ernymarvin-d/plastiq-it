import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { BarPasswordStrengthDisplay } from "react-native-password-strength-meter";
import Ionicons from "react-native-vector-icons/dist/Ionicons";

import { Metrics } from "../themes";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginTop: 24,
    paddingHorizontal: 36
  },
  passwordBarTitle: {
    fontFamily: "Montserrat",
    color: "white"
  },
  passwordBarWrapper: {
    marginTop: 8,
    marginLeft: 0
  },
  criteriaContainer: {
    marginTop: 24,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "white"
  },
  criteriaTitle: {
    fontFamily: "Montserrat",
    fontWeight: "bold",
    color: "white",
    padding: 10
  },
  criteriaRow: {
    flexDirection: "row",
    borderTopColor: "white",
    borderTopWidth: 1,
    padding: 10
  },
  criteriaRowTitle: {
    fontFamily: "Montserrat",
    color: "white",
    marginLeft: 10
  }
});

export default ({ password, criterias }) => (
  <View style={styles.container}>
    <Text style={styles.passwordBarTitle}>Password Strength:</Text>
    <BarPasswordStrengthDisplay
      width={Metrics.passwordStrengthMeterWidth}
      password={password}
      scoreLimit={100}
      minLength={1}
      wrapperStyle={styles.passwordBarWrapper}
    />

    <View style={styles.criteriaContainer}>
      <Text style={styles.criteriaTitle}>Minimum requirements :</Text>
      {criterias.map(({ regex, title }) => (
        <View style={styles.criteriaRow}>
          <Ionicons
            name="md-checkmark-circle"
            size={15}
            color={password.match(regex) ? "lightgreen" : "white"}
          />
          <Text style={styles.criteriaRowTitle}>{title}</Text>
        </View>
      ))}
    </View>
  </View>
);
