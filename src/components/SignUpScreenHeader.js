import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Pagination } from "react-native-snap-carousel";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    backgroundColor: "#a64adb",
    paddingTop: 32
  },
  title: {
    fontFamily: "Montserrat",
    fontSize: 25,
    color: "white",
    marginTop: 15
  },
  paginationContainer: {},
  activeDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: "white"
  },
  inactiveDot: {
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "white"
  }
});

export default ({ title, index }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <Pagination
      dotsLength={3}
      activeDotIndex={index}
      containerStyle={styles.paginationContainer}
      dotStyle={styles.activeDot}
      inactiveDotStyle={styles.inactiveDot}
      inactiveDotOpacity={1}
      inactiveDotScale={1}
    />
  </View>
);
