import React from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-elements";

const styles = StyleSheet.create({
  container: {
    width: "70%",
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 1,
    shadowRadius: 10.0
  },
  button: {
    width: "100%",
    height: 48,
    borderRadius: 24,
    backgroundColor: "#8e47c1",
    marginTop: 32
  }
});

export default ({ containerStyle, buttonStyle, ...props }) => (
  <Button
    type="solid"
    containerStyle={[styles.container, containerStyle]}
    buttonStyle={[styles.button, buttonStyle]}
    {...props}
  />
);
