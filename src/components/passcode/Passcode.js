import React, { useState } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import PasscodeNumber from "./PasscodeNumber";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%"
  },
  circlesContainer: {
    flexDirection: "row",
    justifyContent: "center"
  },
  circle: {
    width: 18,
    height: 18,
    borderWidth: 1.5,
    borderColor: "white",
    borderRadius: 100,
    marginHorizontal: 15
  },
  circleFilled: {
    backgroundColor: "white"
  },
  numberPadContainer: {
    width: "100%",
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    alignContent: "stretch",
    marginTop: 32
  },
  numberPadChildContainer: {
    width: "33%",
    height: 80,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ({ onReset, number, onNumberChanged }) => {
  const circles = [];
  for (let i = 1; i <= 4; i++) {
    circles.push(
      <View
        style={[styles.circle, number.length >= i ? styles.circleFilled : {}]}
      />
    );
  }
  const onPressNumber = digit => onNumberChanged(`${number}${digit}`);
  return (
    <View style={styles.container}>
      <View style={styles.circlesContainer}>{circles}</View>
      <View style={styles.numberPadContainer}>
        <PasscodeNumber number={1} onPress={onPressNumber} />
        <PasscodeNumber number={2} onPress={onPressNumber} />
        <PasscodeNumber number={3} onPress={onPressNumber} />
        <PasscodeNumber number={4} onPress={onPressNumber} />
        <PasscodeNumber number={5} onPress={onPressNumber} />
        <PasscodeNumber number={6} onPress={onPressNumber} />
        <PasscodeNumber number={7} onPress={onPressNumber} />
        <PasscodeNumber number={8} onPress={onPressNumber} />
        <PasscodeNumber number={9} onPress={onPressNumber} />
        <TouchableOpacity
          style={styles.numberPadChildContainer}
          onPress={onReset}
          testID="passcode_reset"
        >
          <Text h2>Reset</Text>
        </TouchableOpacity>
        <PasscodeNumber number={0} onPress={onPressNumber} />
        <TouchableOpacity
          style={styles.numberPadChildContainer}
          onPress={() => onNumberChanged(number.slice(0, -1))}
        >
          <Ionicons name="md-backspace" color="white" size={55} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
