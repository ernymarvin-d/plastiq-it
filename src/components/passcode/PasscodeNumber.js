import React, { useState } from "react";
import { View, TouchableHighlight, StyleSheet } from "react-native";
import { Text } from "react-native-elements";

const styles = StyleSheet.create({
  container: {
    width: "33%",
    alignItems: "center"
  },
  circle: {
    width: 80,
    height: 80,
    borderStyle: "solid",
    borderWidth: 1.5,
    borderColor: "white",
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  number: {
    fontSize: 35,
    textAlign: "center",
    color: "white"
  },
  numberTouched: {
    color: "#a84bdd"
  }
});

export default ({ number, onPress }) => {
  const [isTouched, setIsTouched] = useState(false);
  return (
    <View style={styles.container}>
      <TouchableHighlight
        underlayColor="white"
        style={styles.circle}
        onPress={() => onPress(number)}
        onShowUnderlay={() => setIsTouched(true)}
        onHideUnderlay={() => setIsTouched(false)}
        testID={`passcode_${number}`}
      >
        <Text style={[styles.number, isTouched ? styles.numberTouched : null]}>
          {number}
        </Text>
      </TouchableHighlight>
    </View>
  );
};
