import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: "rgba(64, 73, 82, 0.09)",
    marginBottom: 10,
    marginHorizontal: 9,
    paddingVertical: 12,
    paddingHorizontal: 9
  },
  circle: {
    width: 40,
    height: 40,
    backgroundColor: "#f1effc",
    borderRadius: 40 / 2,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 15
  },
  textContainer: {
    flex: 1
  },
  providerName: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  accountName: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  accountType: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 10,
    fontWeight: "500",
    fontStyle: "italic",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  deleteBtn: {
    // width: 30
  },
  modalContainer: {
    backgroundColor: "white",
    width: "100%",
    padding: 20,
    borderRadius: 20,
    alignItems: "center"
  },
  modalHeader: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center"
  },
  modalBody: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 25,
    letterSpacing: 0,
    textAlign: "center",
    marginVertical: 20
  },
  modalBtn: {
    height: 40,
    borderRadius: 20,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 30
  },
  btnContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  btnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  }
});
