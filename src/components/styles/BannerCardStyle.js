import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: Metrics.itemWidth,
    aspectRatio: 3 / 1,
    borderRadius: 5
  },
  image2: {
    width: Metrics.screenWidth - 9 * 2
  }
});
