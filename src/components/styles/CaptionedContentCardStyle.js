import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: "100%",
    aspectRatio: 4 / 3,
    borderRadius: 5
  },
  textContainer: {
    flex: 1,
    justifyContent: "space-around",
    padding: 15
  },
  title: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    color: "black",
    marginBottom: 10
  },
  content: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    color: "black",
    marginBottom: 10
  },
  link: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    color: "blue"
  }
});
