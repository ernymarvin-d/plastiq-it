import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  btn: {
    marginHorizontal: 15,
    padding: 5,
    borderRadius: 9
    // backgroundColor: "#924bba",
  },
  category: {
    width: 20,
    height: 20,
    resizeMode: "contain"
    // backgroundColor: "red"
  }
});
