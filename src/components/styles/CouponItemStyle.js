import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    marginBottom: 10
  },
  centerContainer: {
    flex: 1
  },
  textTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    textAlign: "left",
    color: "black",
    marginBottom: 3
  },
  textContent: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontStyle: "normal",
    textAlign: "left",
    color: "black",
    marginBottom: 5
  }
});
