import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    padding: 15,
    borderRadius: 5,
    borderRadius: 6,
    backgroundColor: "rgba(64, 73, 82, 0.09)",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    marginHorizontal: 17,
    marginBottom: 10
  },
  merchant_logo: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 12,
    resizeMode: "contain"
  },
  centerContainer: {
    flex: 1
  },
  user: {
    // width: 80,
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "black",
    marginBottom: 5
  },
  status: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "italic",
    letterSpacing: 0,
    textAlign: "left",
    color: "black",
    marginBottom: 2
  },
  date: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  amount: {
    fontFamily: "Montserrat-Regular",
    marginLeft: "auto"
  }
});
