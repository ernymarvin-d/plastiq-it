import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: "rgba(64, 73, 82, 0.09)",
    marginBottom: 10,
    marginHorizontal: 9,
    paddingVertical: 15,
    paddingHorizontal: 9
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 200,
    marginRight: 15,
    resizeMode: "contain"
  },
  centerContainer: {
    flex: 1
  },
  name: {
    // width: 80,
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "black",
    marginBottom: 5
  },
  percent: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  btnContainer: {
    // flex: 1
    // backgroundColor: 'red'
  },
  shopBtn: {
    padding: 3,
    borderRadius: 5,
    backgroundColor: "#b0b2b6"
  },
  shopBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "white"
  }
});
