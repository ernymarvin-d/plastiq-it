import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15
  },
  carouselContainer: {
    flexGrow: 0 // Important!
  },
  paginationContainer: {},
  activeDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: "#a84bdd"
  },
  btnContainer: {
    width: "100%",
    paddingHorizontal: 30
  },
  detail_item: {
    flexDirection: "row",
    alignItems: "center",
    height: 54,
    borderRadius: 27,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    marginTop: 30
  },
  iconContent: {
    alignItems: "center",
    width: 50,
    marginLeft: 30
  },
  infoContent: {
    flex: 1,
    marginRight: 50
  },
  info: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  },
  logoutBtnContainer: {
    marginVertical: 20,
    alignItems: "center"
  },
  modalContainer: {
    backgroundColor: "white",
    width: "100%",
    padding: 20,
    borderRadius: 20,
    alignItems: "center"
  },
  modalHeader: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center"
  },
  modalBody: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 25,
    letterSpacing: 0,
    textAlign: "center",
    marginVertical: 20
  },
  modalBtn: {
    height: 40,
    borderRadius: 20,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 30
  },
  modalBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    fontSize: 20,
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  },
  logOutBtn: {
    height: 54,
    justifyContent: "center"
  },
  btnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "black"
  },
  inputContainer: {
    width: "100%",
    marginTop: 14
  },
  formInput: {
    width: "100%",
    textAlign: "left",
    marginBottom: 20
  },
  textLabel: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.72,
    textAlign: "left",
    color: "#999999",
    // textTransform: "uppercase",
    marginBottom: 10
  },
  textValueEdit: {
    fontFamily: "Montserrat-Regular",
    height: 40,
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    borderRadius: 10,
    color: "#404952",
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    borderColor: "transparent",
    borderWidth: 0.5
  },
  errorLabel: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "red"
  },
  modalBtnContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  errorText: {
    fontFamily: "Montserrat-Regular",
    lineHeight: 18,
    marginBottom: 10,
    color: "red"
  }
});
