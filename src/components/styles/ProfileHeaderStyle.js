import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "space-evenly",
    width: "100%",
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#a64adb",
    paddingTop: 30
  },
  background_img: {
    position: "absolute",
    top: Metrics.screenHeight * 0.25 - 350,
    left: 0,
    width: "100%",
    height: 350
  },
  avatarContainer: {
    // flex: 1,
    // backgroundColor: 'red',
  },
  avatarOverlay: {
    position: "absolute",
    width: Metrics.screenWidth * 0.25,
    height: Metrics.screenWidth * 0.25,
    borderRadius: Metrics.screenWidth * 0.25 * 0.5,
    backgroundColor: "white",
    opacity: 0.55,
    justifyContent: "center"
  },
  avatar: {
    width: Metrics.screenWidth * 0.25,
    height: Metrics.screenWidth * 0.25,
    borderRadius: Metrics.screenWidth * 0.25 * 0.5,
    resizeMode: "cover",
    // borderWidth: 1,
    // borderColor: 'gray',
    backgroundColor: "white"
    // marginBottom:10,
  },
  name: {
    fontFamily: "Montserrat-Regular",
    fontSize: 22,
    color: "white",
    fontWeight: "600"
  },
  userInfo: {
    fontSize: 16,
    color: "#778899",
    fontWeight: "600"
  },
  editBtn: {
    backgroundColor: "#E6E6FA",
    width: 30,
    height: 30,
    position: "absolute",
    top: 1,
    right: 1,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center"
  }
});
