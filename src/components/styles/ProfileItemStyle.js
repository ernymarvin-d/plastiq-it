import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    justifyContent: "center",
    width: Metrics.itemWidth,
    aspectRatio: 3 / 1,
    borderRadius: 5,
    backgroundColor: "#a64adb"
  },
  info: {
    fontFamily: "Montserrat-Regular",
    fontSize: 25,
    textAlign: "center",
    color: "white"
  }
});
