import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: "auto",
    width: "100%",
    height: Metrics.screenHeight * 0.125,
    backgroundColor: "#a64adb",
    paddingTop: 30
  },
  background_img: {
    position: "absolute",
    top: Metrics.screenHeight * 0.15 - 300,
    left: 0,
    width: "100%",
    height: 300
  },
  content: {
    // flex: 1,
    width: "100%",
    flexDirection: "row",
    // justifyContent: 'center',
    paddingHorizontal: 20
    // backgroundColor: 'red'
  },
  backBtn: {
    width: 20
    // backgroundColor: 'green'
  },
  back_icon: {
    width: Metrics.icons.small,
    height: Metrics.icons.small
  },
  title: {
    flex: 1,
    fontFamily: "Montserrat-Regular",
    fontSize: 22,
    color: "white",
    fontWeight: "600",
    textAlign: "center",
    marginRight: Metrics.icons.small
  }
});
