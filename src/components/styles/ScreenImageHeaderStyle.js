import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    justifyContent: "flex-end",
    marginBottom: "auto",
    height: Metrics.screenHeight * 0.125,
    backgroundColor: "#a64adb"
    // paddingTop: 30,
  },
  content: {
    flexDirection: "row",
    paddingLeft: 20,
    paddingRight: 40,
    paddingBottom: 5
  },
  backBtn: {
    width: 20,
    justifyContent: "center"
  },
  back_icon: {
    width: Metrics.icons.small,
    height: Metrics.icons.small
  },
  logoContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    height: Metrics.screenHeight * 0.125 * 0.55,
    width: (Metrics.screenHeight * 0.125 * 0.55 * 143) / 35,
    resizeMode: "contain"
  }
});
