import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(166, 74, 219, 0.7)",
    marginBottom: 10,
    marginHorizontal: 9,
    paddingVertical: 5,
    paddingHorizontal: 9
  },
  title: {
    fontFamily: "Montserrat-Regular",
    fontSize: 22,
    fontWeight: "600",
    color: "white",
    textAlign: "left"
  }
});
