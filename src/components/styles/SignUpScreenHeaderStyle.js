import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    backgroundColor: "#a64adb"
  },
  headerText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 25,
    color: "white",
    textAlign: "center",
    marginTop: 15
  },
  paginationContainer: {
    // backgroundColor: "red",
  },
  activeDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: "white"
  },
  inactiveDot: {
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "white"
  }
});
