import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  image: {
    width: Metrics.itemWidth,
    aspectRatio: 4 / 3,
    borderRadius: 5
  }
});
