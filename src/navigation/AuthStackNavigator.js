import { createStackNavigator } from "react-navigation-stack";

import LandingScreen from "@screens/landing/LandingScreen";
import SignInEmailScreen from "@screens/auth/SignInEmailScreen";
import SignInPasswordScreen from "@screens/auth/SignInPasswordScreen";
import SignUpScreen from "@screens/auth/SignUpScreen";
import SignUp2Screen from "@screens/auth/SignUp2Screen";
import ForgotPasswordScreen from "@screens/auth/ForgotPasswordScreen";
import VerifyPhoneNumberScreen from "@screens/auth/VerifyPhoneNumberScreen";
import PasscodeScreen from "@screens/passcode/PasscodeScreen";
import ForgotPasscodeScreen from "@screens/passcode/ForgotPasscodeScreen";
import NewPasscodeScreen from "@screens/passcode/NewPasscodeScreen";

export default createStackNavigator({
  LandingScreen,
  SignInEmailScreen,
  SignInPasswordScreen,
  SignUpScreen,
  SignUp2Screen,
  VerifyPhoneNumberScreen,
  ForgotPasswordScreen,
  PasscodeScreen,
  ForgotPasscodeScreen,
  NewPasscodeScreen
});
