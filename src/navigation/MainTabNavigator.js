import React from "react";
import { Image } from "react-native";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";

import homeIcon from "@assets/images/dashboard/iconHome.png";
import homeIconActive from "@assets/images/dashboard/iconHomeActive.png";
import offersIcon from "@assets/images/dashboard/iconOffers.png";
import offersIconActive from "@assets/images/dashboard/iconOffersActive.png";
import referIcon from "@assets/images/dashboard/iconRefer.png";
import referIconActive from "@assets/images/dashboard/iconReferActive.png";
import accountIcon from "@assets/images/dashboard/iconAccount.png";
import accountIconActive from "@assets/images/dashboard/iconAccountActive.png";

import HomeScreen from "@screens/home/HomeScreen";

import OffersScreen from "@screens/offers/OffersScreen";
import MerchantScreen from "@screens/offers/MerchantScreen";
import ShopWebViewScreen from "@screens/webview/ShopWebViewScreen";

import ReferScreen from "@screens/refer/ReferScreen";
import ReferalWebViewScreen from "@screens/webview/ReferalWebViewScreen";

import ProfileScreen from "@screens/account/ProfileScreen";
import AccountScreen from "@screens/account/AccountScreen";
import BankDetailsScreen from "@screens/account/BankDetailsScreen";
import CashbacksScreen from "@screens/account/CashbacksScreen";
import WithdrawScreen from "@screens/account/WithdrawScreen";
import SupportScreen from "@screens/account/SupportScreen";

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  ShopWebView: ShopWebViewScreen
});

HomeStack.navigationOptions = {
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        style={{ width: 32, height: 32, resizeMode: "contain" }}
        source={focused ? homeIconActive : homeIcon}
      />
    );
  }
};

const OffersStack = createStackNavigator({
  Offers: OffersScreen,
  Merchant: MerchantScreen,
  ShopWebView: ShopWebViewScreen
});

OffersStack.navigationOptions = {
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        style={{ width: 32, height: 32, resizeMode: "contain" }}
        source={focused ? offersIconActive : offersIcon}
      />
    );
  }
};

const ReferStack = createStackNavigator({
  Refer: ReferScreen,
  ReferalWebView: ReferalWebViewScreen
});

ReferStack.navigationOptions = {
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        style={{ width: 32, height: 32, resizeMode: "contain" }}
        source={focused ? referIconActive : referIcon}
      />
    );
  }
};

const MyAccountStack = createStackNavigator(
  {
    MyAccount: ProfileScreen,
    AccountDetails: AccountScreen,
    BankDetails: BankDetailsScreen,
    Cashbacks: CashbacksScreen,
    Withdraw: WithdrawScreen,
    Support: SupportScreen
  },
  {
    initialRouteName: "MyAccount"
  }
);

MyAccountStack.navigationOptions = {
  tabBarIcon: ({ focused }) => {
    return (
      <Image
        style={{ width: 32, height: 32, resizeMode: "contain" }}
        source={focused ? accountIconActive : accountIcon}
      />
    );
  }
};

export default createBottomTabNavigator(
  {
    HomeStack,
    OffersStack,
    ReferStack,
    MyAccountStack
  },
  {
    defaultNavigationOptions: {
      tabBarOptions: {
        showLabel: false
      }
    }
  }
);
