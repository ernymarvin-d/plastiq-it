import React from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TextInput,
  FlatList,
  ActivityIndicator,
  Keyboard
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import PhoneInput from "react-native-phone-input";
import moment from "moment";
import { connect } from "react-redux";
import {
  setUserDetail,
  changePassword,
  updateUserDetail
} from "@store/auth/actions";

import ScreenHeader from "../../components/ScreenHeader";
import styles from "../styles/AccountScreenStyle";
import AppConfig from "../../config/AppConfig";

class AccountScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      password_edit: false,
      password_old: "",
      password: "",
      password_confirmation: "",
      user_edit: false,
      modal_birthdate: false,
      birthday: new Date()
    };
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("AccountScreen");
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({ keyboardHeight: e.endCoordinates.height });
  };

  _keyboardDidHide = e => {
    this.setState({ keyboardHeight: 0 });
  };

  _editPassword = () => {
    Keyboard.dismiss();

    this.setState({
      password_edit: !this.state.password_edit,
      password_old: "",
      password: "",
      password_confirmation: ""
    });
  };

  _updatePassword = () => {
    Keyboard.dismiss();

    const data = {
      password_old: this.state.password_old,
      password: this.state.password,
      password_confirmation: this.state.password_confirmation
    };

    this.props.changePassword(data, this._editPassword);
  };

  _editUser = () => {
    Keyboard.dismiss();

    const { user_detail } = this.props;

    birthday_string = "Birthday date";
    let birthday = new Date();

    if (user_detail.birthday) {
      birthday = new Date(user_detail.birthday);
      birthday_string = this.formatDate(user_detail.birthday);
    }

    let { postal } = user_detail;
    postal = postal ? postal.toString() : "";

    this.setState({
      user_edit: !this.state.user_edit,
      ...user_detail,
      birthday,
      birthday_string,
      postal
    });
  };

  formatDate = date => {
    if (date) {
      return moment(date).format("DD/MM/YYYY");
    }

    return "Birthday date";
  };

  _changeBirthday = newDate => {
    const birthday = this.formatDate(newDate);

    this.setState({
      birthday_string: birthday,
      birthday: newDate,
      modal_birthdate: false
    });
  };

  _updateUserDetail = () => {
    Keyboard.dismiss();

    const { user_detail } = this.props;

    const data = {
      name: this.state.name,
      last_name: this.state.last_name,
      country_code: user_detail.country_code,
      phone: user_detail.phone,
      email: this.state.email.toLowerCase(),
      birthday: this.state.birthday,
      postal: this.state.postal
    };
    this.props.updateUserDetail(data, this._editUser);
  };

  _onChangePhoneNumber = phone_number => {
    const real_country_code = this.phone.getCountryCode();
    let country_code = `+${real_country_code}`;
    if (real_country_code == null) {
      // country code is null
      country_code = AppConfig.default_country_code;
    }

    phone_number = this.disassembleRealPhoneNumber(country_code, phone_number);
    const { user_detail } = this.props;

    this.props.setUserDetail({
      ...user_detail,
      country_code,
      phone: phone_number
    });
  };

  /**
   * divide country code and real phone number
   * country_code: +61
   * phone_number: 123456789
   */
  disassembleRealPhoneNumber = (country_code, phone_number) => {
    const country_code_length = country_code.length;

    phone_number = phone_number.substring(country_code_length);
    return phone_number;
  };

  _onSelectCountry = iso2 => {
    // this.phone.selectCountry(iso2); // change country flag.

    const { user_detail } = this.props;

    const country_code = `+${this.phone.getCountryCode()}`;
    this.props.setUserDetail({ ...user_detail, country_code });
  };

  render() {
    const {
      user_detail_loading,
      user_detail,
      request_validation_errors,
      change_password_loading
    } = this.props;
    return (
      <View style={styles.container}>
        <ScreenHeader title="Account Details" />
        <ScrollView
          style={styles.scroll_container}
          keyboardShouldPersistTaps="handled"
        >
          <View style={styles.screenContent}>
            <View style={styles.cancelBtnContainer}>
              <TouchableOpacity
                style={{ textAlign: "center" }}
                onPress={() => {
                  this._editUser();
                }}
              >
                {!user_detail_loading ? (
                  <Text style={styles.btnEditText}>
                    {this.state.user_edit ? "Cancel" : "Edit"}
                  </Text>
                ) : (
                  <ActivityIndicator color="white" />
                )}
              </TouchableOpacity>
            </View>

            {/* display details */}
            <View
              style={{
                width: "100%",
                display: this.state.user_edit ? "none" : "flex"
              }}
            >
              <View style={styles.formInput}>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.name && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    (user_detail.name ? user_detail.name : "Name")}
                </Text>
              </View>

              <View style={styles.formInput}>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.last_name && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    (user_detail.last_name
                      ? user_detail.last_name
                      : "Last Name")}
                </Text>
              </View>

              <View style={[styles.formInput, { flexDirection: "row" }]}>
                <Text style={styles.textValue}>
                  {!user_detail_loading &&
                    (user_detail.country_code
                      ? user_detail.country_code
                      : "+61") // +61 12345
                  }
                </Text>
                <Text> </Text>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.phone && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    (user_detail.phone
                      ? user_detail.phone
                      : "Mobile Phone Number") // +61 12345
                  }
                </Text>
              </View>

              <View style={styles.formInput}>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.email && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    (user_detail.email ? user_detail.email : "Email Address")}
                </Text>
              </View>

              <View style={styles.formInput}>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.birthday && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    this.formatDate(user_detail.birthday)}
                </Text>
              </View>

              <View style={styles.formInput}>
                <Text
                  style={[
                    styles.textValue,
                    !user_detail.postal && { color: "#b282d5" }
                  ]}
                >
                  {!user_detail_loading &&
                    (user_detail.postal ? user_detail.postal : "Postcode")}
                </Text>
              </View>
            </View>
            {/* end display details */}

            {/* edit details */}
            <View
              style={{
                width: "100%",
                display: !this.state.user_edit ? "none" : "flex"
              }}
            >
              <View style={styles.formInput}>
                <TextInput
                  style={styles.textValueEdit}
                  placeholder="Name"
                  placeholderTextColor="#b282d5"
                  onChangeText={text => this.setState({ name: text })}
                  value={this.state.name}
                />

                <FlatList
                  data={request_validation_errors.name}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.formInput}>
                <TextInput
                  style={styles.textValueEdit}
                  placeholder="Last Name"
                  placeholderTextColor="#b282d5"
                  onChangeText={text => this.setState({ last_name: text })}
                  value={this.state.last_name}
                />

                <FlatList
                  data={request_validation_errors.last_name}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.formInput}>
                {/* <TextInput
                  style={styles.textValueEdit}
                  placeholder={'Phone Number'}
                  placeholderTextColor="#b282d5"
                  onChangeText={text => this.setState({ phone: text })}
                  value={this.state.phone}
                /> */}
                <PhoneInput
                  ref={ref => {
                    this.phone = ref;
                  }}
                  onChangePhoneNumber={number =>
                    this._onChangePhoneNumber(number)
                  }
                  value={
                    (user_detail.country_code != undefined
                      ? user_detail.country_code
                      : "+61") +
                    (user_detail.phone != undefined ? user_detail.phone : "")
                  }
                  initialCountry={AppConfig.default_iso2}
                  onSelectCountry={this._onSelectCountry}
                  offset={0}
                  textProps={{
                    placeholder: "Mobile Phone Number",
                    placeholderTextColor: "#ba90d8"
                  }}
                  style={styles.inputPhoneText}
                  textStyle={styles.inputPhoneArea}
                />
                <FlatList
                  data={request_validation_errors.phone}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.formInput}>
                <TextInput
                  style={styles.textValueEdit}
                  placeholder="Email Address"
                  placeholderTextColor="#b282d5"
                  onChangeText={text => this.setState({ email: text })}
                  value={this.state.email}
                />

                <FlatList
                  data={request_validation_errors.email}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.formInput}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modal_birthdate: true });
                  }}
                >
                  <Text
                    style={[
                      styles.textValueEdit,
                      this.state.birthday_string == "Birthday date" && {
                        color: "#b282d5"
                      }
                    ]}
                  >
                    {this.state.birthday_string}
                  </Text>
                </TouchableOpacity>

                <FlatList
                  data={request_validation_errors.birthday}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.formInput}>
                <TextInput
                  style={styles.textValueEdit}
                  placeholder="Postcode"
                  placeholderTextColor="#b282d5"
                  onChangeText={text => this.setState({ postal: text })}
                  value={this.state.postal}
                  keyboardType="number-pad"
                />

                <FlatList
                  data={request_validation_errors.postal}
                  renderItem={({ item }) => (
                    <Text style={{ color: "red" }}>* {item}</Text>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>

              <View style={styles.btnContainer}>
                <TouchableOpacity
                  style={{
                    ...styles.btn,
                    display: this.state.user_edit ? "flex" : "none"
                  }}
                  onPress={() => {
                    this._updateUserDetail();
                  }}
                >
                  {!user_detail_loading ? (
                    <Text style={styles.btnText}>Update User Details</Text>
                  ) : (
                    <ActivityIndicator color="#ae78d3" />
                  )}
                </TouchableOpacity>
              </View>
            </View>
            {/* end edit details */}

            {/* password */}
            {this.state.password_edit && (
              <View style={styles.cancelBtnContainer}>
                <TouchableOpacity
                  style={{ textAlign: "center" }}
                  onPress={() => {
                    this._editPassword();
                  }}
                >
                  <Text style={styles.btnEditText}>Cancel</Text>
                </TouchableOpacity>
              </View>
            )}

            <View style={styles.passwordChangedContainer}>
              <View
                style={{
                  display: this.state.password_edit ? "flex" : "none"
                }}
              >
                <View style={styles.formInput}>
                  <TextInput
                    style={styles.textValue}
                    secureTextEntry
                    placeholder="Old Password"
                    placeholderTextColor="#b282d5"
                    onChangeText={text => this.setState({ password_old: text })}
                    value={this.state.password_old}
                  />

                  <FlatList
                    data={request_validation_errors.password_old}
                    renderItem={({ item }) => (
                      <Text style={{ color: "red" }}>* {item}</Text>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>

                <View style={styles.formInput}>
                  <TextInput
                    style={styles.textValue}
                    secureTextEntry
                    placeholder="New Password"
                    placeholderTextColor="#b282d5"
                    onChangeText={text => this.setState({ password: text })}
                    value={this.state.password}
                  />

                  <FlatList
                    data={request_validation_errors.password}
                    renderItem={({ item }) => (
                      <Text style={{ color: "red" }}>* {item}</Text>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>

                <View style={styles.formInput}>
                  <TextInput
                    style={styles.textValue}
                    secureTextEntry
                    placeholder="Confirm Password"
                    placeholderTextColor="#b282d5"
                    onChangeText={text =>
                      this.setState({ password_confirmation: text })
                    }
                    value={this.state.password_confirmation}
                  />

                  <FlatList
                    data={request_validation_errors.password_confirmation}
                    renderItem={({ item }) => (
                      <Text style={{ color: "red" }}>* {item}</Text>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              </View>

              {this.state.password_edit ? (
                <View style={styles.btnContainer}>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() => {
                      this._updatePassword();
                    }}
                  >
                    {!change_password_loading ? (
                      <Text style={styles.btnText}>Update Password</Text>
                    ) : (
                      <ActivityIndicator color="#ae78d3" />
                    )}
                  </TouchableOpacity>
                </View>
              ) : (
                <View style={styles.btnContainer}>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() => {
                      this._editPassword();
                    }}
                  >
                    <Text style={styles.btnText}>Change Password</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
            {/* end edit password */}
          </View>

          <View style={{ height: this.state.keyboardHeight }} />
        </ScrollView>

        {/* modal for birthdate */}
        <DateTimePicker
          isVisible={this.state.modal_birthdate}
          onConfirm={this._changeBirthday}
          onCancel={() => this.setState({ modal_birthdate: false })}
          date={this.state.birthday}
          mode="date"
          locale="en_GB"
          titleIOS="Birthdate"
          titleStyle={styles.modalTitleText}
          cancelTextStyle={styles.modalBtnText}
          confirmTextStyle={styles.modalBtnText}
        />
      </View>
    );
  }
}

AccountScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => ({
  change_password_loading: state.auth.change_password_loading,
  user_detail_loading: state.auth.user_detail_loading,
  request_validation_errors: state.auth.request_validation_errors,
  user_detail: state.auth.user_detail
});

const mapDispatch = dispatch => ({
  changePassword: (data, callback) => dispatch(changePassword(data, callback)),
  updateUserDetail: (data, callback) =>
    dispatch(updateUserDetail(data, callback)),
  setUserDetail: user => dispatch(setUserDetail(user))
});

export default connect(mapState, mapDispatch)(AccountScreen);
