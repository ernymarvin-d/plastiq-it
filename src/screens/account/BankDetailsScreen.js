import React from "react";
import { View, Text, SectionList, Alert } from "react-native";
import LottieView from "lottie-react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { WebView } from "react-native-webview";

import {
  getBankAccounts,
  setNewBankAccountConnecting
} from "@store/account/actions";
import ScreenHeader from "../../components/ScreenHeader";
import BankAccountItem from "../../components/BankAccountItem";
import SectionHeader from "../../components/SectionHeader";
import BannerCard from "../../components/BannerCard";
import styles from "../styles/BankDetailsScreenStyle";
import API_CONFIG from "../../service/api_config";

class BankDetailsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      is_bank_account_connecting: false,
      lottie_animation_speed: 5,
      loading: true,
      link: `${API_CONFIG.SERVER_URL}/api/v1/user/account/connect`,
      token: false
    };
  }

  async componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("BankDetailsScreen");

    this.props.getBankAccounts();

    const token = await AsyncStorage.getItem("token");

    this.setState({
      token
    });
  }

  _connectAccount = () => {
    this.setState({
      is_bank_account_connecting: true,
      lottie_animation_speed: 1
    });
    // this.props.setNewBankAccountConnecting(true);
    // this.props.navigation.navigate("WebViewScreen");
  };

  _handleEventFromWebview = event => {
    event.persist();

    if (event.nativeEvent.data == "done") {
      this.refreshContentCards(); // when connecting a bank account, refresh content cards.

      this.props.getBankAccounts();
      this.setState({
        is_bank_account_connecting: false,
        lottie_animation_speed: 0
      });
    }
  };

  refreshContentCards = () => {
    const { ReactAppboy } = global;
    ReactAppboy.requestContentCardsRefresh();
  };

  _renderBankAccountItem = ({ item }) => {
    return <BankAccountItem item={item} />;
  };

  _renderSectionHeader = ({ section }) => {
    return <SectionHeader info={section} />;
  };

  render() {
    const bank_accounts_list =
      this.state.is_bank_account_connecting ||
      this.props.bank_accounts_loading ? (
        <View style={styles.box}>
          <LottieView
            source={require("@assets/animations/coins-grow.json")}
            autoPlay
            loop
            speed={5}
            style={styles.lottieLoader}
          />
        </View>
      ) : this.props.bank_accounts.length ? (
        <View style={styles.bankaccountsContainer}>
          <SectionList
            sections={this.props.bank_accounts}
            keyExtractor={(item, index) => item + index}
            renderItem={this._renderBankAccountItem}
            renderSectionHeader={this._renderSectionHeader}
          />
        </View>
      ) : (
        <View style={styles.box}>
          <Text style={styles.bankName}>No Bank Accounts Connected.</Text>
        </View>
      );

    return (
      <View style={styles.container}>
        <ScreenHeader title="Bank Details" />
        {this.state.is_bank_account_connecting ? (
          <View style={styles.screenContent_conneting}>
            {this.state.token && (
              <WebView
                source={{
                  uri: this.state.link,
                  headers: {
                    Authorization: `Bearer ${this.state.token}`
                  }
                }}
                onLoadEnd={() => {
                  this.setState({
                    loading: false
                  });
                }}
                onError={() => {
                  // this.props.navigation.goBack();
                  this.setState({
                    loading: false
                  });
                  Alert.alert("Service Error!");
                }}
                onMessage={event => this._handleEventFromWebview(event)}
                canGoBack
                useWebKit
                javaScriptEnabled
                domStorageEnabled
                injectedJavaScript={this.state.cookie}
                startInLoadingState={false}
                cacheEnabled
              />
            )}

            {this.state.loading && (
              <View style={styles.loadingContainer}>
                <LottieView
                  source={require("@assets/animations/finance-animation.json")}
                  autoPlay
                  loop
                  speed={this.state.lottie_animation_speed}
                  style={styles.lottieLoader}
                />
              </View>
            )}
          </View>
        ) : (
          <View style={styles.screenContent_list}>
            {this.props.connect_bank_account_banner_cards[0] != null && (
              <BannerCard
                data={this.props.connect_bank_account_banner_cards[0]}
                onPress={this._connectAccount}
                bank_detail_screen
              />
            )}

            {bank_accounts_list}
          </View>
        )}
      </View>
    );
  }
}

BankDetailsScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => {
  const { cashback, bank_accounts } = state;

  return {
    bank_accounts: bank_accounts.bank_accounts,
    bank_accounts_loading: bank_accounts.bank_accounts_loading,
    connect_bank_account_banner_cards:
      cashback.connect_bank_account_banner_cards
  };
};

const mapDispatch = dispatch => ({
  getBankAccounts: () => dispatch(getBankAccounts()),
  setNewBankAccountConnecting: data =>
    dispatch(setNewBankAccountConnecting(data))
});

export default connect(mapState, mapDispatch)(BankDetailsScreen);
