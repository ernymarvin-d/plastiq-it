import React from "react";
import { View, FlatList, Text, ActivityIndicator } from "react-native";
import { connect } from "react-redux";

import { getCashbacks, loadMoreCashbacks } from "@store/cashback/actions";

import ScreenHeader from "../../components/ScreenHeader";
import MerchantItem from "../../components/MerchantItem";

import styles from "../styles/CashbacksScreenStyle";

class CashbacksScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("CashbacksScreen");

    this.props.getCashbacks();
  }

  _renderMerchantItem = ({ item }) => {
    return <MerchantItem item={item} />;
  };

  handleLoadMoreCashbacks = () => {
    const { current_page, last_page } = this.props.cashbacks;
    // console.log('/handleLoadMoreCashbacks cashbacks', this.props.cashbacks);

    if (current_page <= last_page) {
      const params = {
        page: current_page + 1
      };

      this.props.loadMoreCashbacks(params);
    } else {
      console.log("/handleLoadMoreCashbacks => Nothing more");
    }
  };

  render() {
    const cashback_list =
      this.props.cashbacks.data.length > 0 ? (
        <FlatList
          data={this.props.cashbacks.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderMerchantItem}
          onEndReached={() => this.handleLoadMoreCashbacks()}
          onEndReachedThreshold={0.3}
        />
      ) : (
        // <View style={{ display: this.props.cashbacks_loading ? "none" : "" }}>
        <View style={styles.box}>
          <Text style={styles.textNotification}>No Cashbacks Found</Text>
        </View>
        // </View>
      );

    return (
      <View style={styles.container}>
        <ScreenHeader title="Cashbacks Received" />
        <View style={styles.screenContent}>
          {this.props.cashbacks_loading ? (
            <View style={{ marginBottom: 10 }}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            cashback_list
          )}
        </View>
      </View>
    );
  }
}

CashbacksScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => ({
  cashbacks: state.cashback.cashbacks,
  cashbacks_loading: state.cashback.cashbacks_loading
});

const mapDispatch = dispatch => ({
  getCashbacks: () => dispatch(getCashbacks()),
  loadMoreCashbacks: params => dispatch(loadMoreCashbacks(params))
});

export default connect(mapState, mapDispatch)(CashbacksScreen);
