import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";

import ProfileHeader from "../../components/ProfileHeader";
import ProfileBody from "../../components/ProfileBody";

import styles from "../styles/ProfileScreenStyle";

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("ProfileScreen");
  }

  render() {
    return (
      <View style={styles.container}>
        <ProfileHeader />
        <ProfileBody />
      </View>
    );
  }
}

ProfileScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
  // headerLeft: (
  //   <TouchableOpacity style={{ paddingLeft: 20 }}>
  //     <Image source={require('@assets/images/dashboard/menu.png')} />
  //   </TouchableOpacity>
  // )
};

const mapState = state => ({});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(ProfileScreen);
