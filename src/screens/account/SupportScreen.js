import React from "react";
import { View, ScrollView, ActivityIndicator, Text, Alert } from "react-native";

import { connect } from "react-redux";
import { WebView } from "react-native-webview";

import ScreenHeader from "../../components/ScreenHeader";
import styles from "../styles/SupportScreenStyle";
import AppConfig from "../../config/AppConfig";

class SupportScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      first_loading: true, // make lottie flies icon to be shown once upon initial loading.
      canGoBack: false,
      canGoForward: false
    };
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("SupportScreen");
  }

  render() {
    return (
      <View style={styles.container}>
        <ScreenHeader title="Support" />
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: "space-between"
          }}
          style={styles.scroll_container}
        >
          <View style={styles.screenContent}>
            <WebView
              ref={ref => (this.webview = ref)}
              source={{
                uri: AppConfig.support_link
              }}
              onLoadEnd={syntheticEvent => {
                // const { nativeEvent } = syntheticEvent;
                this.setState({
                  first_loading: false
                });
              }}
              onError={syntheticEvent => {
                const { nativeEvent } = syntheticEvent;
                console.log("onError nativeEvent", nativeEvent);
                this.setState({
                  first_loading: false
                });
                this.props.navigation.goBack();
                Alert.alert("Service Error!", nativeEvent.description);
              }}
              javaScriptEnabled
              domStorageEnabled
              cacheEnabled
            />
            {this.state.first_loading && (
              <View style={styles.loadingContainer}>
                <ActivityIndicator size="large" />
                <Text style={styles.loadingTitle}>Please wait!</Text>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

SupportScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => ({});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(SupportScreen);
