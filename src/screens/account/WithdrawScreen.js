import React from "react";
import { View, ScrollView } from "react-native";
import { connect } from "react-redux";

import ScreenHeader from "../../components/ScreenHeader";
import styles from "../styles/WithdrawScreenStyle";

class WithdrawScreen extends React.PureComponent {
  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("WithdrawScreen");
  }

  render() {
    return (
      <ScrollView style={styles.scroll_container}>
        <View style={styles.container}>
          <ScreenHeader title="Withdraw" />
        </View>
      </ScrollView>
    );
  }
}

WithdrawScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => ({});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(WithdrawScreen);
