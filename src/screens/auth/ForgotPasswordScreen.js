import React from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import { connect } from "react-redux";

import SignInContainer from "@components/SignInContainer";
import SignInInput from "@components/SignInInput";
import SignInForgotButton from "@components/auth/SignInForgotButton";

import { forgotPassword } from "@store/auth/actions";

import { getAuthLoading } from "@store/auth/selectors";

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "white"
  },
  imageView: {
    width: "70%",
    backgroundColor: "transparent",
    justifyContent: "flex-end",
    alignItems: "center",
    marginBottom: 32
  },
  containerView: {
    backgroundColor: "transparent"
  },
  title: {
    color: "black"
  },
  subtitle: {
    marginTop: 16,
    color: "black",
    textAlign: "center"
  },
  inputContainer: {
    borderColor: "darkgray"
  },
  input: {
    color: "black"
  }
});

class ForgotPasswordScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = { email: "" };

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("ForgotPasswordScreen");
  }

  forgotPassword = () => {
    const { forgotPassword } = this.props;
    const { email } = this.state;
    forgotPassword(email);
  };

  render() {
    const { authLoading } = this.props;
    const { email } = this.state;
    return (
      <SignInContainer
        scrollViewStyle={styles.scrollView}
        imageStyle={styles.imageView}
        source={require("@assets/images/auth/forgot/illustration.png")}
        style={styles.containerView}
      >
        <Text h2 style={styles.title}>
          Forgotten your Password
        </Text>

        <Text h4 style={styles.subtitle}>
          Enter your email address and we will send you a password reset link.
        </Text>

        <SignInInput
          leftIcon={{
            type: "material-community",
            name: "email-outline",
            color: "darkgray"
          }}
          onChangeText={email => this.setState({ email })}
          value={email}
          inputContainerStyle={styles.inputContainer}
          inputStyle={styles.input}
          placeholder="Email Address"
          placeholderTextColor="darkgray"
          selectionColor="darkgray"
          autoCompleteType="email"
          autoCapitalize="none"
          keyboardType="email-address"
        />

        <SignInForgotButton
          title="Submit"
          onPress={this.forgotPassword}
          loading={authLoading}
        />
      </SignInContainer>
    );
  }
}

const mapStateToProps = state => ({
  authLoading: getAuthLoading(state)
});

const mapDispatchToProps = {
  forgotPassword
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPasswordScreen);
