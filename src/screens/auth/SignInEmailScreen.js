import React from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import { connect } from "react-redux";

import SignInContainer from "@components/SignInContainer";
import SignInInput from "@components/SignInInput";
import SignUpButton from "@components/SignUpButton";

import { checkEmail } from "@store/auth/actions";
import { getAuthLoading, getValidationErrors } from "@store/auth/selectors";

export const styles = StyleSheet.create({
  subtitle: {
    marginTop: 16,
    textAlign: "center"
  }
});

class SignInEmailScreen extends React.PureComponent {
  static navigationOptions = () => ({
    header: null
  });

  state = { email: "" };

  loadingButton = React.createRef();

  componentDidMount() {
    // Segment Screen
    // TODO: Move to ReactNavigation didStateChange hook.
    const { analytics } = global;
    analytics.screen("SignInEmailScreen");
  }

  validateEmailLowerCase = () => {
    const { email } = this.state;
    const { checkEmail } = this.props;
    checkEmail(email.toLowerCase());
  };

  render() {
    const { authLoading, validationErrors } = this.props;
    const { email } = this.state;
    return (
      <SignInContainer>
        <Text h2>Sign in or Create a New Account</Text>

        <Text h4 style={styles.subtitle}>
          Your email address will be used for login and receiving updates
        </Text>

        <SignInInput
          leftIcon={{
            type: "material-community",
            name: "email-outline"
          }}
          onChangeText={email => this.setState({ email })}
          value={email}
          placeholder="Email Address"
          autoCompleteType="email"
          autoCapitalize="none"
          keyboardType="email-address"
          testID="login-email-input"
          errors={validationErrors.email}
        />

        <SignUpButton
          title="Next"
          onPress={this.validateEmailLowerCase}
          loading={authLoading}
          testID="login-email-btn"
        />
      </SignInContainer>
    );
  }
}

const mapStateToProps = state => ({
  authLoading: getAuthLoading(state),
  validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = {
  checkEmail
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInEmailScreen);
