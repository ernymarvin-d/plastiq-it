import React from "react";
import { StyleSheet } from "react-native";
import { Button, Text } from "react-native-elements";
import { connect } from "react-redux";

import SignInContainer from "@components/SignInContainer";
import SignInInput from "@components/SignInInput";
import SignUpButton from "@components/SignUpButton";

import {
  getUser,
  getAuthLoading,
  getValidationErrors
} from "@store/auth/selectors";
import { login } from "@store/auth/actions";

const styles = StyleSheet.create({
  subtitle: {
    marginTop: 16,
    textAlign: "center"
  },
  forgotPasswordContainer: {
    marginTop: 24
  },
  forgotPasswordTitle: {
    fontSize: 13
  }
});

class SignInPasswordScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    password: "",
    isSecure: true
  };

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("SignInPasswordScreen");
  }

  login = () => {
    const { user, login } = this.props;
    const { password } = this.state;
    const { email } = user;
    login({
      email,
      password
    });
  };

  render() {
    const { user, authLoading, validationErrors, navigation } = this.props;
    const { password, isSecure } = this.state;
    const { firstName } = user;
    return (
      <SignInContainer>
        <Text h2>Hi {firstName},</Text>
        <Text h2>Welcome Back!</Text>

        <Text h4 style={styles.subtitle}>
          Enter your password to login
        </Text>

        <SignInInput
          leftIcon={{
            type: "ionicon",
            name: "md-lock"
          }}
          rightIcon={{
            type: "ionicon",
            name: isSecure ? "md-eye-off" : "md-eye",
            onPress: () => this.setState({ isSecure: !isSecure })
          }}
          onChangeText={password => this.setState({ password })}
          value={password}
          placeholder="Password"
          secureTextEntry={isSecure}
          errors={validationErrors.password}
          testID="login-password-input"
        />

        <SignUpButton
          title="Login"
          loading={authLoading}
          onPress={this.login}
          testID="login-password-btn"
        />

        <Button
          onPress={() => navigation.navigate("ForgotPasswordScreen")}
          type="clear"
          title="Forgot Password?"
          containerStyle={styles.forgotPasswordContainer}
          titleStyle={styles.forgotPasswordTitle}
        />
      </SignInContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: getUser(state),
  authLoading: getAuthLoading(state),
  validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInPasswordScreen);
