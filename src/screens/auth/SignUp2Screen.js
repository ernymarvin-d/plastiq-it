import React from "react";
import { View, Text, Linking } from "react-native";
import { connect } from "react-redux";
import ModalSelector from "react-native-modal-selector";
import { CheckBox } from "react-native-elements";
import PhoneInput from "react-native-phone-input";
import FontAwesome from "react-native-vector-icons/dist/FontAwesome";

import SignUpContainer from "@components/SignUpContainer";
import SignUpButton from "@components/SignUpButton";
import SignUpErrors from "@components/SignUpErrors";

import { getAuthLoading, getValidationErrors } from "@store/auth/selectors";
import { register, setValidationErrors } from "@store/auth/actions";

import AppConfig from "../../config/AppConfig";

export const styles = {
  container: {
    paddingHorizontal: 16
  },
  inputForm: {
    width: "100%",
    height: 45,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 16,
    marginHorizontal: 28,
    paddingHorizontal: 4
  },
  inputText: {
    fontFamily: "Montserrat",
    fontSize: 15,
    color: "white"
  },
  phoneNumberForm: {
    borderBottomWidth: 1,
    borderColor: "#ba90d8"
  },
  phoneNumberFormError: {
    borderColor: "red"
  },
  phoneNumberView: {
    marginLeft: 16
  },
  ageView: {
    width: "70%"
  },
  checkboxGender: {
    flex: 1,
    paddingVertical: 5,
    backgroundColor: "transparent",
    borderColor: "transparent"
  },
  checkboxGenderTitle: {
    fontFamily: "Montserrat",
    color: "white",
    fontSize: 12
  },
  termsForm: {
    width: "100%",
    flexDirection: "row",
    marginTop: 16,
    marginHorizontal: 20
  },
  termsCheckbox: {
    height: 25,
    padding: 0
  },
  termsText: {
    fontFamily: "Montserrat",
    fontSize: 15,
    lineHeight: 20,
    color: "white",
    marginTop: 8
  },
  termsLink: {
    textDecorationLine: "underline"
  }
};

class SignUp2Screen extends React.PureComponent {
  static navigationOptions = () => ({
    headerShown: false
  });

  state = {
    countryCode: "+61",
    phoneNumber: "",
    isMaleChecked: true,
    ageBracket: "18-24",
    termsAndConditionsChecked: false,
    keepUpdatedChecked: true
  };

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("SignUp2Screen");
  }

  get formIsValid() {
    const { phoneNumber, termsAndConditionsChecked } = this.state;
    return phoneNumber && termsAndConditionsChecked;
  }

  visitWebsite = URL =>
    Linking.openURL(URL).catch(error => console.error(error));

  onNext = async () => {
    const { setValidationErrors, navigation, register } = this.props;
    if (this.phone && !this.phone.isValidNumber()) {
      setValidationErrors({
        phone_number: ["Invalid phone number"]
      });
      return;
    }
    setValidationErrors();

    const email = navigation.getParam("email");
    const firstName = navigation.getParam("firstName");
    const lastName = navigation.getParam("lastName");
    const password = navigation.getParam("password");
    const passwordConfirm = navigation.getParam("passwordConfirm");
    const { countryCode, phoneNumber, ageBracket, isMaleChecked } = this.state;
    register({
      email,
      firstName,
      lastName,
      password,
      passwordConfirm,
      countryCode,
      phoneNumber,
      ageBracket,
      gender: isMaleChecked ? "male" : "female"
    });
  };

  onChangePhoneNumber = phoneNumber => {
    let countryCode = this.phone.getCountryCode();
    if (!countryCode) {
      countryCode = AppConfig.default_country_code;
    } else {
      countryCode = `+${countryCode}`;
    }
    this.setState({
      countryCode,
      phoneNumber: phoneNumber.substring(countryCode.length)
    });
  };

  onSelectCountry = () =>
    this.setState({ countryCode: `+${this.phone.getCountryCode()}` });

  render() {
    const { navigation, authLoading, validationErrors } = this.props;
    const {
      countryCode,
      phoneNumber,
      ageBracket,
      isMaleChecked,
      termsAndConditionsChecked,
      keepUpdatedChecked
    } = this.state;
    return (
      <SignUpContainer
        title="Almost there..."
        index={1}
        navigation={navigation}
        style={styles.container}
      >
        <View
          style={[
            styles.inputForm,
            styles.phoneNumberForm,
            validationErrors.phone_number ? styles.phoneNumberFormError : {}
          ]}
        >
          <FontAwesome name="phone" size={20} color="white" />
          <PhoneInput
            ref={ref => (this.phone = ref)}
            value={countryCode + phoneNumber}
            onChangePhoneNumber={this.onChangePhoneNumber}
            initialCountry={AppConfig.default_iso2}
            onSelectCountry={this.onSelectCountry}
            offset={0}
            textProps={{
              placeholder: "Mobile phone number",
              placeholderTextColor: "#ba90d8",
              selectionColor: "#ba90d8"
            }}
            style={styles.phoneNumberView}
            textStyle={styles.inputText}
          />
        </View>
        <SignUpErrors errors={validationErrors.phone_number} />
        <View style={styles.inputForm}>
          <Text style={styles.inputText}>Age:</Text>
          <ModalSelector
            style={styles.ageView}
            data={[
              { key: 0, label: "18-24" },
              { key: 1, label: "25-34" },
              { key: 2, label: "35-44" },
              { key: 3, label: "45-64" },
              { key: 4, label: "65+" }
            ]}
            initValue={ageBracket}
            onChange={option => this.setState({ ageBracket: option.label })}
            scrollViewAccessibilityLabel="Scrollable options"
          />
        </View>

        <View style={styles.inputForm}>
          <Text style={styles.inputText}>Gender: </Text>

          <CheckBox
            containerStyle={styles.checkboxGender}
            textStyle={styles.checkboxGenderTitle}
            center
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            uncheckedColor="white"
            checkedColor="white"
            title="Male"
            checked={isMaleChecked}
            onPress={() => this.setState({ isMaleChecked: true })}
          />

          <CheckBox
            containerStyle={styles.checkboxGender}
            textStyle={styles.checkboxGenderTitle}
            center
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            uncheckedColor="white"
            checkedColor="white"
            title="Female"
            checked={!isMaleChecked}
            onPress={() => this.setState({ isMaleChecked: false })}
          />
        </View>
        <View style={styles.termsForm}>
          <CheckBox
            containerStyle={styles.termsCheckbox}
            uncheckedColor="white"
            checkedColor="white"
            checked={termsAndConditionsChecked}
            onPress={() =>
              this.setState({
                termsAndConditionsChecked: !termsAndConditionsChecked
              })
            }
          />

          <Text style={styles.termsText}>
            {"I agree to the "}
            <Text
              style={styles.termsLink}
              onPress={() => this.visitWebsite(AppConfig.privacy_policy_url)}
            >
              Privacy Policy
            </Text>
            {" and "}
            <Text
              style={styles.termsLink}
              onPress={() => this.visitWebsite(AppConfig.terms_conditions_url)}
            >
              Terms and Conditions
            </Text>
            {" provided by Plastiq Pty Ltd."}
          </Text>
        </View>
        <View style={styles.termsForm}>
          <CheckBox
            containerStyle={styles.termsCheckbox}
            uncheckedColor="white"
            checkedColor="white"
            checked={keepUpdatedChecked}
            onPress={() =>
              this.setState({ keepUpdatedChecked: !keepUpdatedChecked })
            }
          />
          <Text style={styles.termsText}>
            Keep me updated about Plastiq products, news, and promotions
          </Text>
        </View>
        <SignUpButton
          loading={authLoading}
          disabled={!this.formIsValid}
          onPress={this.onNext}
        />
      </SignUpContainer>
    );
  }
}

const mapStateToProps = state => ({
  authLoading: getAuthLoading(state),
  validationErrors: getValidationErrors(state)
});

const mapDispatchToProps = {
  register,
  setValidationErrors
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp2Screen);
