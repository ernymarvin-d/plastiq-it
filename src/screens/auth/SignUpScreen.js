import React from "react";
import { connect } from "react-redux";

import Ionicons from "react-native-vector-icons/dist/Ionicons";
import FontAwesome5 from "react-native-vector-icons/dist/FontAwesome5";

import SignUpContainer from "@components/SignUpContainer";
import SignUpInputForm from "@components/SignUpInputForm";
import SignUpPasswordStrength from "@components/SignUpPasswordStrength";
import SignUpButton from "@components/SignUpButton";

import {
  getAuthLoading,
  getValidationErrors,
  getUser
} from "@store/auth/selectors";
import { register } from "@store/auth/actions";

class SignUpScreen extends React.PureComponent {
  static navigationOptions = () => ({
    header: null
  });

  state = {
    firstName: "",
    lastName: "",
    password: "",
    passwordConfirm: ""
  };

  criterias = [
    {
      regex: /[a-z]/,
      title: "At least 1 lowercase letter."
    },
    {
      regex: /[A-Z]/,
      title: "At least 1 uppercase letter."
    },
    {
      regex: /[0-9]/,
      title: "At least 1 number."
    },
    {
      regex: /^.{8,}$/,
      title: "At least 8 characters long."
    }
  ];

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("SignUpScreen");
  }

  get formIsValid() {
    const { firstName, lastName, password, passwordConfirm } = this.state;
    if (
      !firstName ||
      !lastName ||
      !password ||
      !passwordConfirm ||
      password !== passwordConfirm
    ) {
      return false;
    }
    for (let i = 0; i < this.criterias.length; i++) {
      if (!password.match(this.criterias[i].regex)) {
        return false;
      }
    }
    return true;
  }

  onNext = () => {
    const { navigation, user } = this.props;
    const { firstName, lastName, password, passwordConfirm } = this.state;
    const { email } = user;
    navigation.navigate("SignUp2Screen", {
      email,
      firstName,
      lastName,
      password,
      passwordConfirm
    });
  };

  render() {
    const { authLoading, validationErrors, navigation } = this.props;
    const { firstName, lastName, password, passwordConfirm } = this.state;
    const editable = !authLoading;
    return (
      <SignUpContainer
        title="Enter your details"
        index={0}
        navigation={navigation}
      >
        <SignUpInputForm
          ImageComponent={FontAwesome5}
          name="user-circle"
          placeholder="First Name"
          editable={editable}
          onChangeText={firstName => this.setState({ firstName })}
          value={firstName}
          errors={validationErrors.first_name}
        />
        <SignUpInputForm
          ImageComponent={FontAwesome5}
          name="user-circle"
          placeholder="Last Name"
          editable={editable}
          onChangeText={lastName => this.setState({ lastName })}
          value={lastName}
          errors={validationErrors.last_name}
        />
        <SignUpInputForm
          ImageComponent={Ionicons}
          name="md-lock"
          placeholder="Password"
          editable={editable}
          onChangeText={password => this.setState({ password })}
          value={password}
          secureTextEntry
          errors={validationErrors.password}
        />
        <SignUpInputForm
          ImageComponent={Ionicons}
          name="md-lock"
          placeholder="Confirm Password"
          editable={editable}
          onChangeText={passwordConfirm => this.setState({ passwordConfirm })}
          value={passwordConfirm}
          secureTextEntry
          errors={validationErrors.password_confirmation}
        />

        <SignUpPasswordStrength
          password={password}
          criterias={this.criterias}
        />
        <SignUpButton
          loading={authLoading}
          disabled={!this.formIsValid}
          onPress={this.onNext}
        />
      </SignUpContainer>
    );
  }
}

const mapStateToProps = state => ({
  authLoading: getAuthLoading(state),
  validationErrors: getValidationErrors(state),
  user: getUser(state)
});

const mapDispatchToProps = {
  register
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);
