import React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import { connect } from "react-redux";

import SignUpContainer from "@components/SignUpContainer";
import SignUpButton from "@components/SignUpButton";

import {
  getAuthLoading,
  getValidationErrors,
  getUser
} from "@store/auth/selectors";

import {
  verifyPhoneNumber,
  verifyPhoneNumberResendCode
} from "@store/auth/actions";

const styles = StyleSheet.create({
  descriptionContainer: {
    alignItems: "center",
    paddingTop: 30
  },
  descriptionText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 18,
    letterSpacing: 0,
    color: "white",
    marginBottom: 10
  },
  wrongNumberText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 0,
    color: "white",
    marginBottom: 10
  },
  backText: {
    textDecorationLine: "underline"
  },
  phoneText: {
    fontFamily: "Montserrat-Regular",
    fontWeight: "bold",
    lineHeight: 18,
    color: "white",
    marginVertical: 0,
    marginBottom: 10
  },
  errorText: {
    fontFamily: "Montserrat-Regular",
    lineHeight: 18,
    marginBottom: 10,
    color: "red"
  },
  inputForm: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%",
    marginBottom: 30
  },
  codeText: {
    width: 39,
    height: 50,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "white",
    backgroundColor: "transparent",
    fontFamily: "Montserrat-Regular",
    color: "white",
    fontSize: 22,
    textAlign: "center"
  },
  descriptionContainer2: {
    alignItems: "center"
  },
  expireText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    lineHeight: 25,
    color: "white"
  },
  durationText: {
    fontFamily: "Montserrat-Regular",
    fontWeight: "bold",
    color: "white"
  },
  dontReceivedText: {
    marginTop: 0,
    marginBottom: 5,
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    color: "white"
  },
  resendBtn: {
    padding: 10,
    marginBottom: 5
  },
  resendBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    color: "#ba90d8"
  }
});
class VerifyPhoneNumberScreen extends React.PureComponent {
  static navigationOptions = () => ({
    headerShown: false
  });

  state = {
    one: "",
    two: "",
    three: "",
    four: "",
    five: "",
    six: "",
    codeExpireTime: 300,
    errorMessage: ""
  };

  expireTimer = null;

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("VerifyPhoneNumberScreen");
    this.props.verifyPhoneNumberResendCode();
    this.startExpireTimer();
  }

  componentWillUnmount() {
    this.stopExpireTimer();
  }

  get codeExpireTimeString() {
    const { codeExpireTime } = this.state;
    let min = codeExpireTime / 60;
    let sec = codeExpireTime % 60;

    min = parseInt(min);
    min = min > 10 ? min : `0${min}`;
    sec = sec > 10 ? sec : `0${sec}`;

    return `${min}:${sec}`;
  }

  startExpireTimer = () => {
    this.stopExpireTimer();
    this.expireTimer = setInterval(() => {
      const { codeExpireTime } = this.state;
      if (codeExpireTime) {
        this.setState({
          codeExpireTime: codeExpireTime - 1
        });
      } else {
        this.stopExpireTimer();
      }
    }, 1000);
  };

  stopExpireTimer = () => {
    if (this.expireTimer !== null) {
      clearInterval(this.expireTimer);
      this.expireTimer = null;
    }
  };

  get verificationCode() {
    const { one, two, three, four, five, six } = this.state;
    return `${one}${two}${three}${four}${five}${six}`;
  }

  onVerifyPhoneNumber = () => {
    const { verifyPhoneNumber } = this.props;
    this.setState({ errorMessage: "" });
    verifyPhoneNumber(this.verificationCode).then(({ data }) => {
      const { status } = data;
      if (status === "error") {
        this.setState({ errorMessage: "Incorrect code." });
      }
    });
  };

  onCodeChange = (text, ref) => {
    this.setState({ [ref]: text, errorMessage: "" });
    const { one, two, three, four, five, six } = this.refs;
    switch (ref) {
      case "one":
        if (text) {
          two.focus();
        }
        break;
      case "two":
        if (text) {
          three.focus();
        } else {
          one.focus();
        }
        break;
      case "three":
        if (text) {
          four.focus();
        } else {
          two.focus();
        }
        break;
      case "four":
        if (text) {
          five.focus();
        } else {
          three.focus();
        }
        break;
      case "five":
        if (text) {
          six.focus();
        } else {
          four.focus();
        }
        break;
      case "six":
        if (text) {
          six.blur();
        } else {
          five.focus();
        }
        break;
      default:
        console.warn(`Unexpected ref ${ref}`);
    }
  };

  render() {
    const {
      authLoading,
      validationErrors,
      user,
      verifyPhoneNumberResendCode,
      navigation
    } = this.props;
    const { countryCode, phoneNumber } = user;
    const { one, two, three, four, five, six, errorMessage } = this.state;
    return (
      <SignUpContainer
        title="Phone Verfication"
        index={2}
        navigation={navigation}
      >
        <View style={styles.descriptionContainer}>
          <Text style={styles.descriptionText}>
            A 6-digit code has been sent to
          </Text>

          <Text style={styles.phoneText}>
            {countryCode} {phoneNumber}
          </Text>

          <Text style={styles.wrongNumberText}>
            {"Wrong phone number? "}
            <Text style={styles.backText} onPress={() => navigation.goBack()}>
              Back
            </Text>
          </Text>

          {errorMessage ? (
            <Text style={styles.errorText}>{errorMessage}</Text>
          ) : null}
        </View>

        <View style={styles.inputForm}>
          <TextInput
            onChangeText={text => this.onCodeChange(text, "one")}
            value={one}
            ref="one"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="phone-pad"
            autoCompleteType="off"
          />
          <TextInput
            onChangeText={text => this.onCodeChange(text, "two")}
            value={two}
            ref="two"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="phone-pad"
          />
          <TextInput
            onChangeText={text => this.onCodeChange(text, "three")}
            value={three}
            ref="three"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="phone-pad"
          />
          <TextInput
            onChangeText={text => this.onCodeChange(text, "four")}
            value={four}
            ref="four"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="phone-pad"
          />
          <TextInput
            onChangeText={text => this.onCodeChange(text, "five")}
            value={five}
            ref="five"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="number-pad"
          />
          <TextInput
            onChangeText={text => this.onCodeChange(text, "six")}
            value={six}
            ref="six"
            style={styles.codeText}
            maxLength={1}
            selectionColor="white"
            keyboardType="number-pad"
          />
        </View>

        <View style={styles.descriptionContainer2}>
          <Text style={styles.expireText}>
            The 6-digit code will expire in{" "}
            <Text style={styles.durationText}>{this.codeExpireTimeString}</Text>
          </Text>
          <Text style={styles.dontReceivedText}>Didn’t receive the code?</Text>

          <TouchableOpacity
            onPress={() => {
              verifyPhoneNumberResendCode();
              this.setState({
                codeExpireTime: 300
              });
            }}
            disabled={authLoading}
            style={styles.resendBtn}
          >
            <Text style={styles.resendBtnText}>Resend</Text>
          </TouchableOpacity>
        </View>

        <SignUpButton
          loading={authLoading}
          disabled={this.verificationCode.length < 6}
          title="Verify"
          onPress={this.onVerifyPhoneNumber}
        />
      </SignUpContainer>
    );
  }
}

const mapStateToProps = state => ({
  authLoading: getAuthLoading(state),
  validationErrors: getValidationErrors(state),
  user: getUser(state)
});

const mapDispatchToProps = {
  verifyPhoneNumber,
  verifyPhoneNumberResendCode
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VerifyPhoneNumberScreen);
