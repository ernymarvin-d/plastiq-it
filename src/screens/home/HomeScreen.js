import React from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl
} from "react-native";
import LottieView from "lottie-react-native";

import { connect } from "react-redux";
import { SearchBar } from "react-native-elements";

import { getCashbackCredit, refreshCards } from "@store/cashback/actions";
import { getOffers, setSearchKey } from "@store/offer/actions";

import { getUser } from "@store/auth/selectors";
import CardsContainer from "../../components/CardsContainer";
import styles from "../styles/HomeScreenStyle";

class HomeScreen extends React.PureComponent {
  state = {
    refreshing: false
  };

  componentDidMount() {
    this._getCashbackCredit();

    // Segment Screen
    const { analytics } = global;
    analytics.screen("HomeScreen");

    // firebase.crashlytics().crash();
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.props.refreshCards(() => this.setState({ refreshing: false }));
  };

  _getCashbackCredit = () => {
    this.props.getCashbackCredit();
  };

  updateSearch = search_key => {
    this.props.setSearchKey(search_key);

    const params = {
      category: search_key,
      search: search_key
    };

    setTimeout(() => {
      this.props.getOffers(params);
      this.props.navigation.navigate("Offers");
    }, 2000);
  };

  _handleClickOnCashback = () => {
    this.props.navigation.navigate("Cashbacks");
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <SearchBar
            platform="default"
            onChangeText={this.updateSearch}
            value={this.props.search_key}
            containerStyle={styles.searchbarContainer}
            inputContainerStyle={styles.searchbar_inputContainer}
            inputStyle={styles.searchbar_input}
            placeholder=""
            placeholderTextColor="white"
            searchIcon={styles.search_icon}
            clearIcon={styles.clear_icon}
            lightTheme
          />

          <View style={styles.headerTextContainer}>
            <TouchableOpacity
              onPress={this._handleClickOnCashback}
              style={styles.cashbackContainer}
            >
              <Text style={styles.titleText}>
                Hey {this.props.user_detail.name}! You've earned
              </Text>
              {this.props.cashback_credit_loading ? (
                <LottieView
                  source={require("@assets/animations/three_dot_loader.json")}
                  autoPlay
                  loop
                  speed={1}
                  style={styles.lottieLoader}
                />
              ) : (
                <Text style={styles.cashbackText}>
                  ${this.props.cashback_credit.amount}
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
              tintColor="#a84bdd"
            />
          }
          // backgroundColor={'#a84bdd'}
          keyboardShouldPersistTaps="handled"
        >
          <CardsContainer
            bannerCards={this.props.home_banner_cards}
            carouselCards={this.props.carousel_cards}
            staticCards={this.props.static_cards}
          />
        </ScrollView>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  // title: "Home Screen"
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null,
  headerLeft: (
    <TouchableOpacity style={{ paddingLeft: 20 }}>
      <Image source={require("@assets/images/dashboard/menu.png")} />
    </TouchableOpacity>
  )
};

const mapState = state => {
  const { cashback, auth, offer } = state;

  return {
    cashback_credit_loading: cashback.cashback_credit_loading,
    cashback_credit: cashback.cashback_credit,
    home_banner_cards: cashback.home_banner_cards,
    carousel_cards: cashback.carousel_cards,
    static_cards: cashback.static_cards,
    content_cards_loading: cashback.content_cards_loading,
    user_detail: getUser(state),
    search_key: offer.search_key
  };
};

const mapDispatch = dispatch => ({
  getCashbackCredit: () => dispatch(getCashbackCredit()),
  refreshCards: callback => dispatch(refreshCards(callback)),
  setSearchKey: search_key => dispatch(setSearchKey(search_key)),
  getOffers: params => dispatch(getOffers(params))
});

export default connect(mapState, mapDispatch)(HomeScreen);
