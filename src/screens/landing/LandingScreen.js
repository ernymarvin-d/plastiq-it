import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import styles from "../styles/LandingScreenStyle";
import AppConfig from "../../config/AppConfig";

const SCREEN_WIDTH = Dimensions.get("window").width;

const Screen = props => (
  <View
    style={{
      backgroundColor: "#a64adb",
      paddingHorizontal: 30,
      marginTop: 50
    }}
  >
    <Text
      style={{
        fontFamily: "Montserrat-Regular",
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: "center",
        color: "white"
      }}
    >
      {props.text}
    </Text>
  </View>
);
class LandingScreen extends React.PureComponent {
  static navigationOptions = () => ({
    header: null
  });

  SCREENS = [
    <Screen text="Sign up to Plastiq in less than 30 seconds, and earn cashback at your favourite brands in-store and online." />,
    <Screen text="Simply connect your spending account (bank account, credit card... even PayPal) and you can start to earn cashback automatically." />,
    <Screen text="Plastiq is FREE, Effortless and Secure. Signup to Plastiq now and Get PAID To Shop." />
  ];

  state = {
    activeTab: 0
  };

  async componentDidMount() {
    const { segment_analytics_key } = AppConfig;
    const { analytics } = global;

    await analytics.setup(segment_analytics_key, {
      // Record screen views automatically!
      recordScreenViews: true,
      // Record certain application events automatically!
      trackAppLifecycleEvents: true
    });

    // Segment Screen
    analytics.screen("LandingScreen");

    const { Sentry } = global;
    const { sentry_dsn } = AppConfig;
    Sentry.init({
      dsn: sentry_dsn
    });
  }

  render() {
    return (
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.skipBtn}
            onPress={() => {
              this.props.navigation.navigate("SignInEmailScreen");
            }}
          >
            <Text style={styles.skipBtnText}>Skip</Text>
          </TouchableOpacity>
          <View style={styles.content}>
            <Image
              style={styles.logo}
              source={require("@assets/images/landing/illustration.gif")}
            />
            <Carousel
              ref={ref => (this.carouselRef = ref)}
              data={this.SCREENS}
              renderItem={({ item }) => item}
              onSnapToItem={i => this.setState({ activeTab: i })}
              sliderWidth={SCREEN_WIDTH}
              itemWidth={SCREEN_WIDTH}
              slideStyle={{ width: SCREEN_WIDTH }}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
            />
            <View style={{ backgroundColor: "#fff" }}>
              <Pagination
                dotsLength={this.SCREENS.length}
                activeDotIndex={this.state.activeTab}
                containerStyle={styles.paginationContainer}
                dotStyle={styles.activeDot}
                inactiveDotStyle={
                  {
                    // Define styles for inactive dots here
                  }
                }
                inactiveDotOpacity={0.5}
                inactiveDotScale={0.8}
              />
            </View>

            <View style={styles.btnContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("SignInEmailScreen");
                }}
                style={styles.startedBtn}
                testID="get-started-btn"
              >
                <Text style={styles.startedBtnText}>Get Started</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

LandingScreen.navigationOptions = {
  header: null
};

export default LandingScreen;
