import React from "react";
import { View, ActivityIndicator, StatusBar } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { verifyPhoneNumberResendCode, setUser } from "@store/auth/actions";

class LoadingScreen extends React.PureComponent {
  componentDidMount() {
    this.loadApp();
  }

  loadApp = async () => {
    const { navigation, setUser } = this.props;
    const token = await AsyncStorage.getItem("tokenV2");
    const user = await AsyncStorage.getItem("userV2");
    if (token && user) {
      const userJSON = JSON.parse(user);
      setUser(userJSON);
      if (!userJSON.verified) {
        navigation.navigate("VerifyPhoneNumberScreen");
      } else if (!userJSON.passcode) {
        navigation.navigate("NewPasscodeScreen");
      } else {
        navigation.navigate("PasscodeScreen");
      }
    } else {
      navigation.navigate("AuthStack");
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  verifyPhoneNumberResendCode,
  setUser
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen);
