import React from "react";
import { View, FlatList, Text, Image, TouchableOpacity } from "react-native";
import LottieView from "lottie-react-native";

import { connect } from "react-redux";

import { getMerchantDetails } from "@store/offer/actions";
import ScreenHeader from "../../components/ScreenHeader";
import CouponItem from "../../components/CouponItem";

import styles from "../styles/MerchantScreenStyle";

class MerchantScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("MerchantScreen");

    const merchant_id = this.props.navigation.getParam("merchant_id");
    const source = this.props.navigation.getParam("source");
    this.props.getMerchantDetails({ source, merchant_id });
    // this.setState({ loading: true });
  }

  _renderCouponItem = ({ item }) => {
    return <CouponItem item={item} />;
  };

  handleOnPressShopNow = tracking_url => {
    // this.props.handleShopNow(tracking_url);
    this.props.navigation.navigate("ShopWebView", {
      link: tracking_url,
      prevScreen: "Merchant",
      title: "Shop Now"
    });
  };

  render() {
    const name = this.props.navigation.getParam("name");
    const { merchant_loading, merchant_details } = this.props;

    if (merchant_details != null) {
      const { commission_rate, commission_type_symbol } = merchant_details;
      var percent_or_dollar_comment = `Earn up to ${commission_rate}% Cashback`;
      if (commission_type_symbol == "$") {
        percent_or_dollar_comment = `Earn up to $${commission_rate} Cashback`;
      }
    }

    return (
      <View style={styles.container}>
        <ScreenHeader title={name} />
        {merchant_loading || merchant_details == null ? (
          <View style={styles.loadingContainer}>
            <LottieView
              source={require("@assets/animations/coins-grow.json")}
              autoPlay
              loop
              speed={5}
              style={styles.lottieLoader}
            />
          </View>
        ) : (
          <View style={styles.screenContent}>
            <View style={styles.logoContainer}>
              <Image
                style={styles.avatar}
                source={{ uri: merchant_details.avatar_url }}
              />
            </View>
            <View style={styles.couponsContainer}>
              {merchant_details.coupons.length > 0 ? (
                <FlatList
                  data={merchant_details.coupons}
                  renderItem={({ item }) => this._renderCouponItem({ item })}
                  keyExtractor={(item, index) => index.toString()}
                />
              ) : (
                <Text style={styles.textNotification}>No coupons</Text>
              )}
            </View>
            <View style={styles.bottomContainer}>
              <TouchableOpacity
                style={styles.shopBtn}
                onPress={() =>
                  this.handleOnPressShopNow(merchant_details.tracking_url)
                }
              >
                <Text style={styles.shopBtnText}>Shop</Text>
                <Text style={styles.shopBtnText}>Now</Text>
              </TouchableOpacity>
              <Text style={styles.textOffer}>{percent_or_dollar_comment}</Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}

MerchantScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => {
  const { offer } = state;

  return {
    merchant_loading: offer.merchant_loading,
    merchant_details: offer.merchant_details
  };
};

const mapDispatch = dispatch => ({
  getMerchantDetails: detail_info => dispatch(getMerchantDetails(detail_info))
});

export default connect(mapState, mapDispatch)(MerchantScreen);
