import React from "react";
import { View, FlatList, Text, Image } from "react-native";
import LottieView from "lottie-react-native";
import { SearchBar } from "react-native-elements";

import { connect } from "react-redux";

import { getOffers, loadMoreOffers, setSearchKey } from "@store/offer/actions";
import OfferItem from "../../components/OfferItem";
import CategoryItem from "../../components/CategoryItem";
import {
  GIFT_CARD,
  AIR_TRANSPORT,
  CLOTHES_HANGER,
  LIPSTICK,
  PRESENT_BOX_WITH_BIG_BOW,
  ROUNDED_PLUG,
  SNACK_FOR_WATCH_SPORTIVE_GAME_OF_RUGBY,
  STARS
} from "../../components/Images";
import styles from "../styles/OffersScreenStyle";

class OffersScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("OffersScreen");

    this.props.getOffers();
  }

  _renderOfferItem = ({ item }) => {
    return <OfferItem item={item} />;
    // return <OfferItem item={item} handleShopNow={this._handleShopNow}/>;
  };

  // _handleShopNow = (target_url) => {
  //   console.log('_handleShopNow target_url', target_url);
  // }

  updateSearch = search_key => {
    this.props.setSearchKey(search_key);

    const params = {
      category: search_key,
      search: search_key
    };

    setTimeout(() => {
      this.props.getOffers(params);
    }, 2000);
  };

  handleLoadMore = () => {
    const { current_page, last_page, more_offers_loading } = this.props;

    // console.log('/handleLoadMore props', this.props);
    if (!more_offers_loading && current_page <= last_page) {
      const { search_key } = this.props;

      const params = {
        category: search_key,
        search: search_key,
        page: current_page + 1
      };

      this.props.loadMoreOffers(params);
    } else {
      console.log("/handleLoadMore => Nothing more");
    }
  };

  render() {
    const offers_list = this.props.offers_loading ? (
      <View style={styles.box}>
        <LottieView
          source={require("@assets/animations/coins-grow.json")}
          autoPlay
          loop
          speed={5}
          style={styles.lottieLoader}
        />
      </View>
    ) : this.props.offers_list.length ? (
      <View style={styles.offersContainer}>
        <FlatList
          data={this.props.offers_list}
          renderItem={this._renderOfferItem}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={() => this.handleLoadMore()}
          onEndReachedThreshold={0.3}
          contentContainerStyle={{ paddingTop: 10 }}
        />
      </View>
    ) : (
      <View style={styles.box}>
        <Text style={styles.warningText}>No Offers!</Text>
      </View>
    );

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <SearchBar
            platform="default"
            onChangeText={this.updateSearch}
            value={this.props.search_key}
            containerStyle={styles.searchbarContainer}
            inputContainerStyle={styles.searchbar_inputContainer}
            inputStyle={styles.searchbar_input}
            placeholder=""
            placeholderTextColor="white"
            searchIcon={styles.search_icon}
            clearIcon={styles.clear_icon}
            lightTheme
          />
          <View style={styles.headerCategoriesContainer}>
            <View style={styles.categoriesContent}>
              <CategoryItem category="Trending" source={STARS} />
              <CategoryItem category="Fashion" source={CLOTHES_HANGER} />
              <CategoryItem category="Beauty" source={LIPSTICK} />
              <CategoryItem category="Travel" source={AIR_TRANSPORT} />
            </View>
            <View style={styles.categoriesContent}>
              <CategoryItem
                category="Gifts"
                source={PRESENT_BOX_WITH_BIG_BOW}
              />
              <CategoryItem
                category="Food"
                source={SNACK_FOR_WATCH_SPORTIVE_GAME_OF_RUGBY}
              />
              <CategoryItem category="Electrical" source={ROUNDED_PLUG} />
              <CategoryItem category="Gift card" source={GIFT_CARD} />
            </View>
          </View>
        </View>

        {offers_list}
      </View>
    );
  }
}

OffersScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

const mapState = state => {
  const { offer } = state;

  return {
    offers_list: offer.offers_list,
    offers_loading: offer.offers_loading,
    current_page: offer.current_page,
    last_page: offer.last_page,
    search_key: offer.search_key,
    more_offers_loading: offer.more_offers_loading
  };
};

const mapDispatch = dispatch => ({
  getOffers: params => dispatch(getOffers(params)),
  loadMoreOffers: params => dispatch(loadMoreOffers(params)),
  setSearchKey: search_key => dispatch(setSearchKey(search_key))
});

export default connect(mapState, mapDispatch)(OffersScreen);
