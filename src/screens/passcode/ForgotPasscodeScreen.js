import React from "react";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  StatusBar
} from "react-native";
import { connect } from "react-redux";
import Modal from "react-native-modal";

import { PASSCODE_BACK_3, PASSCODE_CELLPHONE_3 } from "@components/Images";

import { getUser } from "@store/auth/selectors";
import {
  verifyPhoneNumber,
  verifyPhoneNumberResendCode
} from "@store/auth/actions";
import styles from "../styles/ForgotPasscodeScreenStyle";

class ForgotPasscodeScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    codeExpireTime: 300,
    modal_message: "",
    code_1: "",
    code_2: "",
    code_3: "",
    code_4: "",
    code_5: "",
    code_6: "",
    isLoading: false
  };

  expireTimer = null;

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("ForgotPasscodeScreen");
    this.props.verifyPhoneNumberResendCode();
    this.startExpireTimer();
  }

  componentWillUnmount() {
    this.stopExpireTimer();
  }

  get codeExpireTimeString() {
    const { codeExpireTime } = this.state;
    let min = codeExpireTime / 60;
    let sec = codeExpireTime % 60;

    min = parseInt(min);
    min = min > 10 ? min : `0${min}`;
    sec = sec > 10 ? sec : `0${sec}`;

    return `${min}:${sec}`;
  }

  startExpireTimer = () => {
    this.stopExpireTimer();
    this.expireTimer = setInterval(() => {
      const { codeExpireTime } = this.state;
      if (codeExpireTime) {
        this.setState({
          codeExpireTime: codeExpireTime - 1
        });
      } else {
        this.stopExpireTimer();
      }
    }, 1000);
  };

  stopExpireTimer = () => {
    if (this.expireTimer !== null) {
      clearInterval(this.expireTimer);
      this.expireTimer = null;
    }
  };

  resendCode = () => {
    this.setState({
      modal_message: "Resending code."
    });

    this.props.verifyPhoneNumberResendCode().then(() =>
      this.setState({
        codeExpireTime: 300
      })
    );
  };

  onVerifyCode = () => {
    this.setState({
      modal_message: "Verifying code."
    });

    const code = `${this.state.code_1}${this.state.code_2}${this.state.code_3}${this.state.code_4}${this.state.code_5}${this.state.code_6}`;

    if (code.length != 6) {
      this.refs.input_1.focus();
      return;
    }

    const { verifyPhoneNumber } = this.props;
    this.setState({ isLoading: true });
    verifyPhoneNumber(code).finally(() => this.setState({ isLoading: false }));
  };

  render() {
    const { navigation, user } = this.props;
    const { countryCode, phoneNumber } = user;
    const { isLoading } = this.state;
    return (
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        keyboardShouldPersistTaps="handled"
      >
        <View style={styles.content}>
          <StatusBar barStyle="default" />
          <KeyboardAvoidingView
            behavior="position"
            contentContainerStyle={{
              alignItems: "center"
            }}
            // keyboardVerticalOffset="50"
          >
            {/* header */}
            <View style={styles.headerContent}>
              <TouchableOpacity
                onPress={() => navigation.navigate("PasscodeScreen")}
                style={styles.headerBackBtn}
              >
                <Image
                  style={styles.headerBackImage}
                  source={PASSCODE_BACK_3}
                />
              </TouchableOpacity>

              <View style={styles.headerTitleContainer}>
                <Text style={styles.headerTitle}>Forgot Passcode</Text>
              </View>
            </View>
            {/* end header */}

            {/* image */}
            <View style={styles.phoneImageContent}>
              <Image style={styles.phoneImage} source={PASSCODE_CELLPHONE_3} />
            </View>
            {/* end image */}

            {/* text */}
            <View style={styles.textContent}>
              <Text style={styles.textTitle}>Verification Code</Text>
              <Text style={styles.textSubTitle}>
                A 6-digit code has been sent to
              </Text>

              <Text style={styles.textNumber}>
                {countryCode} {phoneNumber}
              </Text>

              <Text
                style={{
                  ...styles.textSubTitle,
                  color: "red",
                  height: 15,
                  marginTop: 5,
                  width: "90%",
                  textAlign: "center"
                }}
              >
                {this.props.error_message}
              </Text>
            </View>
            {/* end text */}

            {/* code */}
            <View style={{ ...styles.codeContent, ...styles.codeInputShadow }}>
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_1: text });
                  if (text) this.refs.input_2.focus();
                }}
                value={this.state.code_1}
                ref="input_1"
              />
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_2: text });
                  if (text) this.refs.input_3.focus();
                }}
                value={this.state.code_2}
                ref="input_2"
              />
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_3: text });
                  if (text) this.refs.input_4.focus();
                }}
                value={this.state.code_3}
                ref="input_3"
              />
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_4: text });
                  if (text) this.refs.input_5.focus();
                }}
                value={this.state.code_4}
                ref="input_4"
              />
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_5: text });
                  if (text) this.refs.input_6.focus();
                }}
                value={this.state.code_5}
                ref="input_5"
              />
              <TextInput
                style={styles.codeInput}
                selectionColor="rgb(168, 75, 221)"
                maxLength={1}
                keyboardType="phone-pad"
                autoCompleteType="off"
                onChangeText={text => {
                  this.setState({ code_6: text });
                  if (text) this.refs.input_6.blur();
                }}
                ref="input_6"
              />
            </View>
            {/* end code */}

            {/* text */}
            <View style={styles.textBottomContent}>
              <Text style={styles.textBottom}>
                The 6-digit code will expire in{" "}
                <Text style={styles.textBottomTime}>
                  {this.codeExpireTimeString}
                </Text>
              </Text>

              <Text style={styles.textBottom}>
                Didn’t received the code?
                <Text style={styles.textBottomResend} onPress={this.resendCode}>
                  {" "}
                  Resend
                </Text>
              </Text>
            </View>
            {/* end text */}

            {/* button */}
            <View style={styles.buttonContent}>
              <View style={styles.buttonShadow}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.onVerifyCode}
                >
                  <Text style={styles.buttonText}>Verify</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
          {/* end button */}
        </View>

        <Modal
          isVisible={isLoading}
          animationIn="fadeIn"
          animationOut="fadeOut"
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator color="white" size="large" />
            <Text style={{ color: "white" }}>Please wait!</Text>
            <Text style={{ color: "white" }}>{this.state.modal_message}</Text>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  user: getUser(state)
});

const mapDispatchToProps = {
  verifyPhoneNumber,
  verifyPhoneNumberResendCode
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPasscodeScreen);
