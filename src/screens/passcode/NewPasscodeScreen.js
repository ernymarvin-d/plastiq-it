import React from "react";
import {
  View,
  Text,
  Image,
  Alert,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet
} from "react-native";
import { PASSCODE_BACK_WHITE_3, PASSCODE_DELETE_3 } from "@components/Images";
import { passcodeUpdate } from "@service/PlastiqClient";

import Passcode from "@components/passcode/Passcode";

const passcodeStyles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    // height: "100%",
    backgroundColor: "#a84bdd"
  },
  content: {
    padding: 20,
    marginTop: 40
  },
  brandLogoContent: {
    alignItems: "center"
  },
  brandLogo: {
    width: 260,
    resizeMode: "contain"
  },
  textContent: {
    flexDirection: "column",
    alignItems: "center",
    marginTop: 20
  },
  textHeading: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    textAlign: "center",
    color: "#ffffff"
  },
  textMessage: {
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    textAlign: "center",
    color: "#ffffff",
    marginTop: 10
  },
  circleContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 90
  },
  circle: {
    width: 18,
    height: 18,
    borderWidth: 1.5,
    borderColor: "#ffffff",
    borderRadius: 100
  },
  circleWithBG: {
    width: 18,
    height: 18,
    borderWidth: 1.5,
    borderColor: "#ffffff",
    backgroundColor: "#ffffff",
    borderRadius: 100
  },
  numberContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginTop: 20
  },

  numberCircle: {
    width: 70,
    height: 70,
    borderStyle: "solid",
    borderWidth: 1.5,
    borderColor: "#ffffff",
    borderRadius: 50,
    justifyContent: "center"
  },
  numberCircleNoBorder: {
    width: 70,
    height: 70,
    justifyContent: "center",
    alignItems: "center"
  },
  numberCircleText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 35,
    textAlign: "center",
    color: "#ffffff"
  },
  numberCircleTextNoBorder: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    textAlign: "center",
    color: "#ffffff"
  },
  numberCircleImage: {
    width: 40,
    resizeMode: "contain"
  },
  TextMessage: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    color: "white",
    // color: "black",
    marginTop: 5
  },
  textTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    color: "white"
    // color: "black"
  }
});

export default class NewPasscodeScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    selected_number: "",
    passcode_title: "Create New Passcode",
    passcode_message: "Enter a 4-digit code",
    passcode: "",
    isLoading: false
  };

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("NewPasscodeScreen");
  }

  _selectNumber = number => {
    const selected_number = `${this.state.selected_number}${number}`;

    this.setState({
      selected_number
    });

    setTimeout(() => {
      if (selected_number.length >= 4) {
        if (!this.state.passcode) {
          this.setState({
            passcode: selected_number,
            passcode_message: "Re-enter passcode to confirm",
            passcode_title: "Create New Passcode"
          });
        } else {
          this.setState({
            passcode_confimation: selected_number
          });

          if (this.state.passcode !== selected_number) {
            this.setState({
              passcode: "",
              passcode_confimation: "",
              passcode_message: "Enter a 4-digit code",
              passcode_title: "Confirm New Passcode"
            });
            Alert.alert("Passcode did not match.", "Please try again.");
          } else {
            this._updatePasscode();
          }
        }

        this.setState({
          selected_number: ""
        });
      }
    }, 500);
  };

  _updatePasscode = () => {
    const { navigation } = this.props;
    const { passcode } = this.state;
    this.setState({ isLoading: true });
    passcodeUpdate(passcode)
      .then(() => {
        navigation.navigate("MainTab");
        this.setState({
          passcode: "",
          passcode_confimation: "",
          passcode_message: "Enter a 4-digit code",
          passcode_title: "Confirm New Passcode"
        });
      })
      .finally(() => this.setState({ isLoading: false }));
  };

  _deleteNumber = number => {
    let { selected_number } = this.state;
    selected_number = selected_number.slice(0, -1);

    this.setState({
      selected_number
    });
  };

  _cancelNewPasscode = () => {
    this.props.navigation.navigate("PasscodeScreen");
  };

  _back = () => {
    this.props.navigation.navigate("PasscodeScreen");
  };

  render() {
    const DEFAULT_COLOR = "#fff";
    // const DEFAULT_COLOR = "#33203f";
    const WHITE = "#ffffff";
    const PURPLE = "#ffffff";
    // const PURPLE = "#a84bdd";

    const styles = passcodeStyles;

    styles.numberCircle = {
      ...styles.numberCircle,
      borderColor: DEFAULT_COLOR
    };

    styles.circleWithBG = {
      ...styles.circleWithBG,
      borderColor: PURPLE,
      backgroundColor: PURPLE
    };

    styles.circle = {
      ...styles.circle,
      borderColor: PURPLE
    };

    styles.headerBackImage = {
      width: 15,
      height: 25,
      resizeMode: "contain"
    };

    styles.textContent = {
      alignItems: "center"
    };

    const { isLoading } = this.state;

    return (
      <ScrollView style={{ ...styles.container, backgroundColor: "#a84bdd" }}>
        <View style={styles.content}>
          {/* header */}
          <View style={styles.headerContent}>
            <TouchableOpacity
              onPress={() => {
                this._back();
              }}
              style={{ width: 30 }}
            >
              <Image
                style={styles.headerBackImage}
                source={PASSCODE_BACK_WHITE_3}
              />
            </TouchableOpacity>
          </View>
          {/* end header */}

          <View style={{ marginTop: "10%" }} />

          {/* text */}
          <View style={styles.textContent}>
            <Text style={styles.textTitle}>{this.state.passcode_title}</Text>
            <Text style={styles.TextMessage}>
              {this.state.passcode_message}
            </Text>

            <Text
              style={{
                ...styles.TextMessage,
                fontSize: 13,
                height: 15,
                marginTop: 10
              }}
            >
              {isLoading ? "Please wait!" : this.props.error_message}
            </Text>
          </View>
          {/* end text */}

          <View style={{ marginTop: "6%" }} />

          {/* text */}
          <View style={styles.circleContent}>
            <View
              style={
                this.state.selected_number.length >= 1
                  ? styles.circleWithBG
                  : styles.circle
              }
            />
            <View
              style={
                this.state.selected_number.length >= 2
                  ? styles.circleWithBG
                  : styles.circle
              }
            />
            <View
              style={
                this.state.selected_number.length >= 3
                  ? styles.circleWithBG
                  : styles.circle
              }
            />
            <View
              style={
                this.state.selected_number.length >= 4
                  ? styles.circleWithBG
                  : styles.circle
              }
            />
          </View>
          {/* end text */}

          <View style={{ marginTop: "10%" }} />

          {/* number */}
          <View style={{ ...styles.numberContent, bottom: 0 }}>
            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 1 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(1);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 1 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 1 ? WHITE : DEFAULT_COLOR
                }}
              >
                1
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 2 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(2);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 2 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 2 ? WHITE : DEFAULT_COLOR
                }}
              >
                2
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 3 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(3);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 3 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 3 ? WHITE : DEFAULT_COLOR
                }}
              >
                3
              </Text>
            </TouchableHighlight>
          </View>

          <View style={styles.numberContent}>
            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 4 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(4);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 4 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 4 ? WHITE : DEFAULT_COLOR
                }}
              >
                4
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 5 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(5);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 5 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 5 ? WHITE : DEFAULT_COLOR
                }}
              >
                5
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 6 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(6);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 6 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 6 ? WHITE : DEFAULT_COLOR
                }}
              >
                6
              </Text>
            </TouchableHighlight>
          </View>

          <View style={styles.numberContent}>
            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 7 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(7);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 7 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 7 ? WHITE : DEFAULT_COLOR
                }}
              >
                7
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 8 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(8);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 8 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 8 ? WHITE : DEFAULT_COLOR
                }}
              >
                8
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number == 9 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(9);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 9 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number == 9 ? WHITE : DEFAULT_COLOR
                }}
              >
                9
              </Text>
            </TouchableHighlight>
          </View>

          <View style={styles.numberContent}>
            <TouchableOpacity
              style={styles.numberCircleNoBorder}
              onPress={() => {
                this._cancelNewPasscode();
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleTextNoBorder,
                  color: DEFAULT_COLOR
                }}
              >
                Cancel
              </Text>
            </TouchableOpacity>

            <TouchableHighlight
              underlayColor={PURPLE}
              style={{
                ...styles.numberCircle,
                borderColor:
                  this.state.touched_number === 0 ? WHITE : DEFAULT_COLOR
              }}
              onPress={() => {
                this._selectNumber(0);
              }}
              onShowUnderlay={() => {
                this.setState({ touched_number: 0 });
              }}
              onHideUnderlay={() => {
                this.setState({ touched_number: false });
              }}
            >
              <Text
                style={{
                  ...styles.numberCircleText,
                  color: this.state.touched_number === 0 ? WHITE : DEFAULT_COLOR
                }}
              >
                0
              </Text>
            </TouchableHighlight>

            <View style={styles.numberCircleNoBorder}>
              <TouchableOpacity
                onPress={() => {
                  this._deleteNumber();
                }}
              >
                <Image
                  style={styles.numberCircleImage}
                  source={PASSCODE_DELETE_3}
                  // source={PASSCODE_DELETE_DARK_3}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* end number */}
        </View>
      </ScrollView>
    );
  }
}
