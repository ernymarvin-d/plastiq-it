import React from "react";
import { View, Image, SafeAreaView, StyleSheet } from "react-native";
import { Text } from "react-native-elements";

import { PLASTIC_BRAND_LOGO } from "@components/Images";
import Passcode from "@components/passcode/Passcode";

import { passcodeCheck, STATUS } from "@service/PlastiqClient";
import NavigationService from "@service/navigation";
import { authenticate } from "@service/TouchID";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#a84bdd"
  },
  safeArea: {
    alignItems: "center"
  },
  logo: {
    width: "100%",
    resizeMode: "contain",
    marginTop: 24
  },
  title: {
    marginTop: 32
  },
  subtitle: {
    marginVertical: 32
  }
});

export default class PasscodeScreen extends React.PureComponent {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  state = {
    number: "",
    message: ""
  };

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    const { navigation } = this.props;
    analytics.screen("PasscodeScreen");
    authenticate().then(success => {
      if (success) {
        navigation.navigate("MainTab");
      }
    });
  }

  onNumberChanged = number => {
    this.setState({ number, message: "" });
    if (number.length === 4) {
      this.setState({ message: "Please wait..." });
      let set = false;
      passcodeCheck(number)
        .then(({ data }) => {
          const { status, message = "Invalid passcode." } = data;
          switch (status) {
            case STATUS.success:
              NavigationService.navigate("MainTab");
              break;
            case STATUS.error:
              set = true;
              this.setState({ message });
          }
        })
        .finally(() => {
          if (!set) {
            this.setState({ message: "" });
          }
        });
    }
  };

  onReset = () => {
    const { navigation } = this.props;
    navigation.navigate("ForgotPasscodeScreen");
  };

  render() {
    const { number, message } = this.state;
    return (
      <View style={[StyleSheet.absoluteFill, styles.container]}>
        <SafeAreaView style={[StyleSheet.absoluteFill, styles.safeArea]}>
          <Image style={styles.logo} source={PLASTIC_BRAND_LOGO} />
          <Text h2 style={styles.title}>
            Enter Your Passcode
          </Text>
          <Text h4 style={styles.subtitle}>
            {message}
          </Text>

          <Passcode
            number={number}
            onNumberChanged={this.onNumberChanged}
            onReset={this.onReset}
          />
        </SafeAreaView>
      </View>
    );
  }
}
