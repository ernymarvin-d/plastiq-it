/* eslint-disable prettier/prettier */
/* eslint-disable react/require-optimization */
import React from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    Linking,
    Alert,
    Share,
} from 'react-native';
import { ShareDialog } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import { getReferralLink, getReferralStatus } from '@store/referral/actions';
import { getAuthToken } from 'service/Auth';
import API_CONFIG from '../../service/api_config';

import StaticImageCard from '../../components/StaticImageCard';
import styles from '../styles/ReferScreenStyle';

class ReferScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            referral_details_link: API_CONFIG.SERVER_REFERRAL_URL,
        };
        // console.log('referral', this.props.referral);
    }

    componentWillReceiveProps(newProps) {
        console.log({ newProps });
    }

    _openLink = async (url, alter_link) => {
        Linking.openURL(url).catch((error) => {
            if (alter_link == 'messenger') {
                Alert.alert('Please install FB Messenger.');
            } else {
                Linking.openURL(alter_link).catch((error) => {
                    Alert.alert("There's an error with the link.");
                });
            }
        });
    };

    _fbShare = async () => {
        const shareLinkContent = {
            contentType: 'link',
            contentUrl: this.props.facebook_referral.link,
            contentDescription: 'Wow, check out this great app!',
        };

        const alter_link = `https://www.facebook.com/sharer/sharer.php?u=${this.props.facebook_referral.link}`;

        const canShow = await ShareDialog.canShow(shareLinkContent);
        if (canShow) {
            try {
                const { isCancelled, postId } = await ShareDialog.show(
                    shareLinkContent
                );
                if (isCancelled) {
                    console.log('Share cancelled');
                } else {
                    console.log(`Share success with postId: ${postId}`);
                }
            } catch (error) {
                console.log(`ShareDialog error: ${error}`);
                this._openLink(alter_link, '');
            }
        } else {
            console.log('ShareDialog canShow: false');
            this._openLink(alter_link, '');
        }
    };

    _fbMessengerShare = () => {
        const link = `fb-messenger://share?link=${this.props.fbmessenger_referral.link}`;

        this._openLink(link, 'messenger');
    };

    _whatsAppShare = () => {
        const link = `https://api.whatsapp.com/send?phone=&text=${this.props.whatsapp_referral.link}&source=&data=`;

        this._openLink(link, '');
    };

    _twitterShare = () => {
        const link = `http://twitter.com/intent/tweet?url=${this.props.twitter_referral.link}`;

        this._openLink(link, '');
    };

    _openShare = async () => {
        try {
            await Share.share({
                title: 'Plastiq - Invitation Link',
                url: this.props.sms_referral.link,
            });
        } catch (error) {
            console.log(error.message);
        }
    };

    _openWebview = async () => {
        await this.props.navigation.navigate('ReferalWebView', {
            link: this.state.referral_details_link,
            prevScreen: 'Refer',
            title: 'Referal Details',
        });
    };

    async componentDidMount() {
        // Segment Screen
        const { analytics } = global;
        analytics.screen('ReferScreen');
        const token = await getAuthToken();
        console.log({ token });

        this.props.getReferralLink();
        this.props.getReferralStatus();
    }

    render() {
        const { refer_a_friend_cards, referral } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>Give $5, Get $5</Text>
                    </View>

                    <View style={styles.guideContainer}>
                        <View style={{ flex: 3 }}>
                            <Text style={styles.guideText}>
                                When your friend signs up and earns their first
                                cash back reward.
                            </Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <TouchableOpacity
                                style={styles.btnDetails}
                                onPress={() => {
                                    this._openWebview();
                                }}
                            >
                                <Text style={styles.detailsText}>Details</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <ScrollView style={styles.bodyContainer}>
                    {refer_a_friend_cards.length > 0 && (
                        <View style={styles.cardContainer}>
                            <StaticImageCard
                                data={refer_a_friend_cards[0]}
                                onPress={this._openWebview}
                            />
                        </View>
                    )}

                    <View style={styles.btnContainer}>
                        <TouchableOpacity
                            style={styles.btnInvite}
                            onPress={() => {
                                this._openShare();
                            }}
                        >
                            <Text style={styles.btnInviteText}>
                                Share Invitation Link
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.social}>
                        <TouchableOpacity
                            style={styles.socialmedia}
                            onPress={() => {
                                this._fbShare();
                            }}
                        >
                            <Image
                                source={require('@assets/images/referafriend/facebook.png')}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.socialmedia}
                            onPress={() => {
                                this._fbMessengerShare();
                            }}
                        >
                            <Image
                                source={require('@assets/images/referafriend/messenger.png')}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.socialmedia}
                            onPress={() => {
                                this._twitterShare();
                            }}
                        >
                            <Image
                                source={require('@assets/images/referafriend/twitter.png')}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.socialmedia}
                            onPress={() => {
                                this._whatsAppShare();
                            }}
                        >
                            <Image
                                source={require('@assets/images/referafriend/whatsapp.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    {/* end::social */}

                    <Text style={styles.referralText}>
                        My Referral Earnings
                    </Text>

                    <View style={styles.boxes}>
                        <View style={styles.box}>
                            <Text style={styles.boxTextUpper}>
                                {referral.success_referrals}/
                                {referral.total_referrals}
                            </Text>
                            <Text style={styles.boxTextLower}>
                                Successful Referral
                            </Text>
                        </View>
                        <View style={styles.box}>
                            <Text style={styles.boxTextUpper}>
                                ${referral.bonus_earned}
                            </Text>
                            <Text style={styles.boxTextLower}>
                                Bonus Earned
                            </Text>
                        </View>
                    </View>
                    {/* end::boxes */}
                </ScrollView>
            </View>
        );
    }
}

ReferScreen.navigationOptions = {
    title: 'Give $5, Get $5',
    // title: "Home Screen"
    headerStyle: {
        backgroundColor: '#a74adb',
        elevation: 0, // for android
        shadowOpacity: 0, // for ios
        borderBottomWidth: 0, // for ios
    },
    headerTitleStyle: {
        alignSelf: 'center',
        textAlign: 'center',
        flex: 1,
        justifyContent: 'center',
        marginLeft: -20,
        color: '#fff',
    },
    headerLayoutPreset: 'center',
    header: null,
    headerMode: 'float',
    headerLeft: (
        <TouchableOpacity style={{ paddingLeft: 20 }}>
            <Image source={require('@assets/images/referafriend/back.png')} />
        </TouchableOpacity>
    ),
};

const mapState = (state) => {
    const { cashback, referral } = state;

    return {
        referral: referral.referral,
        sms_referral: referral.sms_referral,
        facebook_referral: referral.facebook_referral,
        whatsapp_referral: referral.whatsapp_referral,
        twitter_referral: referral.twitter_referral,
        fbmessenger_referral: referral.fbmessenger_referral,
        referral_loading: referral.referral_loading,
        refer_a_friend_cards: cashback.refer_a_friend_cards,
    };
};

const mapDispatch = (dispatch) => ({
    getReferralLink: () => dispatch(getReferralLink()),
    getReferralStatus: () => dispatch(getReferralStatus()),
});

export default connect(mapState, mapDispatch)(ReferScreen);
