import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  scroll_container: {
    // backgroundColor: '#a84bdd'
  },
  container: {
    flex: 1,
    backgroundColor: "#8e47c1"
  },
  screenContent: {
    flex: 1,
    paddingBottom: 20
  },
  passwordHeading: {
    fontFamily: "Montserrat-Regular",
    width: "100%",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#004183"
  },
  formInput: {
    textAlign: "left",
    marginHorizontal: 20,
    paddingVertical: 12,
    paddingLeft: 10,
    borderBottomColor: "#9858c6",
    borderBottomWidth: 1.5
  },
  inputPhoneArea: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "white"
  },
  inputPhoneText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "white"
  },
  textValue: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "white"
  },
  textValueEdit: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "white"
  },
  textLabel: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.72,
    textAlign: "left",
    color: "black",
    textTransform: "uppercase",
    marginBottom: 14
  },
  btnContainer: {
    width: "100%",
    marginTop: 30
  },
  btn: {
    width: "100%",
    height: 45,
    backgroundColor: "#ecd7f8",
    justifyContent: "center",
    alignItems: "center"
  },
  btnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ae78d3"
  },
  modalBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ae78d3"
  },
  accountDetailsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#004183"
  },
  btnEditText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "white"
  },
  cancelBtnContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    width: "100%",
    paddingHorizontal: 20,
    marginTop: 15
  },
  passwordChangedContainer: {
    width: "100%",
    textAlign: "left"
  },
  modalContainer: {
    backgroundColor: "white",
    width: "100%",
    padding: 20,
    borderRadius: 20
  },
  modalTitleText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    textAlign: "center"
  }
});
