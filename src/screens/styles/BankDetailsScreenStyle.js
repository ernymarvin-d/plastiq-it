import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1
    // backgroundColor: '#fcfcfe'
  },
  screenContent_list: {
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fcfcfe",
    paddingTop: 15
  },
  screenContent_conneting: {
    flex: 1
  },
  box: {
    flex: 1,
    width: "100%",
    height: 70,
    borderRadius: 6,
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    marginBottom: 10,
    alignItems: "center",
    paddingVertical: 15,
    paddingHorizontal: 32
  },
  bankName: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "black"
  },
  btnContainer: {
    width: "100%",
    paddingHorizontal: 32
  },
  btn: {
    width: "100%",
    height: 54,
    borderRadius: 27,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center"
    // marginVertical: 30
  },
  btnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  },
  loadingContainer: {
    flex: 1,
    alignItems: "center"
    // marginTop: -200
  },
  loadingTitle: {
    textAlign: "center",
    marginTop: 10
  },
  lottieLoader: {
    width: Metrics.screenWidth * 0.25
  },
  bankaccountsContainer: {
    flex: 1,
    width: "100%",
    paddingTop: 15
  }
});
