import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  scroll_container: {
    // backgroundColor: '#a84bdd'
  },
  container: {
    flex: 1
  },
  screenContent: {
    flex: 1,
    justifyContent: "center",
    // alignItems: 'center', // IMPORTANT: removt this.
    paddingTop: 15
  },
  textNotification: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    color: "black",
    textAlign: "center"
  },
  box: {
    padding: 15,
    borderRadius: 5,
    borderRadius: 6,
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    marginBottom: 10
  },
  initials: {
    width: 40,
    height: 40,
    backgroundColor: "gray",
    borderRadius: 50,
    marginRight: 12,
    fontFamily: "Montserrat-Regular",
    fontSize: 36,
    fontWeight: "500",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "#a84bdd",
    alignItems: "center",
    justifyContent: "center"
  }
});
