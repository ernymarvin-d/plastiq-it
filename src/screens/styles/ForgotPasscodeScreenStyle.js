import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  content: {
    flex: 1,
    // alignItems: "center", // NOTE: This prob affects to width of children views.
    paddingTop: 30
  },
  headerContent: {
    flexDirection: "row",
    paddingHorizontal: 20,
    marginTop: 17
  },
  headerBackBtn: {
    width: 30
  },
  headerBackImage: {
    width: 20,
    resizeMode: "contain"
  },
  headerTitleContainer: {
    flex: 1,
    alignItems: "center",
    marginLeft: -20
  },
  headerTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    color: "black"
  },
  phoneImageContent: {
    marginTop: 25,
    alignItems: "center"
  },
  phoneImage: {
    width: 55,
    resizeMode: "contain"
  },
  textContent: {
    alignItems: "center",
    marginTop: 35
  },
  textTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    color: "black"
  },
  textSubTitle: {
    color: "black",
    marginTop: 10
  },
  numberRegion: {
    flexDirection: "row"
  },
  textNumber: {
    fontFamily: "Montserrat-Regular",
    color: "black",
    marginTop: 5
  },
  codeContent: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20
  },
  codeInput: {
    width: 39,
    height: 50,
    marginHorizontal: 5,
    borderRadius: 8,
    backgroundColor: "#e3cdf0",
    color: "#9040bd",
    fontSize: 22,
    textAlign: "center",
    fontWeight: "bold"
  },
  codeInputShadow: {
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 1,
      height: 5
    },
    shadowOpacity: 0.3,
    shadowRadius: 5.0
  },
  textBottomContent: {
    marginTop: 30,
    alignItems: "center"
  },
  textBottom: {
    color: "black"
  },

  textBottomTime: {
    width: 50,
    color: "#33203f",
    fontWeight: "bold",
    textAlign: "right"
  },
  textBottomResend: {
    color: "rgb(168, 75, 221)"
  },
  buttonContent: {
    width: "100%",
    alignItems: "center",
    marginTop: 35,
    paddingHorizontal: 50
  },
  buttonShadow: {
    width: "100%",
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 1,
    shadowRadius: 10.0
  },
  button: {
    height: 50,
    borderRadius: 25,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  }
});
