import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  headerContainer: {
    alignItems: "center",
    width: "100%",
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#a64adb",
    paddingTop: 30
  },
  logo: {
    position: "absolute",
    top: Metrics.screenHeight * 0.25 - 350,
    left: 0,
    width: "100%",
    height: 350
  },
  headerTextContainer: {
    flex: 1,
    justifyContent: "center"
  },
  cashbackContainer: {
    alignItems: "center"
  },
  titleText: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 19,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 1.44,
    textAlign: "center",
    color: "#ffffff"
  },
  indicator: {
    justifyContent: "center"
  },
  cashbackText: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 42,
    fontWeight: "500",
    fontStyle: "normal",
    letterSpacing: 1.92,
    textAlign: "center",
    color: "#ffffff"
  },
  searchbarContainer: {
    width: "60%",
    padding: 0,
    marginTop: 0,
    backgroundColor: "transparent",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginTop: Metrics.screenHeight * 0.25 * 0.1,
    marginBottom: 5
  },
  searchbar_inputContainer: {
    height: 30,
    borderRadius: 12,
    backgroundColor: "#924bba"
  },
  searchbar_input: {
    color: "white"
  },
  search_icon: {
    color: "white"
  },
  clear_icon: {
    color: "white"
  },
  lottieLoader: {
    width: Metrics.screenWidth * 0.15
  }
});
