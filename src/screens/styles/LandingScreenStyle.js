import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  scrollContainer: {
    backgroundColor: "#a64adb"
  },
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#a64adb"
  },
  content: {
    alignItems: "center",
    marginTop: 40,
    backgroundColor: "#a64adb"
  },
  logo: {
    height: Metrics.screenWidth * 0.5,
    resizeMode: "contain"
  },
  skipBtn: {
    flex: 1,
    alignSelf: "flex-end",
    marginTop: 20,
    marginRight: 24,
    paddingHorizontal: 5
  },
  skipBtnText: {
    flex: 1,
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 15.6,
    letterSpacing: 0,
    textAlign: "right",
    color: "white"
  },
  btnContainer: {
    width: "100%"
  },
  startedBtn: {
    height: 50,
    backgroundColor: "#ecd7f8",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 14
  },
  startedBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#a64adb"
  },
  paginationContainer: {
    backgroundColor: "#a64adb"
  },
  activeDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: "white"
  }
});
