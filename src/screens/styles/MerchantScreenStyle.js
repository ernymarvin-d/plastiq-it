import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fcfcfe"
  },
  screenContent: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  lottieLoader: {
    width: Metrics.screenWidth * 0.25
  },
  couponsContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "center"
  },
  webviewContainer: {
    flex: 1
  },
  webviewContent: {
    flex: 1
    // paddingHorizontal: 15
  },
  loadingTitle: {
    textAlign: "center",
    marginTop: 10
  },
  textNotification: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    color: "black",
    textAlign: "center"
  },
  logoContainer: {
    alignItems: "center",
    marginBottom: 10
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 45,
    resizeMode: "contain"
  },
  bottomContainer: {
    alignItems: "center"
  },
  textOffer: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "black"
  },
  shopBtn: {
    padding: 3,
    borderRadius: 5,
    backgroundColor: "#b0b2b6"
  },
  shopBtnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "white"
  }
});
