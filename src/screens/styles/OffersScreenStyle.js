import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fcfcfe"
    // marginTop: 20,
  },
  headerContainer: {
    alignItems: "center",
    width: "100%",
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#a64adb",
    paddingTop: 30
  },
  logo: {
    position: "absolute",
    top: Metrics.screenHeight * 0.15 - 350,
    left: 0,
    width: "100%",
    height: 350
  },
  headerCategoriesContainer: {
    flex: 1,
    justifyContent: "space-evenly"
  },
  categoriesContent: {
    flexDirection: "row"
  },
  offersContainer: {
    flex: 1,
    width: "100%"
  },
  box: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#404952"
  },
  lottieLoader: {
    width: Metrics.screenWidth * 0.25
  },
  searchbarContainer: {
    width: "60%",
    padding: 0,
    marginTop: 0,
    backgroundColor: "transparent",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginTop: Metrics.screenHeight * 0.25 * 0.1,
    marginBottom: 5
  },
  searchbar_inputContainer: {
    height: 30,
    borderRadius: 12,
    backgroundColor: "#924bba"
  },
  searchbar_input: {
    color: "white"
  },
  search_icon: {
    color: "white"
  },
  clear_icon: {
    color: "white"
  },
  warningText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    color: "black"
  }
});
