import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#a84bdd"
  },
  content: {
    flex: 1,
    padding: 20,
    marginTop: 40
  },
  upperContainer: {
    flex: 1
    // backgroundColor: 'red'
  },
  brandLogoContent: {
    alignItems: "center",
    flex: 2
  },
  brandLogo: {
    width: 260,
    resizeMode: "contain"
  },
  textContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  textHeading: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    textAlign: "center",
    color: "#ffffff"
  },
  textMessage: {
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    textAlign: "center",
    color: "#ffffff",
    marginTop: 10
  },
  circleContent: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
    // marginHorizontal: 50,
  },
  circle: {
    width: 18,
    height: 18,
    borderWidth: 1.5,
    borderColor: "#ffffff",
    borderRadius: 100,
    marginHorizontal: 15
  },
  circleWithBG: {
    width: 18,
    height: 18,
    borderWidth: 1.5,
    borderColor: "#ffffff",
    backgroundColor: "#ffffff",
    borderRadius: 100,
    marginHorizontal: 15
  },
  lowerContainer: {
    flex: 2
    // backgroundColor: 'green'
  },
  numberContent: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  numberCircle: {
    width: 70,
    height: 70,
    borderStyle: "solid",
    borderWidth: 1.5,
    borderColor: "#ffffff",
    borderRadius: 40,
    justifyContent: "center"
  },
  numberCircleNoBorder: {
    width: 70,
    // height: 70,
    justifyContent: "center",
    alignItems: "center"
  },
  numberCircleText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 35,
    textAlign: "center",
    color: "#ffffff"
  },
  numberCircleTextNoBorder: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    textAlign: "center",
    color: "#ffffff"
  },
  numberCircleImage: {
    width: 40,
    resizeMode: "contain"
  }
});
