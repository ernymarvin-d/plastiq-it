import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fcfcfe"
  },
  headerContainer: {
    alignItems: "center",
    justifyContent: "space-evenly",
    width: "100%",
    height: Metrics.screenHeight * 0.25,
    backgroundColor: "#a64adb",
    paddingTop: 30
  },
  logo: {
    position: "absolute",
    top: -30,
    right: 0,
    width: "100%"
  },
  titleContainer: {
    justifyContent: "center"
  },
  title: {
    fontFamily: "Montserrat-Regular",
    fontSize: 22,
    color: "#fff",
    fontWeight: "bold"
  },
  guideContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    paddingHorizontal: 25
  },
  guideText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    height: 40,
    color: "#ffffff",
    paddingTop: 5
  },
  btnDetails: {
    fontFamily: "Montserrat-Regular",
    height: 30,
    borderRadius: 2,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
  },
  detailsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 13,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#c562fd"
  },
  blueBox: {
    height: ((Metrics.screenWidth - 60) * 200) / 335,
    borderRadius: 10,
    backgroundColor: "#d8eaef",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    marginHorizontal: 30,
    marginTop: 20
  },
  cardContainer: {
    marginTop: 20
  },
  featureImg: {
    width: Metrics.screenWidth - 60,
    height: ((Metrics.screenWidth - 60) * 200) / 335, // 200/335 = ratio
    resizeMode: "contain"
  },
  descriptionContainer: {
    width: "60%",
    position: "absolute",
    left: 5,
    top: 55,
    textAlign: "center",
    alignItems: "center"
  },
  description1: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "black",
    marginBottom: 10
  },
  description2: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "900",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#004183",
    textTransform: "uppercase"
  },
  description3: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "900",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#004183",
    textTransform: "uppercase",
    marginBottom: 13
  },
  btnContainer: {
    width: "100%",
    paddingHorizontal: 32,
    marginTop: 20
  },
  btnInvite: {
    height: 54,
    borderRadius: 5,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center"
  },
  btnInviteText: {
    // width: 147,
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  },
  social: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    marginVertical: 30,
    paddingHorizontal: 50
  },
  socialmedia: {
    // marginTop: 30,
    // marginHorizontal: 12,
    // marginBottom: 30
  },
  referralText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#004183",
    marginBottom: 30
  },
  boxes: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 30,
    paddingHorizontal: 30
  },
  box: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    height: 120,
    borderRadius: 8,
    backgroundColor: "#fff",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    marginHorizontal: 5
  },
  boxTextUpper: {
    fontFamily: "Montserrat-Regular",
    fontSize: 36,
    fontWeight: "500",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "#c562fd"
  },
  boxTextLower: {
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "black"
  },
  bodyContainer: {
    width: "100%"
    // alignItems: "center"
  }
});
