import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  screenContent: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  lottieLoader: {
    width: Metrics.screenWidth * 0.25,
    borderRadius: 9,
    backgroundColor: "#2ECC71"
  },
  navigationBtnContainer: {
    marginHorizontal: 25,
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  navigationBtn: {
    width: 20,
    justifyContent: "center",
    alignItems: "center"
  }
});
