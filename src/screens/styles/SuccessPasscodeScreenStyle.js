import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    paddingTop: 30
  },
  imageContainer: {
    alignItems: "center",
    marginBottom: 50
  },
  logo: {
    width: Metrics.screenWidth * 0.5,
    height: (Metrics.screenWidth * 0.5 * 156) / 187, // 156/187 ratio
    resizeMode: "contain"
  },
  textContainer: {
    alignItems: "center"
  },
  textTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    textAlign: "center",
    color: "black",
    marginBottom: 15
  },
  textPasscode: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    textAlign: "center",
    color: "black"
  },
  btnContainer: {
    width: "100%",
    paddingHorizontal: 50
  },
  btnContinue: {
    height: 50,
    borderRadius: 25,
    backgroundColor: "rgb(144, 64, 189)",
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 1,
    shadowRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  textBtnContinue: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  }
});
