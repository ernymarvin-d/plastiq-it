import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  scroll_container: {
    // backgroundColor: '#a84bdd'
  },
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  screenContent: {
    flex: 1,
    marginTop: -20
  },
  loadingContainer: {
    flex: 1
  },
  loadingTitle: {
    textAlign: "center",
    marginTop: 10
  }
});
