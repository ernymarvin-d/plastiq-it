import { StyleSheet } from "react-native";
import { Metrics } from "../../themes";

export default StyleSheet.create({
  scroll_container: {
    // backgroundColor: '#a84bdd'
  },
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
