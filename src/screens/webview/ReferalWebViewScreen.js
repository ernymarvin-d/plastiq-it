import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Text,
  Alert,
  TouchableOpacity
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { withNavigation } from "react-navigation";
import { WebView } from "react-native-webview";
import MaterialIcons from "react-native-vector-icons/dist/MaterialIcons";
import NavigationService from "service/navigation";
import API_CONFIG from "../../service/api_config";

class ReferalWebViewScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: false,
      loading: true
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <Text style={{ fontSize: 10 }}>{navigation.getParam("title")}</Text>
      ),
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            NavigationService.navigate(navigation.getParam("prevScreen"));
          }}
          style={{ marginLeft: 10 }}
        >
          <MaterialIcons
            name="arrow-back"
            size={20}
            color="#000"
            style={{ textAlign: "center" }}
          />
        </TouchableOpacity>
      )
    };
  };

  async componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("ReferalWebViewScreen");

    const token = await AsyncStorage.getItem("token");

    this.setState({
      token
    });

    this.props.navigation.setParams({
      link: API_CONFIG.SERVER_REFERRAL_URL,
      prevScreen: "Refer",
      title: API_CONFIG.SERVER_REFERRAL_URL
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <WebView
          source={{
            uri: this.props.navigation.getParam("link"),
            headers: {
              Authorization: `Bearer ${this.state.token}`
            }
          }}
          onLoadEnd={() => {
            this.setState({
              loading: false
            });
          }}
          onError={() => {
            this.props.navigation.goBack();
            Alert.alert("Service Error!");
          }}
          canGoBack
          useWebKit
          javaScriptEnabled
          domStorageEnabled
          injectedJavaScript={this.state.cookie}
          startInLoadingState={false}
          cacheEnabled
        />

        {this.state.loading && (
          <View style={{ flex: 1, marginTop: -200 }}>
            <ActivityIndicator size="large" />

            <Text
              style={{
                textAlign: "center",
                marginTop: 10
              }}
            >
              Please wait!
            </Text>
          </View>
        )}
      </View>
    );
  }
}
export default withNavigation(ReferalWebViewScreen);
