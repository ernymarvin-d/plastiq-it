import React, { Component } from "react";
import { View, Alert, TouchableOpacity } from "react-native";
import LottieView from "lottie-react-native";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

import { withNavigation } from "react-navigation";
import { WebView } from "react-native-webview";

import ScreenImageHeader from "../../components/ScreenImageHeader";
import styles from "../styles/ShopWebViewScreenStyle";
import Colors from "../../constants/Colors";

class ShopWebViewScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      link: "",
      first_loading: true, // make lottie flies icon to be shown once upon initial loading.
      canGoBack: false,
      canGoForward: false
    };
  }

  componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("ShopWebViewScreen");

    const link = this.props.navigation.getParam("link");
    this.setState({ link });
  }

  handleGoBack = () => {
    this.webview.goBack();
  };

  handleGoForward = () => {
    this.webview.goForward();
  };

  render() {
    return (
      <View style={styles.container}>
        <ScreenImageHeader />
        <WebView
          ref={ref => (this.webview = ref)}
          source={{
            uri: this.state.link
          }}
          onError={syntheticEvent => {
            const { nativeEvent } = syntheticEvent;
            console.log("onError nativeEvent", nativeEvent);
            this.setState({
              first_loading: false
            });
            this.props.navigation.goBack();
            Alert.alert("Service Error!", nativeEvent.description);
          }}
          onLoadProgress={syntheticEvent => {
            // update component to be aware of loading status
            const { nativeEvent } = syntheticEvent;
            this.setState({
              canGoBack: nativeEvent.canGoBack,
              canGoForward: nativeEvent.canGoForward
            });
          }}
          onLoadEnd={syntheticEvent => {
            // const { nativeEvent } = syntheticEvent;
            this.setState({
              first_loading: false
            });
          }}
          onNavigationStateChange={navState => {
            // so loadding speed is fast, onLoadProgress may not work, so canGoBack and canGoForward can be gotten here.
            this.setState({
              canGoBack: navState.canGoBack,
              canGoForward: navState.canGoForward
            });
          }}
          javaScriptEnabled
          domStorageEnabled
          injectedJavaScript={this.state.cookie}
          cacheEnabled
        />
        {this.state.first_loading ? (
          <View style={styles.screenContent}>
            <LottieView
              source={require("@assets/animations/shopping-basket-icon.json")}
              autoPlay
              loop
              speed={1}
              style={styles.lottieLoader}
            />
          </View>
        ) : (
          <View style={styles.navigationBtnContainer}>
            <TouchableOpacity
              style={styles.navigationBtn}
              onPress={this.handleGoBack}
              disabled={!this.state.canGoBack}
            >
              <FontAwesome5Icon
                name="angle-left"
                size={30}
                color={
                  this.state.canGoBack
                    ? Colors.navigationBackForwardBtn
                    : Colors.navigationBackForwardBtnDisabled
                }
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navigationBtn}
              onPress={this.handleGoForward}
              disabled={!this.state.canGoForward}
            >
              <FontAwesome5Icon
                name="angle-right"
                size={30}
                color={
                  this.state.canGoForward
                    ? Colors.navigationBackForwardBtn
                    : Colors.navigationBackForwardBtnDisabled
                }
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

ShopWebViewScreen.navigationOptions = {
  headerStyle: {
    backgroundColor: "#a74adb",
    elevation: 0, // for android
    shadowOpacity: 0, // for ios
    borderBottomWidth: 0 // for ios
  },
  header: null
};

export default withNavigation(ShopWebViewScreen);
