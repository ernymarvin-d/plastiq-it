import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Text,
  Alert,
  TouchableOpacity
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { withNavigation } from "react-navigation";
import { WebView } from "react-native-webview";
import MaterialIcons from "react-native-vector-icons/dist/MaterialIcons";
import NavigationService from "service/navigation";
import API_CONFIG from "../../service/api_config";

class WebViewScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      link: `${api}/user/account/connect`,
      title: "Connect Your Account",
      token: false,
      loading: true
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Text>{navigation.getParam("title")}</Text>,
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            NavigationService.navigate(navigation.getParam("prevScreen"));
          }}
          style={{ marginLeft: 10 }}
        >
          <MaterialIcons
            name="arrow-back"
            size={20}
            color="#000"
            style={{ textAlign: "center" }}
          />
        </TouchableOpacity>
      )
    };
  };

  async componentDidMount() {
    // Segment Screen
    const { analytics } = global;
    analytics.screen("WebViewScreen");

    const token = await AsyncStorage.getItem("token");

    this.setState({
      token
    });

    this.props.navigation.setParams({
      link: this.state.link,
      prevScreen: "BankDetails",
      title: this.state.title
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.state.token && (
          <WebView
            source={{
              uri: this.state.link,
              headers: {
                Authorization: `Bearer ${this.state.token}`
              }
            }}
            onLoadEnd={() => {
              this.setState({
                loading: false
              });
            }}
            onError={() => {
              this.props.navigation.goBack();
              Alert.alert("Service Error!");
            }}
            canGoBack
            useWebKit
            javaScriptEnabled
            domStorageEnabled
            injectedJavaScript={this.state.cookie}
            startInLoadingState={false}
            cacheEnabled
          />
        )}

        {this.state.loading && (
          <View style={{ flex: 1, marginTop: -200 }}>
            <ActivityIndicator size="large" />
            <Text
              style={{
                textAlign: "center",
                marginTop: 10
              }}
            >
              Authenticating
            </Text>
            <Text
              style={{
                textAlign: "center",
                marginTop: 10
              }}
            >
              Please wait!
            </Text>
          </View>
        )}
      </View>
    );
  }
}
export default withNavigation(WebViewScreen);
