import ReactAppboy from "react-native-appboy-sdk";
import analytics from "@segment/analytics-react-native";
import branch, { BranchEvent } from "react-native-branch";
import Radar from "react-native-radar";
import * as Sentry from "@sentry/react-native";
import Smartlook from "smartlook-react-native-wrapper";

export const logUser = ({ id, email }) => {
  ReactAppboy.changeUser(id.toString());

  analytics.identify(id.toString(), {
    email
  });

  branch.setIdentity(id.toString());

  Radar.setUserId(id.toString());

  Sentry.setUser({ id, email });

  Smartlook.setUserIdentifier(id.toString(), { email });
};

export const logUserLoggedIn = ({
  id = 0,
  firstName,
  lastName,
  gender,
  phoneNumber,
  email
}) => {
  logUser({ id, email });
  ReactAppboy.setFirstName(firstName);
  ReactAppboy.setLastName(lastName);
  if (gender != null && gender !== undefined) {
    ReactAppboy.setGender(gender);
  }
  ReactAppboy.setPhoneNumber(phoneNumber);
  ReactAppboy.setEmail(email);
  Radar.getPermissionsStatus().then(status => {
    if (status !== "GRANTED") {
      Radar.requestPermissions(true);
    } else {
      Radar.startTracking();
    }
  });
};

export const logEvent = (event, properties) => {
  console.info(`Log event: ${event}`, properties);
  ReactAppboy.logCustomEvent(event, properties);
  analytics.track(event, properties);
};

export const logRegistrationStarted = properties =>
  logEvent("registrationStarted", properties);

export const logRegistrationDetails = properties =>
  logEvent("registrationDetails", properties);

export const logPromptToCompleteRegistration = properties =>
  logEvent("promptToCompleteRegistration", properties);

export const logRegistrationPhone = properties =>
  logEvent("registrationPhone", properties);
