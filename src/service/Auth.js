import AsyncStorage from "@react-native-community/async-storage";

export const getAuthToken = () => AsyncStorage.getItem("tokenV2");

export const setAuthTokenFromUser = async user => {
  if (user && user.passport_token) {
    await AsyncStorage.setItem("userV2", JSON.stringify(user));
    await AsyncStorage.setItem("tokenV2", `${user.passport_token}`);
    await AsyncStorage.setItem(
      "tokenExpiryV2",
      `${user.passport_token_expires_at}`
    );
  }
};
