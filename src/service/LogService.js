import axios from "axios";
import { client } from "./PlastiqClient";

if (__DEV__) {
  const logResponseAxiosInterceptor = response => {
    const { status, data } = response;
    const { url, method, data: request } = response.config;
    console.log(`${method} ${url}  ${status} ${request}`, data || "");
    return response;
  };

  const logErrorAxiosInterceptor = error => {
    const { url, method, data: request } = error.config;
    console.warn(
      `${method} ${url} ${
        error.response ? error.response.status : ""
      } ${request}`,
      error.response && error.response.data ? error.response.data : ""
    );
    return Promise.reject(error);
  };
  axios.interceptors.response.use(
    logResponseAxiosInterceptor,
    logErrorAxiosInterceptor
  );
  client.interceptors.response.use(
    logResponseAxiosInterceptor,
    logErrorAxiosInterceptor
  );
}
