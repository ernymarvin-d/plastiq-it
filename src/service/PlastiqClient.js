import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import { getAuthToken, setAuthTokenFromUser } from '@service/Auth';
import NavigationService from './navigation';

const baseURL = __DEV__
    ? 'https://api-dev.plastiq.it/api/v2/'
    : 'https://api.plastiq.it/api/v2/';

export const client = axios.create({
    baseURL,
    withCredentials: true,
});

export const STATUS = {
    success: 'success',
    error: 'error',
};

const setUserTokenMiddleware = async (response) => {
    const { data } = response;
    await setAuthTokenFromUser(data.user);
    return response;
};

const setCookieMiddleware = async (response) => {
    if (Array.isArray(response.headers['set-cookie'])) {
        await Promise.all(
            response.headers['set-cookie'].map(async (cookie) => {
                if (
                    typeof cookie === 'string' &&
                    cookie.startsWith('refresh_token')
                ) {
                    // eslint-disable-next-line no-await-in-loop
                    await AsyncStorage.setItem('refreshTokenV2', cookie);
                }
            })
        );
    }
};

const refreshTokenMiddleware = (failedRequest) =>
    // eslint-disable-next-line no-use-before-define
    tokenRefresh().then(async (response) => {
        if (
            response &&
            response.data &&
            response.data.code === 'UNAUTHORIZED'
        ) {
            return Promise.reject();
        }
        await setUserTokenMiddleware(response);
        await setCookieMiddleware(response);
        failedRequest.response.config.headers.Authorization = `Bearer ${response.data.user.passport_token}`;
        return Promise.resolve();
    });

client.interceptors.request.use(async (config) => {
    const token = await getAuthToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    const refreshToken = await AsyncStorage.getItem('refreshTokenV2');
    if (refreshToken) {
        config.headers.Cookie = refreshToken;
    }
    config.headers.common.Accept = 'application/json';
    return config;
});

client.interceptors.response.use(
    async (response) => {
        await setCookieMiddleware(response);
        return response;
    },
    (error) => {
        // Only handle un-authorized request from refresh token
        if (
            error &&
            error.response &&
            error.response.data &&
            error.response.data.code === 'NOT_REFRESHED'
        ) {
            Alert.alert(
                'Your login has expired',
                `Please re-enter your login details to continue`,
                [
                    {
                        text: 'OK',
                        onPress: () =>
                            NavigationService.navigate('SignInPasswordScreen'),
                    },
                ]
            );
        }
        return Promise.reject(error);
    }
);

createAuthRefreshInterceptor(client, refreshTokenMiddleware);

export const checkEmail = (email) =>
    client.post(`check-email`, {
        email,
    });

export const register = ({
    email,
    firstName,
    lastName,
    password,
    passwordConfirm,
    gender,
    ageBracket,
    countryCode,
    phoneNumber,
}) =>
    client
        .post(`register`, {
            email,
            first_name: firstName,
            last_name: lastName,
            password,
            password_confirmation: passwordConfirm,
            gender,
            age_bracket: ageBracket,
            phone_country_code: countryCode,
            phone_number: phoneNumber,
        })
        .then(setUserTokenMiddleware);

export const login = (email, password) =>
    client
        .post(`login`, {
            email,
            password,
        })
        .then(setUserTokenMiddleware);

export const forgotPassword = (email) =>
    client.post(`forgot-password`, { email });

export const checkCode = (code) =>
    client
        .post('check-code', {
            code,
        })
        .then(setUserTokenMiddleware);

export const sendCode = () => client.get('send-code');

export const getUserCashback = () => client.get('user/cashback');

export const passcodeCheck = (passcode) =>
    client.post('passcode/check', {
        passcode,
    });

export const passcodeUpdate = (passcode) =>
    client.post('passcode/update', {
        passcode,
    });

export const tokenRefresh = () => client.post('token/refresh');

export const getBankAccountConnectWebviewLink = () =>
    client.get(`user/account/connect`);

export const getBankAccounts = () => client.get('user/account');
