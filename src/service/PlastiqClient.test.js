import { checkEmail, login, tokenRefresh } from "./PlastiqClient";
// import "./LogService";

const email = "sven2@plastiq.it";
const password = "12345678";

const loginHelper = login.bind(null, email, password);

// This is for manual testing. For now don't run this in CI.
describe("PlastiqClient", () => {
  describe("auth", () => {
    it("check-email", async () => {
      const response = await checkEmail(email);
      expect(response.status).toEqual(200);
    });

    // Register without creating an account.
    it.todo("register");

    it("login", async () => {
      const response = await loginHelper();
      expect(response.status).toEqual(200);
    });
  });

  describe("authenticated", () => {
    beforeEach(loginHelper);
    it("refresh tokens", async () => {
      const response = await tokenRefresh();
      expect(response.status).toEqual(200);
    });
    describe("change-password", () => {});
    describe("user", () => {
      describe("GET user/", () => {});
      describe("GET user/profile", () => {});
      describe("user/profile/avatar", () => {});
      describe("PUT user/profile", () => {});
    });
    describe("cashback/credit", () => {});
    describe("user/withdrawal", () => {});
    describe("offers/merchants", () => {});
    describe("offers/merchants/%source/%merchantID", () => {});
    describe("passcode", () => {
      describe("passcode/check", () => {});
      describe("passcode/update", () => {});
      describe("passcode/verify-user", () => {});
    });

    describe("referral", () => {});
  });

  describe("validate", () => {
    describe("validate/email", () => {});
    describe("verify/phone-number", () => {});
    describe("verify/phone-number/resend-code", () => {});
  });

  // it("user/cashback", async () => {
  //   expect(await getCashback()).toBeDefined();
  // });
});
