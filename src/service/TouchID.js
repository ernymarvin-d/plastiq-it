import TouchID from "react-native-touch-id";

export const options = {
  title: "Authentication Required", // Android	    title: "Authentication Required", // Android
  imageColor: "#e00606", // Android	    imageColor: "#e00606", // Android
  imageErrorColor: "#ff0000", // Android	    imageErrorColor: "#ff0000", // Android
  sensorDescription: "Touch sensor", // Android	    sensorDescription: "Touch sensor", // Android
  sensorErrorDescription: "Failed", // Android	    sensorErrorDescription: "Failed", // Android
  cancelText: "Cancel", // Android	    cancelText: "Cancel", // Android
  fallbackLabel: "Show Passcode", // iOS (if empty, then label is hidden)	    fallbackLabel: "Show Passcode", // iOS (if empty, then label is hidden)
  unifiedErrors: false, // use unified error messages (default false)	    unifiedErrors: false, // use unified error messages (default false)
  passcodeFallback: false // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.	    passcodeFallback: false // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export const authenticate = (
  title = "Confirm your fingerprint to authenticate"
) => TouchID.authenticate(title, options);
