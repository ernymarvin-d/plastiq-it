import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";
import API_CONFIG from "./api_config";

const api = axios.create();

const BASE_URL = `${API_CONFIG.SERVER_URL}/api/v1`;
console.log({ BASE_URL });

api.defaults.baseURL = BASE_URL;

api.interceptors.request.use(async function(config) {
  const token = await AsyncStorage.getItem("token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  config.headers.common.Accept = "application/json";

  return config;
});

export default api;
