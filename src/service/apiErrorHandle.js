import NavigationService from "service/navigation";
import AsyncStorage from "@react-native-community/async-storage";
import { REQUEST_VALIDATE_ERRORS } from "../store/auth/actionTypes";

export default (error = {}) => {
  const response = error.response ? error.response : {};

  if (error.toString() == "Error: Network Error") {
    console.log("Network error!");

    return dispatch => {
      return false;
    };
  }

  return (dispatch, getState) => {
    switch (response.status) {
      case 401:
        const { message } = response.data;
        dispatch({
          type: REQUEST_VALIDATE_ERRORS,
          errors: {
            unauthenticated: [
              `${message} Invalid or expired token. Please sign in again.`
            ]
          }
        });
        NavigationService.navigate("SignInEmail");
        AsyncStorage.clear();
        break;
      case 403:
        NavigationService.navigate("Started");
        AsyncStorage.clear();
        break;
      case 422:
        const { errors } = response.data;
        dispatch({
          type: REQUEST_VALIDATE_ERRORS,
          errors
        });
        const { auth } = getState();
        const { is_registration_completed, verified } = auth.user_detail;

        /**
         * is_registration_completed means user registration isn't yet finished.
         * verified = undefined or null: means that user is new subscirber.
         * verified = 0: user that wasn't verified yet.
         * Only when user is unverified, it navigates to "SignUp".
         */
        if (
          !is_registration_completed &&
          (verified == undefined || verified == null || verified == 0) &&
          ((errors.name && errors.name.length > 0) ||
            (errors.password && errors.password.length > 0))
        ) {
          NavigationService.navigate("SignUp");
        }
        break;
      default:
        console.log("axios-error");
        return false;
    }
  };
};
