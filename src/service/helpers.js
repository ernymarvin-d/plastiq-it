/* convert object to array */
export const objectToArray = object => {
  if (typeof object === "undefined") {
    return [];
  }

  return Object.keys(object).map(function(key) {
    return object[key];
  });
};
