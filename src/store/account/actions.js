import api from "service/api";
import apiErrorHandle from "service/apiErrorHandle";
import {
  SET_BANK_ACCOUNTS,
  SET_BANK_ACCOUNTS_LOADING,
  SET_BANK_ACCOUNTS_DELETING,
  SET_NEW_BANK_ACCOUNT_CONNECTING
} from "./actionTypes";

export const setBankAccountsLoading = loading => {
  return {
    type: SET_BANK_ACCOUNTS_LOADING,
    loading
  };
};

export const setBankAccountsDeleting = deleting => {
  return {
    type: SET_BANK_ACCOUNTS_DELETING,
    deleting
  };
};

export const setBankAccounts = data => {
  return {
    type: SET_BANK_ACCOUNTS,
    data
  };
};

export const getBankAccounts = () => {
  return (dispatch, getState) => {
    dispatch(setBankAccountsLoading(true));

    api
      .get("user/account")
      .then(response => {
        dispatch(setBankAccountsLoading(false));

        const { bank_accounts } = getState();
        initial_num_of_bank_accounts = bank_accounts.bank_accounts.length;
        new_bank_account_connecting = bank_accounts.new_bank_account_connecting;

        response = response.data;
        bank_accounts_from_response = response.bank_accounts;
        bank_accounts_from_response = bank_accounts_from_response.length
          ? bank_accounts_from_response
          : [];

        /**
         * when connecting new bank account,
         * if the user has empty bank before connecting and has one more banks after connecting,
         * this is first bank connecting
         */
        if (
          new_bank_account_connecting &&
          initial_num_of_bank_accounts == 0 &&
          bank_accounts_from_response.length >= 1
        ) {
          // Braze custom event for registration bank
          const { ReactAppboy } = global;
          const properties = {};
          properties.bank = "first";
          ReactAppboy.logCustomEvent("registrationBank", properties);

          // Segment Track
          const { analytics } = global;
          analytics.track("registrationBank", properties);

          // Branch standar event
          // const BranchEvent = global.BranchEvent;
          // let branch_event = new BranchEvent(BranchEvent.UnlockAchievement);
          // // branch_event.bank = "first";
          // branch_event.logEvent();
        }

        // reset the connecting state to false
        if (new_bank_account_connecting) {
          dispatch(setNewBankAccountConnecting(false));
        }

        const results = classifyBankAccounts(bank_accounts_from_response);
        dispatch(setBankAccounts(results));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setBankAccountsLoading(false));
        return error.response;
      });
  };
};

const classifyBankAccounts = total_bank_accounts => {
  const checking_banks = getCheckingsBankAccounts(total_bank_accounts);
  const savings_banks = getSavingsBankAccounts(total_bank_accounts);
  const credits_banks = getCreditsBankAccounts(total_bank_accounts);
  const other_banks = getOtherBankAccounts(total_bank_accounts);
  const results = [checking_banks, savings_banks, credits_banks, other_banks];
  return results;
};

const getCheckingsBankAccounts = total_bank_accounts => {
  const data = total_bank_accounts.filter(bank_account => {
    return bank_account.account_type == "CHECKING";
  });

  const result = {
    title: "Bank",
    account_type: "CHECKING",
    data
  };
  return result;
};

const getSavingsBankAccounts = total_bank_accounts => {
  const data = total_bank_accounts.filter(bank_account => {
    return bank_account.account_type == "SAVINGS";
  });

  const result = {
    title: "Savings",
    account_type: "SAVINGS",
    data
  };
  return result;
};

const getCreditsBankAccounts = total_bank_accounts => {
  const data = total_bank_accounts.filter(bank_account => {
    return bank_account.account_type == "CREDIT";
  });

  const result = {
    title: "Credit Card",
    account_type: "CREDIT",
    data
  };
  return result;
};

const getOtherBankAccounts = total_bank_accounts => {
  const data = total_bank_accounts.filter(bank_account => {
    return (
      bank_account.account_type != "CHECKING" &&
      bank_account.account_type != "SAVINGS" &&
      bank_account.account_type != "CREDIT"
    );
  });

  const result = {
    title: "Others",
    account_type: "ETC",
    data
  };
  return result;
};

export const deleteBankAccount = (account_id, callback) => {
  return dispatch => {
    dispatch(setBankAccountsLoading(true));

    api
      .delete(`user/account/${account_id}`)
      .then(response => {
        console.log("/deleteBankAccount response", response);

        // Braze custom event for registration bank
        const { ReactAppboy } = global;
        const properties = {};
        properties.account_id = account_id;
        ReactAppboy.logCustomEvent("BankRemoved", properties);

        // Segment Track
        const { analytics } = global;
        analytics.track("BankRemoved", properties);

        dispatch(callback);
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setBankAccountsLoading(false));
        return error.response;
      });
  };
};

export const setNewBankAccountConnecting = connecting => {
  return {
    type: SET_NEW_BANK_ACCOUNT_CONNECTING,
    connecting
  };
};
