import {
  SET_BANK_ACCOUNTS,
  SET_BANK_ACCOUNTS_LOADING,
  SET_BANK_ACCOUNTS_DELETING,
  SET_NEW_BANK_ACCOUNT_CONNECTING
} from "./actionTypes";

const initial_state = {
  bank_accounts: [],
  bank_accounts_loading: false,
  bank_accounts_deleting: false,
  new_bank_account_connecting: true
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case SET_BANK_ACCOUNTS:
      return {
        ...state,
        bank_accounts: action.data
      };
    case SET_BANK_ACCOUNTS_LOADING:
      return {
        ...state,
        bank_accounts_loading: action.loading
      };
    case SET_BANK_ACCOUNTS_DELETING:
      return {
        ...state,
        bank_accounts_deleting: action.deleting
      };
    case SET_NEW_BANK_ACCOUNT_CONNECTING:
      return {
        ...state,
        new_bank_account_connecting: action.connecting
      };
    default:
      return state;
  }
};

export default reducer;
