import { Alert } from "react-native";

import * as Client from "@service/PlastiqClient";
import {
  logUser,
  logUserLoggedIn,
  logPromptToCompleteRegistration
} from "@service/Analytics";
import NavigationService from "@service/navigation";
import { getUser } from "./selectors";
import {
  SET_AUTH_LOADING,
  SET_VALIDATION_ERRORS,
  SET_USER
} from "./actionTypes";

const { STATUS } = Client;

export const setAuthLoading = data => ({
  type: SET_AUTH_LOADING,
  data
});

export const setValidationErrors = data => ({
  type: SET_VALIDATION_ERRORS,
  data
});

export const setUser = data => ({
  type: SET_USER,
  data
});

export const checkEmail = email => dispatch => {
  dispatch(setAuthLoading(true));
  dispatch(setValidationErrors());
  return Client.checkEmail(email)
    .then(({ data }) => {
      const { status, code, user = {}, errors = {} } = data;
      switch (status) {
        case STATUS.success:
          dispatch(setUser(user));
          logUser(user);
          switch (code) {
            case "EXISTS_REGISTERED":
              NavigationService.navigate("SignInPasswordScreen");
              break;
            case "EXISTS_NOT_REGISTERED":
              logPromptToCompleteRegistration({
                email
              });
              NavigationService.navigate("SignUpScreen", {
                email
              });
              break;
            default:
              console.warn(`Unexpected code for checkEmail ${status}`);
          }
          break;
        case STATUS.error:
          dispatch(setValidationErrors(errors));
          break;
        default:
          console.warn(`Unexpected status for check-email ${status}`);
      }
    })
    .finally(() => {
      dispatch(setAuthLoading(false));
    });
};

export const register = user => (dispatch, getState) => {
  dispatch(setAuthLoading(true));
  dispatch(setValidationErrors());
  return Client.register(user)
    .then(({ data }) => {
      const { status, user = {}, errors = {} } = data;
      switch (status) {
        case STATUS.success:
          dispatch(setUser(user));
          logUserLoggedIn(getUser(getState()));
          NavigationService.navigate("VerifyPhoneNumberScreen");
          break;
        case STATUS.error:
          dispatch(setValidationErrors(errors));
          break;
        default:
          console.warn(`Unexpected status for register ${status}`);
      }
    })
    .finally(() => {
      dispatch(setAuthLoading(false));
    });
};

export const verifyPhoneNumber = code => dispatch => {
  dispatch(setAuthLoading(true));
  dispatch(setValidationErrors());
  return Client.checkCode(code)
    .then(response => {
      const { data } = response;
      const { status } = data;
      switch (status) {
        case STATUS.success:
          NavigationService.navigate("NewPasscodeScreen");
          break;
        default:
          console.warn(`Unexpected status for verifyPhoneNumber ${status}`);
      }
      return response;
    })
    .finally(() => dispatch(setAuthLoading(false)));
};

export const verifyPhoneNumberResendCode = () => _ => Client.sendCode();

export const login = ({ email, password }) => (dispatch, getState) => {
  dispatch(setAuthLoading(true));
  dispatch(setValidationErrors());
  return Client.login(email, password)
    .then(response => {
      const { data } = response;
      const { status, user } = data;
      switch (status) {
        case STATUS.success:
          dispatch(setUser(user));
          logUserLoggedIn(getUser(getState()));
          if (user.verified) {
            NavigationService.navigate(
              user.passcode ? "PasscodeScreen" : "NewPasscodeScreen"
            );
          } else {
            Client.sendCode().then(() => {
              NavigationService.navigate("VerifyPhoneNumberScreen");
            });
          }
          break;
        case STATUS.error:
          dispatch(
            setValidationErrors({
              password: ["Invalid username or password, please try again."]
            })
          );
          break;
        default:
          console.warn(`Unexpected status for login ${status}`);
      }
      return response;
    })
    .finally(() => dispatch(setAuthLoading(false)));
};

export const forgotPassword = email => dispatch => {
  dispatch(setAuthLoading(true));
  dispatch(setValidationErrors());
  return Client.forgotPassword(email)
    .then(response => {
      const { data } = response;
      const { status, code } = data;
      switch (status) {
        case STATUS.success:
          Alert.alert(
            "Reset password email sent",
            `An email was sent to ${email} with instructions to reset your email`,
            [
              {
                text: "OK",
                onPress: () => NavigationService.navigate("SignInEmailScreen")
              }
            ]
          );
          break;
        case STATUS.error:
          switch (code) {
            case "USER_DOES_NOT_EXIST":
              dispatch(
                setValidationErrors({
                  email: ["The email was invalid or does not exist"]
                })
              );
              break;
            default:
              dispatch(
                setValidationErrors({
                  email: ["There was an error sending a "]
                })
              );
          }
          break;
        default:
          console.warn(`Unexpected status for forgotPassword ${status}`);
      }

      return response;
    })
    .finally(() => dispatch(setAuthLoading(false)));
};
