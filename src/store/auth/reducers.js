import {
  SET_AUTH_LOADING,
  SET_VALIDATION_ERRORS,
  SET_USER
} from "./actionTypes";

const initialState = {
  authLoading: false,
  changePasswordLoading: false,
  token: {},
  user: {
    id: 0,
    firstName: "",
    lastName: "",
    email: "",
    gender: "",
    ageBracket: "",
    countryCode: "",
    phoneNumber: "",
    verified: false,
    registered: false,
    passcode: "",
    dateOfBirth: "",
    postcode: 0,
    avatarURL: ""
  },
  validationErrors: {
    email: [],
    first_name: [],
    last_name: [],
    password: [],
    password_confirmation: [],
    age_bracket: [],
    phone_country_code: [],
    phone_number: []
  }
};

export default function reducer(state = initialState, { type, data }) {
  switch (type) {
    case SET_AUTH_LOADING:
      return {
        ...state,
        authLoading: data
      };
    case SET_VALIDATION_ERRORS:
      return {
        ...state,
        validationErrors: data || {}
      };
    case SET_USER:
      // eslint-disable-next-line no-case-declarations
      const {
        first_name: firstName,
        last_name: lastName,
        age_bracket: ageBracket,
        phone_country_code: countryCode,
        phone_number: phoneNumber,
        date_of_birth: dateOfBirth,
        avatar_url: avatarURL,
        ...properties
      } = data || {};
      return {
        ...state,
        user: {
          firstName,
          lastName,
          ageBracket,
          countryCode,
          phoneNumber,
          dateOfBirth,
          avatarURL,
          ...properties
        }
      };
    default:
      return state;
  }
}
