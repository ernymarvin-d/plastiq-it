export const getAuth = state => state.auth;

export const getAuthLoading = state => getAuth(state).authLoading;

export const getValidationErrors = state => getAuth(state).validationErrors;

export const getUser = state => getAuth(state).user;
