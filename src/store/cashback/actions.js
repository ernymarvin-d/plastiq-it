import { Alert } from "react-native";

import api from "service/api";
import apiErrorHandle from "service/apiErrorHandle";
import {
  SET_CASHBACKS,
  SET_CASHBACKS_LOADING,
  SET_CASHBACK_CREDIT,
  SET_CASHBACK_CREDIT_LOADING,
  SET_HOME_BANNER_CARDS,
  SET_CONNECT_BANK_ACCOUNT_BANNER_CARDS,
  SET_CAROUSEL_CARDS,
  SET_STATIC_CARDS,
  SET_ACCOUNT_STATIC_CARDS,
  SET_REFER_A_FRIEND_CARDS,
  SET_CONTENT_CARDS_LOADING,
  REQUEST_CASHBACK_ERRORS
} from "./actionTypes";

export const setCashbacksLoading = loading => {
  return {
    type: SET_CASHBACKS_LOADING,
    loading
  };
};

export const setCashbackCreditLoading = loading => {
  return {
    type: SET_CASHBACK_CREDIT_LOADING,
    loading
  };
};

export const setCashbacks = data => {
  return {
    type: SET_CASHBACKS,
    data
  };
};

export const setCashbackCredit = data => {
  return {
    type: SET_CASHBACK_CREDIT,
    data
  };
};

export const getCashbacks = () => {
  return (dispatch, getState) => {
    dispatch(setCashbacksLoading(true));
    const { auth } = getState();
    const { id } = auth.user_detail;

    api
      .get("cashback", {
        params: {
          // user_id: 275,
          user_id: id
        }
      })
      .then(response => {
        response = response.data;
        cashbacks = response.cashbacks;

        addRefinedTransactionDate(cashbacks.data);

        dispatch(setCashbacks(cashbacks));

        setTimeout(() => {
          dispatch(setCashbacksLoading(false));
        }, 800);
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setCashbacksLoading(false));
        return error.response;
      });
  };
};

export const loadMoreCashbacks = params => {
  return (dispatch, getState) => {
    const { auth, cashback } = getState();
    const { id } = auth.user_detail;

    api
      .get("cashback", {
        params: {
          // user_id: 275,
          user_id: id,
          page: params.page
        }
      })
      .then(response => {
        response = response.data;
        cashbacks_from_response = response.cashbacks;

        addRefinedTransactionDate(cashbacks_from_response.data);

        previous_cashbacks_list = cashback.cashbacks.data;
        cashbacks_from_response.data = previous_cashbacks_list.concat(
          cashbacks_from_response.data
        );

        dispatch(setCashbacks(cashbacks_from_response));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        return error.response;
      });
  };
};

const addRefinedTransactionDate = cashbacks => {
  cashbacks.forEach(element => {
    element.refined_transaction_date = processTransactionDate(
      element.transaction_date
    );
  });
};

const processTransactionDate = date => {
  const only_date = date.split(" ")[0];
  const temp = only_date.split("-");
  const yyyy = temp[0];
  const mm = temp[1];
  const dd = temp[2];
  return `${dd}-${mm}-${yyyy}`;
};

export const getCashbackCredit = () => {
  return dispatch => {
    dispatch(setCashbackCreditLoading(true));

    api
      .get("cashback/credit")
      .then(response => {
        dispatch(setCashbackCreditLoading(false));

        response = response.data;
        dispatch(setCashbackCredit(response));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setCashbackCreditLoading(false));
        return error.response;
      });
  };
};

export const setContentCardsLoading = loading => {
  return {
    type: SET_CONTENT_CARDS_LOADING,
    loading
  };
};

export const setContentCards = (type, cards) => {
  return { type, cards };
};

export const getContentCards = () => {
  return async dispatch => {
    dispatch(setContentCardsLoading(true));

    const { ReactAppboy } = global;
    // ReactAppboy.getInstallTrackingId((err, response) => {
    //   if (err) {
    //     console.log('/getContentCards error ', err);
    //   } else {
    //     console.log('/getContentCards device id ', response);
    //   }
    // });

    // ReactAppboy.requestContentCardsRefresh();

    try {
      const cards = await ReactAppboy.getContentCards();

      const {
        home_banner_cards,
        carousel_cards,
        static_cards,

        refer_a_friend_cards,

        connect_bank_account_banner_cards,
        account_static_cards
      } = classifyCards(cards);

      dispatch(setContentCards(SET_HOME_BANNER_CARDS, home_banner_cards));
      dispatch(
        setContentCards(
          SET_CONNECT_BANK_ACCOUNT_BANNER_CARDS,
          connect_bank_account_banner_cards
        )
      );
      dispatch(setContentCards(SET_CAROUSEL_CARDS, carousel_cards));
      dispatch(setContentCards(SET_STATIC_CARDS, static_cards));
      dispatch(setContentCards(SET_REFER_A_FRIEND_CARDS, refer_a_friend_cards));
      dispatch(setContentCards(SET_ACCOUNT_STATIC_CARDS, account_static_cards));

      dispatch(setContentCardsLoading(false));
    } catch (e) {
      console.error(e);
      dispatch(setContentCardsLoading(false));
    }
  };
};

export const refreshCards = callback => {
  return async dispatch => {
    dispatch(setContentCardsLoading(true));

    const { ReactAppboy } = global;

    ReactAppboy.requestContentCardsRefresh();

    try {
      const cards = await ReactAppboy.getContentCards();

      const {
        home_banner_cards,
        carousel_cards,
        static_cards,

        refer_a_friend_cards,

        connect_bank_account_banner_cards,
        account_static_cards
      } = classifyCards(cards);

      dispatch(setContentCards(SET_HOME_BANNER_CARDS, home_banner_cards));
      dispatch(
        setContentCards(
          SET_CONNECT_BANK_ACCOUNT_BANNER_CARDS,
          connect_bank_account_banner_cards
        )
      );
      dispatch(setContentCards(SET_CAROUSEL_CARDS, carousel_cards));
      dispatch(setContentCards(SET_STATIC_CARDS, static_cards));
      dispatch(setContentCards(SET_REFER_A_FRIEND_CARDS, refer_a_friend_cards));
      dispatch(setContentCards(SET_ACCOUNT_STATIC_CARDS, account_static_cards));

      dispatch(setContentCardsLoading(false));
      dispatch(callback);
    } catch (e) {
      console.log("/refreshCards e =>", e);
      dispatch(setContentCardsLoading(false));
      dispatch(callback);
    }
  };
};

const classifyCards = cards => {
  const banner_cards = cards.filter(card => card.type == "Banner");
  const home_banner_cards = getHomeBannerCards(banner_cards);
  const connect_bank_account_banner_cards = getConnectBankAccountBannerCards(
    banner_cards
  );

  const captioned_content_cards = cards.filter(
    card => card.type == "Captioned"
  );
  const carousel_cards = getCarouselCards(captioned_content_cards);
  const static_cards = getStaticCards(captioned_content_cards);
  const refer_a_friend_cards = getReferAFriend(captioned_content_cards);
  const account_static_cards = getAccountStaticCards(captioned_content_cards);

  return {
    home_banner_cards,
    connect_bank_account_banner_cards,
    carousel_cards,
    static_cards,
    refer_a_friend_cards,
    account_static_cards
  };
};

const getHomeBannerCards = banner_cards => {
  const temp = banner_cards.filter(card => {
    return card.extras.kind != undefined && card.extras.kind == "home_banner";
  });

  return temp;
};

const getProfileBannerCards = banner_cards => {
  const temp = banner_cards.filter(card => {
    return (
      card.extras.kind != undefined &&
      (card.extras.kind == "btn_account" ||
        card.extras.kind == "btn_bank_details" ||
        card.extras.kind == "btn_cashbacks" ||
        card.extras.kind == "btn_withdraw" ||
        card.extras.kind == "btn_support")
    );
  });

  const results = temp.sort(
    (a, b) =>
      (a.extras.index != undefined || b.extras.index != undefined) &&
      a.extras.index - b.extras.index
  );
  return results;
};

const getConnectBankAccountBannerCards = banner_cards => {
  const temp = banner_cards.filter(card => {
    return (
      card.extras.kind != undefined &&
      card.extras.kind == "connect_bank_account"
    );
  });

  return temp;
};

const getCarouselCards = captioned_content_cards => {
  const temp = captioned_content_cards.filter(card => {
    return card.extras.kind != undefined && card.extras.kind == "carousel";
  });

  const carousel_cards = temp.sort((a, b) => a.extras.index - b.extras.index);
  return carousel_cards;
};

const getStaticCards = captioned_content_cards => {
  const static_cards = [];
  let temp = captioned_content_cards.filter(card => {
    return (
      card.extras.kind != undefined &&
      card.extras.kind == "static" &&
      card.extras.index == 1
    );
  });
  if (temp[0] != undefined) {
    static_cards.push(temp[0]);
  }

  temp = captioned_content_cards.filter(card => {
    return (
      card.extras.kind != undefined &&
      card.extras.kind == "static" &&
      card.extras.index == 2
    );
  });
  if (temp[0] != undefined) {
    static_cards.push(temp[0]);
  }

  temp = captioned_content_cards.filter(card => {
    return (
      card.extras.kind != undefined &&
      card.extras.kind == "static" &&
      card.extras.index == 3
    );
  });
  if (temp[0] != undefined) {
    static_cards.push(temp[0]);
  }

  return static_cards;
};

const getReferAFriend = captioned_content_cards => {
  const temp = captioned_content_cards.filter(card => {
    return (
      card.extras.kind != undefined && card.extras.kind == "refer_a_friend"
    );
  });

  return temp;
};

const getAccountStaticCards = captioned_content_cards => {
  const temp = captioned_content_cards.filter(card => {
    return (
      card.extras.kind != undefined && card.extras.kind == "account_static"
    );
  });

  const entries = temp.sort((a, b) => a.extras.index - b.extras.index);
  return entries;
};

export const withdrawCashback = (data, callback) => {
  return dispatch => {
    dispatch({
      type: REQUEST_CASHBACK_ERRORS,
      errors: {}
    });
    dispatch(setCashbacksLoading(true));

    api
      .post("user/withdrawal", data)
      .then(response => {
        response = response.data;
        Alert.alert(response.message);

        // Braze custom event for registration details
        const { ReactAppboy } = global;
        ReactAppboy.logCustomEvent("Withdrawal", data);

        // Segment Track
        const { analytics } = global;
        analytics.track("Withdrawal", data);
        setTimeout(() => {
          dispatch(setCashbacksLoading(false));
          dispatch(callback);
        }, 800);
      })
      .catch(error => {
        const { errors } = error.response.data;
        console.log("/withdrawCashback error", errors);
        dispatch({
          type: REQUEST_CASHBACK_ERRORS,
          errors
        });
        dispatch(setCashbacksLoading(false));
        return error.response;
      });
  };
};
