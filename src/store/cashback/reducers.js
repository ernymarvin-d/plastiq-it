import {
  SET_CASHBACKS,
  SET_CASHBACKS_LOADING,
  SET_CASHBACK_CREDIT,
  SET_CASHBACK_CREDIT_LOADING,
  SET_HOME_BANNER_CARDS,
  SET_CONNECT_BANK_ACCOUNT_BANNER_CARDS,
  SET_PROFILE_BANNER_CARDS,
  SET_CAROUSEL_CARDS,
  SET_STATIC_CARDS,
  SET_ACCOUNT_STATIC_CARDS,
  SET_REFER_A_FRIEND_CARDS,
  SET_CONTENT_CARDS_LOADING,
  REQUEST_CASHBACK_ERRORS
} from "./actionTypes";

const initial_state = {
  cashbacks: {
    data: []
  },
  cashbacks_loading: false,
  cashback_credit: 0,
  cashback_credit_loading: false,

  home_banner_cards: [],
  connect_bank_account_banner_cards: [],
  profile_banner_cards: [],
  carousel_cards: [],
  static_cards: [],
  account_static_cards: [],
  refer_a_friend_cards: [],
  classic_content_cards: [],
  content_cards_loading: false,
  request_cashback_errors: {}
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case SET_CASHBACKS:
      return {
        ...state,
        cashbacks: action.data
      };
    case SET_CASHBACKS_LOADING:
      return {
        ...state,
        cashbacks_loading: action.loading
      };
    case SET_CASHBACK_CREDIT:
      return {
        ...state,
        cashback_credit: action.data
      };
    case SET_CASHBACK_CREDIT_LOADING:
      return {
        ...state,
        cashback_credit_loading: action.loading
      };
    case SET_HOME_BANNER_CARDS:
      return {
        ...state,
        home_banner_cards: action.cards
      };
    case SET_CONNECT_BANK_ACCOUNT_BANNER_CARDS:
      return {
        ...state,
        connect_bank_account_banner_cards: action.cards
      };
    case SET_PROFILE_BANNER_CARDS:
      return {
        ...state,
        profile_banner_cards: action.cards
      };
    case SET_CAROUSEL_CARDS:
      return {
        ...state,
        carousel_cards: action.cards
      };
    case SET_STATIC_CARDS:
      return {
        ...state,
        static_cards: action.cards
      };
    case SET_REFER_A_FRIEND_CARDS:
      return {
        ...state,
        refer_a_friend_cards: action.cards
      };
    case SET_ACCOUNT_STATIC_CARDS:
      return {
        ...state,
        account_static_cards: action.cards
      };
    case SET_CONTENT_CARDS_LOADING:
      return {
        ...state,
        content_cards_loading: action.loading
      };
    case REQUEST_CASHBACK_ERRORS:
      return {
        ...state,
        request_cashback_errors: action.errors
      };
    default:
      return state;
  }
};

export default reducer;
