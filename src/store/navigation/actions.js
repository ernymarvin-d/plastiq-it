import { SET_CURRENT_ROUTE } from "./actionTypes";

export const setCurrentRoute = route => {
  return {
    type: SET_CURRENT_ROUTE,
    route
  };
};
