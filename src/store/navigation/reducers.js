import { SET_CURRENT_ROUTE } from "./actionTypes";

const initial_state = {
  current_route: "home"
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case SET_CURRENT_ROUTE:
      return {
        ...state,
        current_route: action.route
      };
    default:
      return state;
  }
};

export default reducer;
