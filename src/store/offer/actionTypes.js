export const SET_OFFERS = "SET_OFFERS";
export const SET_OFFERS_LOADING = "SET_OFFERS_LOADING";
export const SET_MORE_OFFERS_LOADING = "SET_MORE_OFFERS_LOADING";
export const SET_SEARCH_KEY = "SET_SEARCH_KEY";
export const SET_MERCHANT_DETAILS = "SET_MERCHANT_DETAILS";
export const SET_MERCHANT_LOADING = "SET_MERCHANT_LOADING";
