import api from "service/api";
import apiErrorHandle from "service/apiErrorHandle";
import {
  SET_OFFERS,
  SET_OFFERS_LOADING,
  SET_MORE_OFFERS_LOADING,
  SET_SEARCH_KEY,
  SET_MERCHANT_DETAILS,
  SET_MERCHANT_LOADING
} from "./actionTypes";

export const setOffersLoading = loading => {
  return {
    type: SET_OFFERS_LOADING,
    loading
  };
};

export const setMoreOffersLoading = loading => {
  return {
    type: SET_MORE_OFFERS_LOADING,
    loading
  };
};

export const setOffers = data => {
  return {
    type: SET_OFFERS,
    data
  };
};

export const setMerchantDetails = data => {
  return {
    type: SET_MERCHANT_DETAILS,
    data
  };
};

export const setMerchantLoading = loading => {
  return {
    type: SET_MERCHANT_LOADING,
    loading
  };
};

export const getOffers = params => {
  return dispatch => {
    dispatch(setOffersLoading(true));

    api
      .get("offers/merchants", {
        params
      })
      .then(response => {
        // console.log("/getOffers response", response);
        dispatch(setOffersLoading(false));
        data = response.data;

        merchants = data.merchants;

        offers_list = merchants.data;
        current_page = merchants.current_page;
        last_page = merchants.last_page;

        dispatch(setOffers({ offers_list, current_page, last_page }));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setOffersLoading(false));
        return error.response;
      });
  };
};

export const loadMoreOffers = params => {
  return (dispatch, getState) => {
    dispatch(setMoreOffersLoading(true));

    api
      .get("offers/merchants", {
        params
      })
      .then(response => {
        data = response.data;
        merchants = data.merchants;
        new_offers_list = merchants.data;

        const { offer } = getState();
        previous_offers_list = offer.offers_list;

        offers_list = previous_offers_list.concat(new_offers_list);
        current_page = merchants.current_page;
        last_page = merchants.last_page;

        dispatch(setOffers({ offers_list, current_page, last_page }));
        dispatch(setMoreOffersLoading(false));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setMoreOffersLoading(false));
        return error.response;
      });
  };
};

export const setSearchKey = search_key => {
  return dispatch => {
    dispatch({
      type: SET_SEARCH_KEY,
      search_key
    });
  };
};

export const getMerchantDetails = detail_info => {
  return dispatch => {
    dispatch(setMerchantLoading(true));

    const { source, merchant_id } = detail_info;

    api
      .get(`offers/merchants/${source}/${merchant_id}`)
      .then(response => {
        dispatch(setMerchantLoading(false));

        const { data } = response;
        const merchant_details = data.merchant;
        sortCouponsNewestToOldest(merchant_details.coupons);

        dispatch(setMerchantDetails(merchant_details));
      })
      .catch(error => {
        dispatch(apiErrorHandle(error));
        dispatch(setMerchantLoading(false));
        return error.response;
      });
  };
};

const sortCouponsNewestToOldest = coupons => {
  coupons.sort((a, b) => {
    if (a.end_date != null && b.end_date != null) {
      return new Date(b.end_date) - new Date(a.end_date);
    }
    if (a.start_date != undefined && b.start_date != undefined) {
      return new Date(b.start_date) - new Date(a.start_date);
    }
    return false;
  });
};
