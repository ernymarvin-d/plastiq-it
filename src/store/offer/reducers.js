import {
  SET_OFFERS,
  SET_OFFERS_LOADING,
  SET_MORE_OFFERS_LOADING,
  SET_SEARCH_KEY,
  SET_MERCHANT_DETAILS,
  SET_MERCHANT_LOADING
} from "./actionTypes";

const initial_state = {
  offers_list: [],
  offers_loading: false,
  more_offers_loading: false,
  current_page: 1,
  last_page: 1,
  search_key: "",
  merchant_details: null,
  merchant_loading: false
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case SET_OFFERS:
      const { offers_list, current_page, last_page } = action.data;
      return {
        ...state,
        offers_list,
        current_page,
        last_page
      };
    case SET_OFFERS_LOADING:
      return {
        ...state,
        offers_loading: action.loading
      };
    case SET_MORE_OFFERS_LOADING:
      return {
        ...state,
        more_offers_loading: action.loading
      };
    case SET_MERCHANT_DETAILS:
      return {
        ...state,
        merchant_details: action.data
      };
    case SET_MERCHANT_LOADING:
      return {
        ...state,
        merchant_loading: action.loading
      };
    case SET_SEARCH_KEY:
      return {
        ...state,
        search_key: action.search_key
      };
    default:
      return state;
  }
};

export default reducer;
