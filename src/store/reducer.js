import { combineReducers } from "redux";
import authReducer from "@store/auth/reducers";
import cashbackReducer from "@store/cashback/reducers";
import referralReducer from "@store/referral/reducers";
import bankAccountReducer from "@store/account/reducers";
import offerReducer from "@store/offer/reducers";
import navigationReducer from "@store/navigation/reducers";

export default combineReducers({
  auth: authReducer,
  cashback: cashbackReducer,
  referral: referralReducer,
  bank_accounts: bankAccountReducer,
  offer: offerReducer,
  navigation: navigationReducer
});
