/* eslint-disable camelcase */
/* eslint-disable prettier/prettier */
import api from 'service/api';
import apiErrorHandle from 'service/apiErrorHandle';
import { client } from 'service/PlastiqClient';
import {
    SET_REFERRAL,
    SET_SMS_REFERRAL,
    SET_EMAIL_REFERRAL,
    SET_FACEBOOK_REFERRAL,
    SET_WHATSAPP_REFERRAL,
    SET_TWITTER_REFERRAL,
    SET_FACEBOOK_MESSENGER_REFERRAL,
    SET_REFERRAL_LOADING,
} from './actionTypes';

export const setReferralLoading = (loading) => {
    return {
        type: SET_REFERRAL_LOADING,
        loading,
    };
};

export const setReferralData = (data) => {
    return {
        type: SET_REFERRAL,
        data,
    };
};

export const setReferral = (type, referral) => {
    return {
        type,
        referral,
    };
};

export const getReferral = () => {
    return (dispatch) => {
        dispatch(setReferralLoading(true));

        api.get('user/referral')
            .then((response) => {
                referral_data = response.data;
                const {
                    sms_referral,
                    email_referral,
                    facebook_referral,
                    whatsapp_referral,
                    twitter_referral,
                    fbmessenger_referral,
                } = classifyReferrals(referral_data.referral_links);

                dispatch(setReferralData(referral_data));
                dispatch(setReferral(SET_SMS_REFERRAL, sms_referral));
                dispatch(setReferral(SET_EMAIL_REFERRAL, email_referral));
                dispatch(setReferral(SET_FACEBOOK_REFERRAL, facebook_referral));
                dispatch(setReferral(SET_WHATSAPP_REFERRAL, whatsapp_referral));
                dispatch(setReferral(SET_TWITTER_REFERRAL, twitter_referral));
                dispatch(
                    setReferral(
                        SET_FACEBOOK_MESSENGER_REFERRAL,
                        fbmessenger_referral
                    )
                );

                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
            })
            .catch((error) => {
                dispatch(apiErrorHandle(error));

                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
                return error.response;
            });
    };
};

export const getReferralStatus = () => {
    console.log('ref status action called');
    return (dispatch) => {
        dispatch(setReferralLoading(true));

        client
            .get('user/referral/stats')
            .then((response) => {
                console.log('ref stats', { response });
                const referral_data = response.data.referral;
                dispatch(setReferralData(referral_data));
                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
            })
            .catch((error) => {
                dispatch(apiErrorHandle(error));

                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
                return error.response;
            });
    };
};

export const getReferralLink = () => {
    console.log('ref link action called');
    return (dispatch) => {
        dispatch(setReferralLoading(true));
        client
            .get('user/referral/link')
            .then((response) => {
                console.log('ref link', { response });
                const referral_data = response.data;
                const {
                    sms_referral,
                    email_referral,
                    facebook_referral,
                    whatsapp_referral,
                    twitter_referral,
                    fbmessenger_referral,
                } = classifyReferrals(referral_data.referralLinks);

                dispatch(setReferral(SET_SMS_REFERRAL, sms_referral));
                dispatch(setReferral(SET_EMAIL_REFERRAL, email_referral));
                dispatch(setReferral(SET_FACEBOOK_REFERRAL, facebook_referral));
                dispatch(setReferral(SET_WHATSAPP_REFERRAL, whatsapp_referral));
                dispatch(setReferral(SET_TWITTER_REFERRAL, twitter_referral));
                dispatch(
                    setReferral(
                        SET_FACEBOOK_MESSENGER_REFERRAL,
                        fbmessenger_referral
                    )
                );

                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
            })
            .catch((error) => {
                dispatch(apiErrorHandle(error));

                setTimeout(() => {
                    dispatch(setReferralLoading(false));
                }, 800);
                return error.response;
            });
    };
};

const classifyReferrals = (referrals) => {
    const sms_referral = referrals.filter(
        (referral) => referral.channel === 'SMS'
    )[0];
    const email_referral = referrals.filter(
        (referral) => referral.channel === 'Email'
    )[0];
    const facebook_referral = referrals.filter(
        (referral) => referral.channel === 'Facebook'
    )[0];
    const whatsapp_referral = referrals.filter(
        (referral) => referral.channel === 'WhatsApp'
    )[0];
    const twitter_referral = referrals.filter(
        (referral) => referral.channel === 'Twitter'
    )[0];
    const fbmessenger_referral = referrals.filter(
        (referral) => referral.channel === 'Facebook Messenger'
    )[0];

    return {
        sms_referral,
        email_referral,
        facebook_referral,
        whatsapp_referral,
        twitter_referral,
        fbmessenger_referral,
    };
};
