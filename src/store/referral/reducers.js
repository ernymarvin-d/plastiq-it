import {
  SET_REFERRAL,
  SET_SMS_REFERRAL,
  SET_EMAIL_REFERRAL,
  SET_FACEBOOK_REFERRAL,
  SET_WHATSAPP_REFERRAL,
  SET_TWITTER_REFERRAL,
  SET_FACEBOOK_MESSENGER_REFERRAL,
  SET_REFERRAL_LOADING
} from "./actionTypes";

const initial_state = {
  referral: {},
  sms_referral: {},
  email_referral: {},
  facebook_referral: {},
  whatsapp_referral: {},
  twitter_referral: {},
  fbmessenger_referral: {},
  referral_loading: false
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case SET_REFERRAL:
      return {
        ...state,
        referral: action.data
      };
    case SET_SMS_REFERRAL:
      return {
        ...state,
        sms_referral: action.referral
      };
    case SET_EMAIL_REFERRAL:
      return {
        ...state,
        email_referral: action.referral
      };
    case SET_FACEBOOK_REFERRAL:
      return {
        ...state,
        facebook_referral: action.referral
      };
    case SET_WHATSAPP_REFERRAL:
      return {
        ...state,
        whatsapp_referral: action.referral
      };
    case SET_TWITTER_REFERRAL:
      return {
        ...state,
        twitter_referral: action.referral
      };
    case SET_FACEBOOK_MESSENGER_REFERRAL:
      return {
        ...state,
        fbmessenger_referral: action.referral
      };
    case SET_REFERRAL_LOADING:
      return {
        ...state,
        referral_loading: action.loading
      };
    default:
      return state;
  }
};

export default reducer;
