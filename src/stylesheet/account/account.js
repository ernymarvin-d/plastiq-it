import { StyleSheet } from "react-native";

export default StyleSheet.create({
  editField: {
    width: 335,
    borderRadius: 6,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    padding: 20
  },
  active: {
    width: 335,
    borderRadius: 6,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#c562fd",
    padding: 20
  },
  passwordHeading: {
    fontFamily: "Montserrat-Regular",
    width: "100%",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#004183"
  },
  formInput: {
    width: "100%",
    textAlign: "left",
    marginBottom: 20
  },
  textValue: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#404952"
  },
  textValueEdit: {
    fontFamily: "Montserrat-Regular",
    height: 50,
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    borderRadius: 10,
    color: "#404952",
    backgroundColor: "#fff",
    paddingHorizontal: 10,
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1
  },
  textLabel: {
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0.72,
    textAlign: "left",
    color: "#999999",
    textTransform: "uppercase",
    marginBottom: 14
  },
  headingTitle: {
    fontFamily: "Montserrat-Regular",
    width: "100%",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#004183",
    marginTop: 50,
    marginBottom: 30
  },
  box: {
    width: "100%",
    height: 70,
    borderRadius: 6,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(64, 73, 82, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    flexDirection: "row",
    marginBottom: 10,
    alignItems: "center",
    padding: 15
  },
  circle: {
    width: 40,
    height: 40,
    backgroundColor: "#f1effc",
    borderRadius: 40 / 2,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 12
  },
  bankName: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 14,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#404952"
  },
  bankType: {
    width: "100%",
    fontFamily: "Montserrat-Regular",
    fontSize: 12,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#b0b2b6"
  },
  btn: {
    width: "100%",
    height: 54,
    borderRadius: 27,
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius: 15,
    shadowOpacity: 1,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  btnText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  },
  accountDetailsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 18,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#004183"
  },
  btnEditText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "normal",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "left",
    color: "#c562fd"
  }
});
