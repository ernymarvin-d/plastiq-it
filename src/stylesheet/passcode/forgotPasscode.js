import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    height: "100%"
  },
  content: {
    flex: 1,
    padding: 20,
    marginTop: 15
  },
  headerContent: {
    flexDirection: "row",
    marginTop: 17
  },
  headerBackImage: {
    width: 10,
    resizeMode: "contain"
  },
  headerTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    color: "#33203f"
  },
  phoneImageContent: {
    marginTop: 15,
    alignItems: "center"
  },
  phoneImage: {
    width: 50,
    resizeMode: "contain"
  },
  textContent: {
    flexDirection: "column",
    alignItems: "center"
  },
  textTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 20,
    color: "#33203f"
  },
  textSubTitle: {
    color: "#999999",
    marginTop: 10
  },
  textNumber: {
    color: "#33203f",
    fontWeight: "bold",
    marginTop: 5
  },
  codeContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20
  },
  codeInput: {
    width: 39,
    height: 50,
    marginHorizontal: 8,
    borderRadius: 8,
    backgroundColor: "#e3cdf0",
    color: "#9040bd",
    fontSize: 22,
    textAlign: "center",
    fontWeight: "bold"
  },
  codeInputShadow: {
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 1,
      height: 5
    },
    shadowOpacity: 0.3,
    shadowRadius: 5.0
  },
  textBottomContent: {
    marginTop: 30,
    flexDirection: "column",
    alignItems: "center"
  },
  textBottom: {
    color: "#999999"
  },

  textBottomTime: {
    width: 50,
    color: "#33203f",
    fontWeight: "bold",
    textAlign: "right"
  },
  textBottomResend: {
    color: "rgb(168, 75, 221)"
  },
  buttonContent: {
    alignItems: "center",
    marginTop: 35
  },
  button: {
    width: 311,
    height: 50,
    borderRadius: 25,
    backgroundColor: "rgb(144, 64, 189)",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonShadow: {
    shadowColor: "rgba(168, 75, 221, 0.5)",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 1,
    shadowRadius: 10.0
  },
  buttonText: {
    fontFamily: "Montserrat-Regular",
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff"
  }
});
