import { Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");
function wp(percentage) {
  const value = (percentage * width) / 100;
  return Math.round(value);
}
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

// Used via Metrics.baseMargin
const metrics = {
  marginHorizontal: 10,
  marginVertical: 10,
  section: 25,
  baseMargin: 10,
  doubleBaseMargin: 20,
  smallMargin: 5,
  doubleSection: 50,
  horizontalLineHeight: 1,
  screenWidth: width < height ? width : height,
  menuScreenWidth: width < height ? width * 0.9 : height * 0.9,
  screenHeight: width < height ? height : width,
  navBarHeight: Platform.OS === "ios" ? 64 : 54,
  buttonRadius: 4,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50,
    s1: 40
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 200
  },
  sliderWidth: width,
  itemWidth: slideWidth + itemHorizontalMargin * 2,
  passwordStrengthMeterWidth: width - 35 * 2 // passwordStrengthContainer's paddingHorizontal = 30
};

export default metrics;
